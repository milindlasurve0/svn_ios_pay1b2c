//
//  StoreLocationResponse.m
//  Pay1
//
//  Created by Annapurna on 10/08/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "StoreLocationResponse.h"

@implementation StoreLocationResponse

-(id)init{
    self = [super init];
    if(self) {
    self.storeLocationArray = [[NSMutableArray alloc] init] ;
    }
    return self;
}

@end
