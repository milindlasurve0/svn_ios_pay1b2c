//
//  GetOperatorResponse.h
//  Pay1
//
//  Created by Annapurna on 13/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetOperatorResponse : NSObject

@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *errorCode;
@property (nonatomic, strong) NSMutableDictionary *discriptionOperator;



@property (nonatomic, strong) NSMutableArray *mobileArrayList;
@property (nonatomic, strong) NSMutableArray *dataCardArrayList;
@property (nonatomic, strong) NSMutableArray *postPaidArrayList;
@property (nonatomic, strong) NSMutableArray *dthArrayList;



@end
