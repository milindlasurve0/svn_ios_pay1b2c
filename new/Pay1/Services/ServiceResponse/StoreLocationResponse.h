//
//  StoreLocationResponse.h
//  Pay1
//
//  Created by Annapurna on 10/08/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoreLocationResponse : NSObject

@property (nonatomic, strong) NSMutableArray *storeLocationArray;

@end
