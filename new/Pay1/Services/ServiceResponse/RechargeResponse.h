//
//  RechargeResponse.h
//  Pay1
//
//  Created by Annapurna on 15/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RechargeResponse : NSObject

@property (nonatomic, strong) NSString *errorCode;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *formContent;
@property (nonatomic, strong) NSString *allDeals;
@property (nonatomic, strong) NSString *claimedGifts;
@property (nonatomic, strong) NSString *offerDetails;
@property (nonatomic, strong) NSString *txnID;
@property (nonatomic, strong) NSString *descMessage;

@property (nonatomic, strong) NSString *closingBalance;
@property (nonatomic, strong) NSString *expiry;
@property (nonatomic, strong) NSString *loyaltyPoints;
@property (nonatomic, strong) NSString *loyaltyBalance;
@property (nonatomic, strong) NSString *onlineForm;
@property (nonatomic, strong) NSString *operatorName;
@property (nonatomic, strong) NSString *pin;



@property (nonatomic, strong) NSMutableArray *dealArrayList;

@end
