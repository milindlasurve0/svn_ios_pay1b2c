//
//  GiftCategoryResponse.h
//  Pay1
//
//  Created by Annapurna on 05/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GiftCategoryResponse : NSObject
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *errorCode;

@property (nonatomic, strong) NSMutableArray *giftCatDataArray;

@end
