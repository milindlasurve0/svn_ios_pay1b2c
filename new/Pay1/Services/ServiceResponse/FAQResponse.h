//
//  FAQResponse.h
//  Pay1
//
//  Created by webninjaz on 16/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FAQResponse : NSObject

@property (nonatomic, strong) NSString *statusFAQResponse;
@property (nonatomic, strong) NSString *errorCodeFAQResponse;
@property (nonatomic, strong) NSMutableArray *descriptionResponse;
@property (nonatomic, strong) NSString * idResponse;
@property (nonatomic, strong) NSString *questionResponse;
@property (nonatomic, strong) NSString *answerResponse;
@property (nonatomic, strong) NSString *visibleResponse;

@end
