//
//  GiftDetailResponse.h
//  Pay1
//
//  Created by Annapurna on 18/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GiftDetailResponse : NSObject

@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *errorCode;

@property (nonatomic, strong) NSString *brandDescription;
@property (nonatomic, strong) NSString *companyURL;

@property (nonatomic, strong) NSString *contentText;

@property (nonatomic, strong) NSMutableArray *imageListArray;

@property (nonatomic, strong) NSString *offerAverage;

@property (nonatomic, strong) NSMutableArray *locationDetailArray;
@property (nonatomic, strong) NSMutableArray *offerDetailArray;

@property (nonatomic, strong) NSMutableDictionary *offerInfoDic;

@property (nonatomic, strong) NSString *appointment;
@property (nonatomic, strong) NSString *days;
@property (nonatomic, strong) NSString *dealerContact;
@property (nonatomic, strong) NSString *exclusiveGender;
@property (nonatomic, strong) NSString *time;



@property (nonatomic, strong) NSString *offerInfoHTML;
@property (nonatomic, strong) NSString *offerLike;
@property (nonatomic, strong) NSString *pocketGift;



@end
