//
//  QuickPayResponse.h
//  Pay1
//
//  Created by Annapurna on 03/07/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QuickPayResponse : NSObject

@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *errorCode;
@property (nonatomic, strong) NSString *missedNumber;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSMutableArray *descriptionArray;



@end
