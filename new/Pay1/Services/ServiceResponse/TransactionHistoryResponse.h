//
//  TransactionHistoryResponse.h
//  Pay1
//
//  Created by webninjaz on 09/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransactionHistoryResponse : NSObject

@property (nonatomic, strong) NSString *statusTransactionHistoryResponse;
@property (nonatomic, strong) NSString *errorCodeTransactionHistoryResponse;
@property (nonatomic, strong) NSString *descriptionTransactionHistoryResponse;
@property (nonatomic,strong)  NSMutableArray *transactionHistoryResponse;

@end
