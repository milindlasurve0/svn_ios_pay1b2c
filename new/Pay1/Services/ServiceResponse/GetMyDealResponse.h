//
//  GetMyDealResponse.h
//  Pay1
//
//  Created by Annapurna on 12/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetMyDealResponse : NSObject

@property (nonatomic, strong) NSString *errorCode;
@property (nonatomic, strong) NSString *status;

@property (nonatomic, strong) NSMutableArray *myDealResponseArray;

@end
