//
//  GetOperatorResponse.m
//  Pay1
//
//  Created by Annapurna on 13/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "GetOperatorResponse.h"

@implementation GetOperatorResponse

-(id)init {
    self = [super init];
    if(self) {
        self.mobileArrayList = [[NSMutableArray alloc] init] ;
        self.dataCardArrayList = [[NSMutableArray alloc] init] ;
        self.postPaidArrayList = [[NSMutableArray alloc] init] ;
        self.dthArrayList = [[NSMutableArray alloc] init] ;
    }
    return self;
}

@end
