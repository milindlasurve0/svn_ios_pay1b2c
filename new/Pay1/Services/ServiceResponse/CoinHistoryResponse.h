//
//  CoinHistoryResponse.h
//  Pay1
//
//  Created by webninjaz on 09/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoinHistoryResponse : NSObject

@property (nonatomic, strong) NSString *statusCoinHistoryResponse;
@property (nonatomic, strong) NSString *errorCodeCoinHistoryResponse;
@property (nonatomic, strong) NSString *descriptionCoinHistoryResponse;
@property (nonatomic,strong) NSMutableArray *coinHistoryResponse;

@end
