//
//  CoinHistoryResponse.m
//  Pay1
//
//  Created by webninjaz on 09/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "CoinHistoryResponse.h"

@implementation CoinHistoryResponse

-(id)init {
    self = [super init];
    if(self) {
        self.coinHistoryResponse = [[NSMutableArray alloc] init] ;
    }
    return self;
}

@end
