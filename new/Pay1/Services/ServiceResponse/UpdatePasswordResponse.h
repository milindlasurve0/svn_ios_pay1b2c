//
//  UpdatePasswordResponse.h
//  Pay1
//
//  Created by Annapurna on 12/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpdatePasswordResponse : NSObject

@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *errorCode;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *messageOTP;

@end
