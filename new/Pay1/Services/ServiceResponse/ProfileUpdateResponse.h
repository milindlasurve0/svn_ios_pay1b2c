//
//  ProfileUpdateResponse.h
//  Pay1
//
//  Created by webninjaz on 11/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProfileUpdateResponse : NSObject

@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *error;
@property (nonatomic, strong) NSString *descMessage;
@property (nonatomic, strong) NSString *profileID;
@property (nonatomic, strong) NSString *profileName;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *profileDOB;
@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *profileGCM;
@property (nonatomic, strong) NSString *profileUDID;
@property (nonatomic, strong) NSString *profileLat;
@property (nonatomic, strong) NSString *profileLong;
@property (nonatomic, strong) NSString *profileSRCP;
@property (nonatomic, strong) NSString *profileManufacturer;
@property (nonatomic, strong) NSString *profileOS;
@property (nonatomic, strong) NSString *profileAPI;
@property (nonatomic, strong) NSString *profileImageURL;
@property (nonatomic, strong) NSString *profileFBData;
@property (nonatomic, strong) NSString *profileGenuine;
@property (nonatomic, strong) NSString *profileDeviceType;
@property (nonatomic, strong) NSString *profileUserType;
@property (nonatomic, strong) NSString *createdDOB;
@property (nonatomic, strong) NSString *profileUpdatedResponse;
@property (nonatomic, strong) NSString *profileLatLong;

@end
