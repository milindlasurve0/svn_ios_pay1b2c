//
//  GetUpdatedDealResponse.h
//  Pay1
//
//  Created by Annapurna on 09/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetUpdatedDealResponse : NSObject
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *errorCode;
@property (nonatomic, strong) NSMutableArray *offerDetailsArray;

@property (nonatomic, strong) NSString *claimedGifts;
@property (nonatomic, strong) NSString *expiredOffers;


@property (nonatomic, strong) NSMutableArray *allDealsArray;


@end
