//
//  UpdatePasswordService.h
//  Pay1
//
//  Created by Annapurna on 12/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "Service.h"
@class UpdatePasswordCriteria;
@class UpdatePasswordResponse;
typedef void (^updatePasswordResponse)(NSError *error, UpdatePasswordResponse *objUpdatePswdResponse);
typedef void (^forgotPswdResponse)(NSError *error, UpdatePasswordResponse *objForgotPswdRespons);
@interface UpdatePasswordService : Service

-(void)callUpdatePasswordService:(UpdatePasswordCriteria *)criteria callback:(updatePasswordResponse)callback;
-(void)callForgotPasswordService:(NSString *)userName vtype:(NSString *)vType callback:(forgotPswdResponse)callback;
@end
