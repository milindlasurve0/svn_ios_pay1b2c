//
//  UpdatePasswordService.m
//  Pay1
//
//  Created by Annapurna on 12/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "UpdatePasswordService.h"
#import "UpdatePasswordCriteria.h"
#import "UpdatePasswordResponse.h"
#import "Session.h"
@implementation UpdatePasswordService

-(void)callUpdatePasswordService:(UpdatePasswordCriteria *)criteria callback:(updatePasswordResponse)callback{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
    [postData setObject:criteria.userMobNum forKey:@"mobile_number"];
    [postData setObject:criteria.userPassword forKey:@"password"];
    [postData setObject:criteria.confirmPassword forKey:@"confirm_password"];
    [postData setObject:criteria.currentPassword forKey:@"current_password"];
    
    [postData setObject:criteria.otpNum forKey:@"code"];
      
    NSLog(@"PostData for Update Password :%@",postData);
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    requestData.webServiceURL = kUpdatePasswordURL;
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
    [self printResponse:request.responseData forRequest:@"Update Password Response"];
    if ([self handleNullResponse:request])
    return;
        
    callback(nil, [self parseUpdatePasswordResponse:request]);
    };
    
    
    requestData.requestFailureBlock = ^(RequestData *request) {
    [self handleNetworkError:request.responseData];
    callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];

}

-(UpdatePasswordResponse *)parseUpdatePasswordResponse:(RequestData *)request{
    UpdatePasswordResponse *objUpdatePasswordResponse = [[UpdatePasswordResponse alloc]init];
    objUpdatePasswordResponse.status = [request.responseData objectForKey:@"status"];
    objUpdatePasswordResponse.errorCode = [request.responseData objectForKey:@"errCode"];
    objUpdatePasswordResponse.desc = [request.responseData objectForKey:@"description"];
    
    if([request.responseData objectForKey:@"otp"]){
     objUpdatePasswordResponse.messageOTP = [request.responseData objectForKey:@"otp"];
    }
    
    return objUpdatePasswordResponse;
}

#pragma mark - ForGot Password

-(void)callForgotPasswordService:(NSString *)userName vtype:(NSString *)vType callback:(forgotPswdResponse)callback{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
    [postData setObject:userName forKey:@"user_name"];
    [postData setObject:vType forKey:@"vtype"];
    NSLog(@"PostData for Update Password :%@",postData);
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    requestData.webServiceURL = kForgotPasswordURL;
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
    [self printResponse:request.responseData forRequest:@"Forgot Password Response"];
    if ([self handleNullResponse:request])
    return;
        
    callback(nil, [self parseUpdatePasswordResponse:request]);
    };
    
    
    requestData.requestFailureBlock = ^(RequestData *request) {
        [self handleNetworkError:request.responseData];
        callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];
 
}

@end
