//
//  TransactionHistoryService.m
//  Pay1
//
//  Created by webninjaz on 09/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "TransactionHistoryService.h"
#import "TransactionHistoryCriteria.h"
#import "TransactionHistoryResponse.h"
#import "TransactionList.h"

@implementation TransactionHistoryService

-(void)callTransactionHistoryService:(TransactionHistoryCriteria *)criteria callback:(transactionHistoryResponse)callback{
    
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
   // [postData setObject:criteria.transactionParamCriteria forKey:@"Param"];
    [postData setObject:criteria.transactionPageCriteria forKey:@"page"];
    [postData setObject:criteria.transactionLimitCriteria forKey:@"limit"];
    [self fillDefaultData:postData];
    NSLog(@"PostData for Update Password :%@",postData);
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    requestData.webServiceURL = kTransactionHistoryURL;
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
    [self printResponse:request.responseData forRequest:@"Update Transaction History Response"];
    if ([self handleNullResponse:request])
    return;
        
    callback(nil, [self parseTransactionHistoryResponse:request]);
    };
    
    requestData.requestFailureBlock = ^(RequestData *request) {
        [self handleNetworkError:request.responseData];
        callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];
}

-(TransactionHistoryResponse *)parseTransactionHistoryResponse:(RequestData *)request{
    TransactionHistoryResponse *objTransactionResponse = [[TransactionHistoryResponse alloc]init];
    
    objTransactionResponse.statusTransactionHistoryResponse = [request.responseData objectForKey:@"status"];
    NSLog(@"The status is%@",objTransactionResponse.statusTransactionHistoryResponse);
    
    objTransactionResponse.errorCodeTransactionHistoryResponse = [request.responseData objectForKey:@"errCode"];
    NSLog(@"The code wallet response is%@",objTransactionResponse.errorCodeTransactionHistoryResponse);
    
    if ([objTransactionResponse.statusTransactionHistoryResponse isEqualToString:@"failure"]) {
        return objTransactionResponse;
    }
    
    NSMutableArray *objTransactionResponseArray = [request.responseData valueForKey:@"description"];
    NSLog(@"The discription is %@",objTransactionResponseArray);
    
    for(NSMutableDictionary *dic in objTransactionResponseArray){
        TransactionList *transactionList = [[TransactionList alloc]init];
        transactionList.transactionID = [dic objectForKey:@"transaction_id"];
        NSLog(@"The wallet history base amount is%@",transactionList.transactionID);
        transactionList.transactionTransType = [dic objectForKey:@"trans_type"];
       // NSLog(@"The closing balance is%@",transactionList.transactionTransType);
        transactionList.transactionDateTime = [dic objectForKey:@"trans_datetime"];
        transactionList.transactionAmount = [dic objectForKey:@"transaction_amount"];
        transactionList.transactionTransCategory  = [dic objectForKey:@"trans_category"];
        transactionList.transactionStatus = [dic objectForKey:@"status"];
       // NSLog(@"The transaction status is%@",transactionList.transactionStatus);
        transactionList.transactionPaymentMode = [dic objectForKey:@"transaction_mode"];
     //   NSLog(@"The transaction payment mode is%@",transactionList.transactionPaymentMode);
        transactionList.transactionPhoneNumber = [dic objectForKey:@"number"];
        transactionList.transactionRechargeFlag = [dic objectForKey:@"recharge_flag"];
        transactionList.transactionClosingBalance = [dic objectForKey:@"closing_bal"];
        transactionList.transactionOperatorName = [dic objectForKey:@"operator_name"];
        transactionList.transactionDealName = [dic objectForKey:@"deal_name"];
        transactionList.transactionOfferName = [dic objectForKey:@"offer_name"];
      
        transactionList.transactionBaseAmount = [dic objectForKey:@"base_amount"];
        transactionList.transactionIPAddress = [dic objectForKey:@"ip_address"];
        transactionList.transactionMihPayID = [dic objectForKey:@"mihpayid"];
        transactionList.transactionResponseID = [dic objectForKey:@"response_id"];
        transactionList.transactionProductID = [dic objectForKey:@"product_id"];
        transactionList.transactionServiceID = [dic objectForKey:@"service_id"];
        
        transactionList.paymentMode = [dic objectForKey:@"payment_mode"];

        
      //  NSLog(@"The wallet trasaction mode is%@",transactionList.transactionServiceID);
       [objTransactionResponse.transactionHistoryResponse addObject:transactionList];
    }
    return objTransactionResponse;
}


@end
