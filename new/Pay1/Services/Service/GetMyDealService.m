//
//  GetMyDealService.m
//  Pay1
//
//  Created by Annapurna on 12/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "GetMyDealService.h"
#import "GetMyDealResponse.h"
#import "GetMyDealList.h"
@implementation GetMyDealService
-(void)callGetMyDealService:(geMytDealResponse)callback{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
    [self fillDefaultData:postData];
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    requestData.webServiceURL = kGetMyDealURL;
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
        
    [self printResponse:request.responseData forRequest:@"Get My Deal Response"];
    if ([self handleNullResponse:request])
    return;
        
    callback(nil, [self parseUpdatedMyDealResponse:request]);
    };
    
    
    requestData.requestFailureBlock = ^(RequestData *request) {
    [self handleNetworkError:request.responseData];
    callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];
}

-(GetMyDealResponse *)parseUpdatedMyDealResponse:(RequestData *)request{
    GetMyDealResponse *dealResponse = [[GetMyDealResponse alloc]init];
    dealResponse.errorCode = [request.responseData objectForKey:@"errCode"];
    dealResponse.status = [request.responseData objectForKey:@"status"];
    
    if([[request.responseData valueForKey:@"description"] isKindOfClass:[NSDictionary class]]){
    NSLog(@"Its Dictionary");
    }
    
   if([[request.responseData valueForKey:@"description"] isKindOfClass:[NSArray class]]){
    NSMutableArray *descriptionArray = [request.responseData valueForKey:@"description"];
    for(NSMutableDictionary *dic in descriptionArray){
    GetMyDealList *objMyDealList = [[GetMyDealList alloc]init];
    objMyDealList.actualPrice = [dic objectForKey:@"actual_price"];
    objMyDealList.amount = [dic objectForKey:@"amount"];
    objMyDealList.code = [dic objectForKey:@"code"];
    objMyDealList.couponStatus = [dic objectForKey:@"coupon_status"];
    objMyDealList.dealID = [dic objectForKey:@"deal_id"];
    objMyDealList.dealName = [dic objectForKey:@"deal_name"];
    objMyDealList.dealerContact = [dic objectForKey:@"dealer_contact"];
    objMyDealList.discount = [dic objectForKey:@"discount"];
    objMyDealList.expiry = [dic objectForKey:@"expiry"];
    objMyDealList.myDealID = [dic objectForKey:@"id"];
    objMyDealList.myDealImgURL = [dic objectForKey:@"img_url"];
    objMyDealList.myDealLogoURL = [dic objectForKey:@"logo_url"];
    objMyDealList.longDesc = [dic objectForKey:@"long_desc"];
    objMyDealList.minAmount = [dic objectForKey:@"min_amount"];
    objMyDealList.myLikes = [dic objectForKey:@"mylikes"];
    objMyDealList.offerDec = [dic objectForKey:@"offer_desc"];
    objMyDealList.offerID = [dic objectForKey:@"offer_id"];
    objMyDealList.offerName = [dic objectForKey:@"offer_name"];
    objMyDealList.offerPrice = [dic objectForKey:@"offer_price"];
    objMyDealList.quantity = [dic objectForKey:@"quantity"];
    objMyDealList.shortDesc = [dic objectForKey:@"short_desc"];
    objMyDealList.staus = [dic objectForKey:@"status"];
    objMyDealList.stockSold = [dic objectForKey:@"stock_sold"];
    objMyDealList.totalStock = [dic objectForKey:@"total_stock"];
    objMyDealList.transDateTime = [dic objectForKey:@"trans_datetime"];
    objMyDealList.transactionID = [dic objectForKey:@"transaction_id"];
    objMyDealList.transactionMode = [dic objectForKey:@"transaction_mode"];
    objMyDealList.usersID = [dic objectForKey:@"users_id"];
    objMyDealList.pinCode = [dic objectForKey:@"pin"];


    [dealResponse.myDealResponseArray addObject:objMyDealList];
        
    }
    }
    return dealResponse;
}

@end
