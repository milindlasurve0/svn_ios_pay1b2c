//
//  GetUpdatedDealService.h
//  Pay1
//
//  Created by Annapurna on 09/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "Service.h"
@class GetUpdatedDealResponse;
@class GetUpdateDealCriteria;
typedef void (^getUpdatedDealResponse)(NSError *error,GetUpdatedDealResponse *updateDealResponse);
@interface GetUpdatedDealService : Service
-(void)callUpdatedDealService:(GetUpdateDealCriteria *)criteria callback:(getUpdatedDealResponse)callback;
@end
