//
//  MyLikeService.h
//  Pay1
//
//  Created by Annapurna on 17/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "Service.h"
@class MyLikeResponse;
typedef void (^myLikeResponse)(NSError *error,MyLikeResponse *likeResponse);
@interface MyLikeService : Service
-(void)callMyLikeService:(NSString *)likeOfferID callback:(myLikeResponse)callback;
@end
