//
//  ProfileUpdateService.h
//  Pay1
//
//  Created by webninjaz on 11/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "Service.h"

@class ProfileUpdateCriteria;
//@class ProfileUpdateResponse;
@class SignInResponse;
typedef void (^profileUpdateResponse)(NSError *error, SignInResponse *profileUpdateResponse);

@interface ProfileUpdateService : Service

-(void)callProfileUpdateService:(ProfileUpdateCriteria *)profileUpdateCriteria callback:(profileUpdateResponse)callback;

@end
