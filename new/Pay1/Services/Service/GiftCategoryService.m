//
//  GiftCategoryService.m
//  Pay1
//
//  Created by Annapurna on 05/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "GiftCategoryService.h"
#import "GiftCategoryResponse.h"
#import "GiftCategoryList.h"
#import "GiftCriteria.h"
@implementation GiftCategoryService

-(void)callGiftCategoryService:(GiftCriteria *)criteria callback:(giftCatResponse)callback{
    
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
    [postData setObject:criteria.type forKey:@"type"];
    [postData setObject:criteria.next forKey:@"next"];
    
    NSLog(@"#GiftCatResponse :%@",postData);
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    requestData.webServiceURL = kGiftCategoryURL;
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
        
    [self printResponse:request.responseData forRequest:@"GetCatGift Response"];
    if ([self handleNullResponse:request])
    return;
        
    callback(nil, [self parseGiftCategoryResponse:request]);
    };
    
    
    requestData.requestFailureBlock = ^(RequestData *request) {
    [self handleNetworkError:request.responseData];
    callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];
}

-(GiftCategoryResponse *)parseGiftCategoryResponse:(RequestData *)request{
    GiftCategoryResponse *objGiftCatResponse = [[GiftCategoryResponse alloc]init];
    objGiftCatResponse.status = [request.responseData objectForKey:@"status"];
    objGiftCatResponse.errorCode = [request.responseData objectForKey:@"errCode"];
    
    NSMutableArray *descArray = [request.responseData valueForKey:@"description"];
    
    for(NSMutableDictionary *dic in descArray){
    GiftCategoryList *objCatList = [[GiftCategoryList alloc]init];
    objCatList.catName = [dic objectForKey:@"name"];
    objCatList.catID = [dic objectForKey:@"id"];
    objCatList.details = [dic objectForKey:@"details"];
        

    [objGiftCatResponse.giftCatDataArray addObject:objCatList];
    }
  
    return objGiftCatResponse;
}

@end
