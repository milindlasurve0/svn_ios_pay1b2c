//
//  GetMyDealService.h
//  Pay1
//
//  Created by Annapurna on 12/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "Service.h"
@class GetMyDealResponse;
typedef void (^geMytDealResponse)(NSError *error,GetMyDealResponse *objGetMyDealResponse);
@interface GetMyDealService : Service
-(void)callGetMyDealService:(geMytDealResponse)callback;
@end
