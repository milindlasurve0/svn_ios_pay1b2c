//
//  Service.h
//  Pay1
//
//  Created by Annapurna on 11/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServiceManager.h"
#import "RequestData.h"
#import "NSString+Utility.h"
#import "MBProgressHUD.h"

extern NSString *const kBaseURL;
extern NSString *const kMobilePlanBaseURL;
extern NSString *const kCreateUserURL;
extern NSString *const kSignInURL;
extern NSString *const kUpdatePasswordURL;
extern NSString *const kGetOperatorURL;
extern NSString *const kRechargeURL;
extern NSString *const kRefillWalletURL;
extern NSString *const kRedeemCouponURL;
extern NSString *const kGiftServiceURL;
extern NSString *const kGiftDetailURL;
extern NSString *const kGiftCategoryURL;
extern NSString *const kFAQRequestURL;
extern NSString *const kWalletRequestURL;
extern NSString *const kSignOutURL;
extern NSString *const kGetMobileDataURL;
extern NSString *const kGetUpdatedDealResponseURL;
extern NSString *const kGetMyDealURL;
extern NSString *const kMyLikeURL;
extern NSString *const kPurchaseDealURL;
extern NSString *const kRedeemURL;
extern NSString *const kCoinHistoryURL;
extern NSString *const kProfileUpdateURL;
extern NSString *const kTransactionHistoryURL;
extern NSString *const kUserReviewURL;
extern NSString *const kForgotPasswordURL;
extern NSString *const kQuickPayURL;
extern NSString *const kEditPayURL;
extern NSString *const kStoreLocationURL;
extern NSString *const kPostPaidRechargeURL;

@interface Service : NSObject{
    
}

-(BOOL)handleNullResponse:(RequestData*)request;
-(void) printResponse:(id)response forRequest:(NSString *) request ;
-(void) handleNetworkError:(NSError *)error;
-(void) fillDefaultData:(NSMutableDictionary *) dic;

@end
