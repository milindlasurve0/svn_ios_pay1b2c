//
//  StoreLocationService.m
//  Pay1
//
//  Created by Annapurna on 10/08/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "StoreLocationService.h"
#import "StoreLocationCriteria.h"
#import "StoreLocationResponse.h"
#import "StoreList.h"
@implementation StoreLocationService
-(void)callStoreLocationService:(StoreLocationCriteria *)storeCritera callback:(storeLocationResponse)callback{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
    [postData setObject:@"getNearByRetailer" forKey:@"method"];
    [postData setObject:storeCritera.userLat forKey:@"lat"];
    [postData setObject:storeCritera.userLong forKey:@"lng"];
    [postData setObject:storeCritera.userMobNum forKey:@"mobile"];
    [postData setObject:storeCritera.distance forKey:@"distance"];
    
    NSLog(@"#PostDataForStoreLocation :%@",postData);
    
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kMobilePlanBaseURL;
    requestData.webServiceURL = kStoreLocationURL;
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
        
    [self printResponse:request.responseData forRequest:@"GetStore Response"];
    if ([self handleNullResponse:request])
    return;
        
    callback(nil, [self parseStoreLocationResponse:request]);
    };
    
    
    requestData.requestFailureBlock = ^(RequestData *request) {
    [self handleNetworkError:request.responseData];
    callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];
}

-(StoreLocationResponse *)parseStoreLocationResponse:(RequestData *)request{
    StoreLocationResponse *objStoreResponse = [[StoreLocationResponse alloc]init];
    NSArray *storeArray = request.responseData;

    NSMutableArray *storeLocArray = [storeArray objectAtIndex:0];
    
    for(NSMutableDictionary *dic in storeLocArray){
    StoreList *objStoreList = [[StoreList alloc]init];
    NSDictionary *dataDic = [dic objectForKey:@"t"];
    objStoreList.storeDistance = [dataDic objectForKey:@"D"];
    objStoreList.storeAddress = [dataDic objectForKey:@"address"];
    objStoreList.storeAreaName = [dataDic objectForKey:@"area_name"];
    objStoreList.storeCityName = [dataDic objectForKey:@"city_name"];
    objStoreList.storeImageURL = [dataDic objectForKey:@"imagepath"];
    objStoreList.storeLat = [dataDic objectForKey:@"latitude"];
    objStoreList.storeLong = [dataDic objectForKey:@"longitude"];
    objStoreList.storeMobileNum = [dataDic objectForKey:@"mobile"];
    objStoreList.storePinCode = [dataDic objectForKey:@"pin"];
    objStoreList.storeSale = [dataDic objectForKey:@"sale"];
    objStoreList.storeShopName = [dataDic objectForKey:@"shopname"];
    objStoreList.storeStateName = [dataDic objectForKey:@"state_name"];
    objStoreList.storeUserID = [dataDic objectForKey:@"user_id"];
        
   // NSLog(@"StoreValues :%@ %@",objStoreList.storeAddress,objStoreList.storeCityName);

    [objStoreResponse.storeLocationArray addObject:objStoreList];
    }


    return objStoreResponse;
}

@end
