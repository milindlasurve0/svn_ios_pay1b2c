//
//  SaveUserReviewService.h
//  Pay1
//
//  Created by Annapurna on 25/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "Service.h"
@class SaveUserResponse;
typedef void (^saveUserResponse)(NSError *error,SaveUserResponse *objSaveUserResponse);

@interface SaveUserReviewService : Service
-(void)callSaveUserReview:(NSString *)offerReviewID reviewPoint:(NSString *)reviewPoint callback:(saveUserResponse)callback;

@end
