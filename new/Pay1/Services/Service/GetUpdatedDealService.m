//
//  GetUpdatedDealService.m
//  Pay1
//
//  Created by Annapurna on 09/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "GetUpdatedDealService.h"
#import "GetUpdateDealCriteria.h"
#import "GetUpdatedDealResponse.h"
#import "AllDealList.h"
#import "DealLocationList.h"
@implementation GetUpdatedDealService

-(void)callUpdatedDealService:(GetUpdateDealCriteria *)criteria callback:(getUpdatedDealResponse)callback{
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
    [postData setObject:criteria.userMobileNum forKey:@"user_mobile"];
    [postData setObject:criteria.userLat forKey:@"latitude"];
    [postData setObject:criteria.userLong forKey:@"longitude"];
    [postData setObject:criteria.updatedTime forKey:@"updatedTime"];
    [postData setObject:criteria.nextValue forKey:@"next"];
    [self fillDefaultData:postData];
    
    
    NSLog(@"PostData is :%@",postData);
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    requestData.webServiceURL = kGetUpdatedDealResponseURL;
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
        
    [self printResponse:request.responseData forRequest:@"Get Deal Response"];
    if ([self handleNullResponse:request])
    return;
        
    callback(nil, [self parseUpdatedDealResponse:request]);
    };
    
    
    requestData.requestFailureBlock = ^(RequestData *request) {
    [self handleNetworkError:request.responseData];
    callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];

}

-(GetUpdatedDealResponse *)parseUpdatedDealResponse:(RequestData *)request{
    GetUpdatedDealResponse *updatedResponse = [[GetUpdatedDealResponse alloc]init];
    
    
    updatedResponse.status = [NSString handleNull:[request.responseData objectForKey:@"status"]];
    updatedResponse.errorCode = [request.responseData objectForKey:@"errCode"];
    NSMutableDictionary *descriptionDic = [request.responseData objectForKey:@"description"];
    updatedResponse.offerDetailsArray = [descriptionDic valueForKey:@"offer_details"];
    
    
    if([descriptionDic objectForKey:@"claimedGifts"] != [NSNull null]){
    updatedResponse.claimedGifts = [NSString handleNull:[descriptionDic objectForKey:@"claimedGifts"] ];
    }
    
     if([descriptionDic objectForKey:@"ExpiredOffers"] != [NSNull null]){
    updatedResponse.expiredOffers = [NSString handleNull:[descriptionDic objectForKey:@"ExpiredOffers"] ];
     }
  //  NSLog(@"GetDetailValues :%@ %@ %@",updatedResponse.offerDetails,updatedResponse.claimedGifts,updatedResponse.expiredOffers);
    
    NSMutableArray *allDealArrayList = [descriptionDic valueForKey:@"Alldeals"];
    for(NSDictionary *dic in allDealArrayList){
        
        
    AllDealList *objAllDealList = [[AllDealList alloc]init];
    objAllDealList.dealID = [[dic objectForKey:@"id"] intValue];
    
    if([dic objectForKey:@"name"] != [NSNull null]){
    NSString *nameValue  =  [NSString handleNull:[[dic objectForKey:@"name"] stringByReplacingOccurrencesOfString:@"'" withString:@"%27"]];
  //  NSLog(@"nameValue :%@",nameValue);
    objAllDealList.dealName = nameValue; //[dic objectForKey:@"name"];
    }
        
   if([dic objectForKey:@"i_url"] != [NSNull null]){
    objAllDealList.dealImgURL = [NSString handleNull:[dic objectForKey:@"i_url"] ];
   // NSLog(@"i_url :%@",objAllDealList.dealImgURL);

   }
        
    if([dic objectForKey:@"logo_url"] != [NSNull null]){
    objAllDealList.dealLogoURL = [NSString handleNull:[dic objectForKey:@"logo_url"] ];
  //  NSLog(@"dealLogoURL :%@",objAllDealList.dealLogoURL);

    }
        
    if([dic objectForKey:@"cat"] != [NSNull null]){
    NSString *catValue  =  [NSString handleNull:[[dic objectForKey:@"cat"] stringByReplacingOccurrencesOfString:@"'" withString:@"%27"] ];
    objAllDealList.dealCat = catValue; //[dic objectForKey:@"cat"];
  //  NSLog(@"dealCat :%@",objAllDealList.dealCat);

    }
     
    if([dic objectForKey:@"cat_id"] != [NSNull null]){
    objAllDealList.dealCatID = [[dic objectForKey:@"cat_id"] intValue];
   // NSLog(@"dealCatID :%d",objAllDealList.dealCatID);

    }
        
    if([dic objectForKey:@"min"] != [NSNull null]){
    objAllDealList.dealMin = [[dic objectForKey:@"min"] intValue];
   // NSLog(@"dealMin :%d",objAllDealList.dealMin);

    }
        
    if([dic objectForKey:@"offer_price"] != [NSNull null]){
    objAllDealList.dealOfferPrice = [dic objectForKey:@"offer_price"];
    NSLog(@"offer_price :%@",objAllDealList.dealOfferPrice);
            
    }
        
        
    if([dic objectForKey:@"of_id"] != [NSNull null]){
    objAllDealList.dealOfferID = [[dic objectForKey:@"of_id"] intValue];
   // NSLog(@"dealOfferID :%d",objAllDealList.dealOfferID);
  
    }
        
    if([dic objectForKey:@"by_voucher"] != [NSNull null]){
    objAllDealList.dealByVoucher = [dic objectForKey:@"by_voucher"];
            // NSLog(@"dealMin :%d",objAllDealList.dealMin);
            
    }
        
    if([dic objectForKey:@"of_name"] != [NSNull null]){
    NSString *offerNameValue  =  [NSString handleNull:[[dic objectForKey:@"of_name"] stringByReplacingOccurrencesOfString:@"'" withString:@"%27"] ];
    objAllDealList.dealOfferName = offerNameValue; //[dic objectForKey:@"of_name"];
  //  NSLog(@"dealOfferName :%@",objAllDealList.dealOfferName);

    }
        
    if([dic objectForKey:@"valid"] != [NSNull null]){
    objAllDealList.dealValid = [NSString handleNull:[dic objectForKey:@"valid"] ];
  //  NSLog(@"dealValid :%@",objAllDealList.dealValid);

    }
        
    if([dic objectForKey:@"desc"] != [NSNull null]){
    NSString *dealDescValue  =  [NSString handleNull:[[dic objectForKey:@"desc"] stringByReplacingOccurrencesOfString:@"'" withString:@"%27"] ];
    objAllDealList.dealDescription = dealDescValue;//[dic objectForKey:@"desc"];
  //  NSLog(@"dealDescription :%@",objAllDealList.dealDescription);

    }
       
    if([dic objectForKey:@"offer_desc"] != [NSNull null]){
     NSString *dealOfferValue  =  [NSString handleNull:[[dic objectForKey:@"offer_desc"] stringByReplacingOccurrencesOfString:@"'" withString:@"%27"] ];
    objAllDealList.dealOfferDescription = dealOfferValue; //[dic objectForKey:@"offer_desc"];
    //NSLog(@"dealOfferDescription :%@",objAllDealList.dealOfferDescription);

    }
        
        
    if([dic objectForKey:@"dealer_contact"] != [NSNull null]){
    objAllDealList.dealerContact = [NSString handleNull:[dic objectForKey:@"dealer_contact"] ];
   // NSLog(@"dealerContact :%@",objAllDealList.dealerContact);

    }
    NSMutableArray *locArray = [dic valueForKey:@"locs"];
   // NSLog(@"TheLocArrayCount :%lu",locArray.count);
        
   /* if(locArray.count == 0){
    NSLog(@"NoLocationFound");
    }
    
    else{*/
    for(NSMutableDictionary *dataDic in locArray){
    DealLocationList *objDealLocList = [[DealLocationList alloc]init];
     
    if([dataDic objectForKey:@"lat"] != [NSNull null]){
    objDealLocList.dealLat = [NSString handleNull:[dataDic objectForKey:@"lat"] ];
  //  NSLog(@"dealLat :%@",objDealLocList.dealLat );

    }
        
    if([dataDic objectForKey:@"lng"] != [NSNull null]){
    objDealLocList.dealLong = [NSString handleNull:[dataDic objectForKey:@"lng"] ];
  //  NSLog(@"dealLong :%@",objDealLocList.dealLong );
    }
        
    if([dataDic objectForKey:@"addr"] != [NSNull null]){
    NSString *dealAddressValue  =  [NSString handleNull:[[dataDic objectForKey:@"addr"] stringByReplacingOccurrencesOfString:@"'" withString:@"%27"] ];
    objDealLocList.dealAddress = dealAddressValue; //[dataDic objectForKey:@"addr"];
  //  NSLog(@"addr :%@",objDealLocList.dealAddress );

    }
        
    if([dataDic objectForKey:@"city"] != [NSNull null]){
    NSString *dealCityValue  =  [NSString handleNull:[[dataDic objectForKey:@"city"] stringByReplacingOccurrencesOfString:@"'" withString:@"%27"] ];
    objDealLocList.dealCity = dealCityValue; //[dataDic objectForKey:@"city"];
   // NSLog(@"dealCity :%@",objDealLocList.dealCity );

    }
        
    if([dataDic objectForKey:@"state"] != [NSNull null]){
    NSString *dealStateValue  =  [NSString handleNull:[[dataDic objectForKey:@"state"] stringByReplacingOccurrencesOfString:@"'" withString:@"%27"] ];
    objDealLocList.dealState = dealStateValue; //[dataDic objectForKey:@"state"];
   // NSLog(@"state :%@",objDealLocList.dealState );

    }
        
    NSLog(@"#CheckArea :%@",[dataDic objectForKey:@"area"]);
        
    if([dataDic objectForKey:@"area"] != [NSNull null]){
    NSString *dealAreaValue  =  [NSString handleNull:[[dataDic objectForKey:@"area"] stringByReplacingOccurrencesOfString:@"'" withString:@"%27"] ];
    objDealLocList.dealArea = dealAreaValue; //[dataDic objectForKey:@"area"];
     //   NSLog(@"dealArea :%@",objDealLocList.dealArea );

    }
        
        
    if([dataDic objectForKey:@"full_address"] != [NSNull null]){
    NSString *dealFullAddressValue  = [NSString handleNull:[[dataDic objectForKey:@"full_address"] stringByReplacingOccurrencesOfString:@"'" withString:@"%27"] ];
    objDealLocList.dealFullAddress = dealFullAddressValue; //[dataDic objectForKey:@"full_address"];
      //  NSLog(@"dealFullAddress :%@",objDealLocList.dealFullAddress );

    }
     
        
    if([dataDic objectForKey:@"dealer_contact"] != [NSNull null]){
    objDealLocList.dealerContactNum = [dataDic objectForKey:@"dealer_contact"];
      //  NSLog(@"dealerContactNum :%@",objDealLocList.dealerContactNum );

    }
   // NSLog(@">>>>>>>>>>>>>>>>>>>>>>> %@",objDealLocList.dealAddress);

        
    [objAllDealList.locationArrayList addObject:objDealLocList];

    }
  //  }
        
    /*NSLog(@"ClassValue :%@",objAllDealList);
    if(objAllDealList.locationArrayList.count >0){
    [updatedResponse.allDealsArray addObject:objAllDealList];
    }
    else{
    NSLog(@"#TheClassIsNill");
    }*/
        
        [updatedResponse.allDealsArray addObject:objAllDealList];

    }

    return updatedResponse;
}

@end
