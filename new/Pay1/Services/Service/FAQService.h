//
//  FAQService.h
//  Pay1
//
//  Created by webninjaz on 16/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "Service.h"
@class FAQCriteria;
@class FAQResponse;

typedef void (^faqResponse)(NSError *error, FAQResponse *faqResponse);

@interface FAQService : Service

-(void)callFAQService:(FAQCriteria *)criteria callback:(faqResponse)callback;

@end
