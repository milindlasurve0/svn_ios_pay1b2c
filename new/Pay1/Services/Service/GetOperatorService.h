//
//  GetOperatorService.h
//  Pay1
//
//  Created by Annapurna on 13/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "Service.h"
@class GetOperatorResponse;
@class QuickPayResponse;
@class EditQuickPayCriteria;

typedef void (^getOeratorResponse)(NSError *error,GetOperatorResponse *objGetOperatorResponse);
typedef void (^quickPayResponse)(NSError *error,QuickPayResponse *objQuickResponse);
typedef void (^editQuickPayRessponse)(NSError *error, QuickPayResponse *objEditQuickResponse);
@interface GetOperatorService : Service

-(void)callGetOperatorService:(getOeratorResponse)callback;
-(void)callQuickPayService:(quickPayResponse)callback;
-(void)callEditQuickPayService:(EditQuickPayCriteria *)criteria callback:(editQuickPayRessponse)callback;
@end
