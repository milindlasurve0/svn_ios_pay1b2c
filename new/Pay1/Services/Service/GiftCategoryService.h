//
//  GiftCategoryService.h
//  Pay1
//
//  Created by Annapurna on 05/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "Service.h"
@class GiftCategoryResponse;
@class GiftCriteria;
typedef void (^giftCatResponse)(NSError *error,GiftCategoryResponse *objGiftCatResponse);

@interface GiftCategoryService : Service
-(void)callGiftCategoryService:(GiftCriteria *)criteria callback:(giftCatResponse)callback;
@end
