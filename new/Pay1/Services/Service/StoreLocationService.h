//
//  StoreLocationService.h
//  Pay1
//
//  Created by Annapurna on 10/08/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "Service.h"
@class StoreLocationCriteria;
@class StoreLocationResponse;
typedef void (^storeLocationResponse)(NSError *error, StoreLocationResponse *objStoreResponse);
@interface StoreLocationService : Service
-(void)callStoreLocationService:(StoreLocationCriteria *)storeCritera callback:(storeLocationResponse)callback;
@end
