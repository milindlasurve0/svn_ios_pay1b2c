//
//  FAQService.m
//  Pay1
//
//  Created by webninjaz on 16/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "FAQService.h"
#import "FAQCriteria.h"
#import "FAQResponse.h"
#import "FAQDataList.h"
#import "NSString+Utility.h"

@implementation FAQService

-(void)callFAQService:(FAQCriteria *)criteria callback:(faqResponse)callback{
    
    NSMutableDictionary *postData = [[NSMutableDictionary alloc]init];
    [postData setObject:criteria.faqParamCriteria forKey:@"Param"];
    [postData setObject:criteria.faqReqCriteria forKey:@"req"];
    
    NSLog(@"Post data :%@",postData);
    
    RequestData *requestData = [[RequestData alloc]init];
    requestData.baseURL = kBaseURL;
    requestData.webServiceURL = kFAQRequestURL;
    requestData.postData = postData;
    requestData.showLoader = FALSE;
    
    requestData.requestSuccessBlock = ^(RequestData *request) {
        
        [self printResponse:request.responseData forRequest:@"The Faq Response"];
        if ([self handleNullResponse:request])
            return;
        
        callback(nil, [self parseFAQResponse:request]);
    };
    
    
    requestData.requestFailureBlock = ^(RequestData *request) {
        [self handleNetworkError:request.responseData];
        callback(request.responseData, nil);
    };
    
    [[ServiceManager sharedInstance] makeRequest:requestData];

}


-(FAQResponse *)parseFAQResponse:(RequestData *)request{
    FAQResponse *faqResponse = [[FAQResponse alloc]init];
    faqResponse.statusFAQResponse =  [request.responseData objectForKey:@"status"];
    faqResponse.errorCodeFAQResponse = [request.responseData objectForKey:@"errCode"];
    NSMutableArray *dic = [request.responseData valueForKey:@"description"];
    for (NSMutableDictionary *dataDic in dic){
        FAQDataList *faqDataList = [[FAQDataList alloc]init];
        faqDataList.faqDataID = [NSString handleNull:[dataDic objectForKey:@"id"]];
        faqDataList.faqDataQuestion = [NSString handleNull:[dataDic objectForKey:@"question"]];
        faqDataList.faqDataAnswer = [NSString handleNull:[dataDic objectForKey:@"answer"]];
        NSLog(@"The answer is%@",faqDataList.faqDataAnswer);
        faqDataList.faqDataVisible = [NSString handleNull:[dataDic objectForKey:@"visible"]];
        [faqResponse.descriptionResponse addObject:faqDataList];
    }
    return faqResponse;
}

@end
