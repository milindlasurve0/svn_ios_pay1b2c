//
//  WalletHistoryService.h
//  Pay1
//
//  Created by webninjaz on 19/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "Service.h"
@class WalletHistoryCriteria;
@class WalletHistoryResponse;

typedef void (^walletHistoryResponse)(NSError *error, WalletHistoryResponse *walletHistoryResponse);

@interface WalletHistoryService : Service

-(void)callWalletHistoryService:(WalletHistoryCriteria *)criteria callback:(walletHistoryResponse)callback;

@end
