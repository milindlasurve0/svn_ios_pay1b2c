//
//  GiftsService.h
//  Pay1
//
//  Created by Annapurna on 16/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "Service.h"
@class GiftCriteria;
@class GiftResponse;

typedef void (^giftResponse)(NSError *error,GiftResponse *objGiftResponse);
@interface GiftsService : Service

//-(void)callGiftService:(giftResponse)callback;

-(void)callGiftService:(GiftCriteria *)criteria callback:(giftResponse)callback;


@end
