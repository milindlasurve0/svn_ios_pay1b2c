//
//  CreateUserService.h
//  Pay1
//
//  Created by Annapurna on 11/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "Service.h"
@class CreateUserResponse;
typedef void (^createUserResponse) (NSError *error, CreateUserResponse *objCreateUserResponse);

@interface CreateUserService : Service

-(void)callCreateUserService:(NSString *)mobileNumber callback:(createUserResponse)callback;

@end
