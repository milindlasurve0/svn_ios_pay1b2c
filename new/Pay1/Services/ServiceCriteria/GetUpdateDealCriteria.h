//
//  GetUpdateDealCriteria.h
//  Pay1
//
//  Created by Annapurna on 09/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetUpdateDealCriteria : NSObject
@property (nonatomic, strong) NSString *userMobileNum;
@property (nonatomic, strong) NSString *userLat;
@property (nonatomic, strong) NSString *userLong;
@property (nonatomic, strong) NSString *updatedTime;
@property (nonatomic, strong) NSString *nextValue;



@end
