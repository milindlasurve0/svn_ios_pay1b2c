//
//  EditQuickPayCriteria.h
//  Pay1
//
//  Created by Annapurna on 14/07/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EditQuickPayCriteria : NSObject

@property (nonatomic, strong) NSString *mobileNumber;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *amount;
@property (nonatomic, strong) NSString *operatorID;
@property (nonatomic, strong) NSString *delagteFlag;
@property (nonatomic, strong) NSString *flag;
@property (nonatomic, strong) NSString *quickID;
@property (nonatomic, strong) NSString *quickSTV;
@property (nonatomic, strong) NSString *confirmation;



@end
