//
//  ProfileUpdateCriteria.h
//  Pay1
//
//  Created by webninjaz on 11/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProfileUpdateCriteria : NSObject

@property (nonatomic, strong) NSString *profileUpdateName;
@property (nonatomic, strong) NSString *profileUpdateEmail;
@property (nonatomic, strong) NSString *profileUpdateGender;
@property (nonatomic, strong) NSString *profileUpdateDateOfBirth;
@property (nonatomic, strong) NSString *profileUpdateUDID;
@property (nonatomic, strong) NSString *profileUpdateDeviceType;
@property (nonatomic, strong) NSString *profileUpdatePassword;
@property (nonatomic, assign) BOOL isSendPassword;


@end
