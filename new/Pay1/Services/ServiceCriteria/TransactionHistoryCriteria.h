//
//  TransactionHistoryCriteria.h
//  Pay1
//
//  Created by webninjaz on 09/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransactionHistoryCriteria : NSObject

@property (nonatomic,strong) NSString *transactionParamCriteria;
@property (nonatomic,strong) NSString *transactionPageCriteria;
@property (nonatomic,strong) NSString *transactionLimitCriteria;
@property (nonatomic,strong) NSString *transactionAPIVersionCriteria;

@end
