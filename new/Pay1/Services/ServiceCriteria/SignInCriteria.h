//
//  SignInCriteria.h
//  Pay1
//
//  Created by Annapurna on 12/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SignInCriteria : NSObject

@property (nonatomic, strong) NSString *userMobNum;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *udid;
@property (nonatomic, strong) NSString *userLat;
@property (nonatomic, strong) NSString *userLong;
@property (nonatomic, strong) NSString *deviceType;


@end
