//
//  UpdatePasswordCriteria.h
//  Pay1
//
//  Created by Annapurna on 12/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpdatePasswordCriteria : NSObject

@property (nonatomic, strong) NSString *userMobNum;
@property (nonatomic, strong) NSString *userPassword;
@property (nonatomic, strong) NSString *confirmPassword;
@property (nonatomic, strong) NSString *otpNum;
@property (nonatomic, strong) NSString *currentPassword;




@end
