//
//  GiftCriteria.h
//  Pay1
//
//  Created by Annapurna on 11/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GiftCriteria : NSObject
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *next;
@end
