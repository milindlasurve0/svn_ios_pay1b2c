//
//  FAQCriteria.h
//  Pay1
//
//  Created by webninjaz on 16/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FAQCriteria : NSObject

@property (nonatomic, strong) NSString *faqParamCriteria;
@property (nonatomic, strong) NSString *faqReqCriteria;

@end
