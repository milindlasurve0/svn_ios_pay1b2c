//
//  AppDelegate.h
//  Pay1
//
//  Created by Annapurna on 02/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate>{
   
}

@property (strong, nonatomic) UIWindow *window;

@property(nonatomic,strong)UITabBarController *tabBarController;
@property(nonatomic,strong) UINavigationController *navController;



-(void)loginDone;
-(void)doLogout;


//spatial-victory-95011

@end

