//
//  ProfileViewController.h
//  Pay1
//
//  Created by webninjaz on 16/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"
@class DatePickerView;
@class PhotoPicker;
@class TPKeyboardAvoidingScrollView;

@interface ProfileViewController : BaseViewController<UITextFieldDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate>{
    
    IBOutlet UIButton *maleGenderButton;
    IBOutlet UIButton *femaleGenderButton;
    IBOutlet UIButton *datePickerButton;

    IBOutlet UIButton *penImgView;
    IBOutlet UIImageView *picImageView;
    
    IBOutlet UITextField *nameTextField;
    IBOutlet UITextField *dateOfBirthTextField;
    IBOutlet UITextField *emailTextField;
    IBOutlet UITextField *mobileNumberTextField;
    IBOutlet UITextField *resetPasswordTextField;
    
    IBOutlet UILabel *maleLabel;
    IBOutlet UILabel *femaleLabel;
    IBOutlet UILabel *genderLabel;
    IBOutlet UILabel *nameLabel;
    IBOutlet UILabel *genderTextLabel;
    IBOutlet UILabel *dateOfBirthLabel;
    IBOutlet UILabel *emailLabel;
    IBOutlet UILabel *mobileNumberLabel;
    IBOutlet UILabel *resetPasswordLabel;
    DatePickerView *datePicker;
    IBOutlet UIImageView *firstThinImage;
    IBOutlet UIImageView *secondThinImage;
    IBOutlet UIImageView *thirdThinImage;
    IBOutlet UIImageView *fourthThinImage;
    IBOutlet UIImageView *fifthThinImage;
    IBOutlet UIImageView *sixthThinImage;
    IBOutlet UIImageView *seventhThinImage;
    UIBarButtonItem *rightBarButtonItem;
    
    
    NSMutableData *webData;
    NSMutableDictionary *dicValues;
    NSMutableDictionary *parsedJson;
    IBOutlet PhotoPicker *objPhotoPicker;
    BOOL photoSelected;
    NSData *imgData;
    IBOutlet UIImageView *img;
    UIPopoverController *popOverController;
    
    UIView *coverView;
    
    UIBarButtonItem *rightButton;
    
    IBOutlet UIView *chnagePasswordView;
    
    BOOL isSaveData;
    BOOL isUserLoginData;
    
    IBOutlet UIButton *dobCoverButton;
}

@property (nonatomic,strong) IBOutlet UIImageView *profileImageView;
@property (nonatomic, strong) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (nonatomic,assign) BOOL isFromFB;

@end
