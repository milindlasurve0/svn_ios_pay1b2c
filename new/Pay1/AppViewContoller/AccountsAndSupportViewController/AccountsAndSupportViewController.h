//
//  AccountsAndSupportViewController.h
//  Pay1
//
//  Created by webninjaz on 16/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"

@interface AccountsAndSupportViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>{
    
    IBOutlet UITableView *accountsAndSupportTableView;
    NSMutableArray *accountsAndSupportArray;
}

@end
