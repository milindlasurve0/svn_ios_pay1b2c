//
//  PasswordViewController.h
//  Pay1
//
//  Created by Annapurna on 04/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"
#import <CommonCrypto/CommonDigest.h>

@class TPKeyboardAvoidingScrollView;
@class CreateUserResponse;
@interface PasswordViewController : BaseViewController<UITextFieldDelegate>{
    IBOutlet UITextField *passwordTxtField;
    IBOutlet UILabel *mobileNumLabel;
    
    IBOutlet UIButton *loginButton;
    IBOutlet UIButton *forgotPswdButton;
    
    IBOutlet UIImageView *passwordImgView;
    IBOutlet UIView *sepView;
    
    
  
    IBOutlet UIImageView *logoImageView;
    
    IBOutlet UIView *numberDetailView;
    
    IBOutlet UIImageView *backGroundImgView;
    IBOutlet UIImageView *thinImageView;
    
    IBOutlet UITextField *numberErrorTxtView;
    
    IBOutlet UIImageView *numberErrorImgView;
    
    
    
    NSMutableArray *updatedDealsArrayList;
    
    IBOutlet UILabel *passwordTxtLabel;
    IBOutlet UILabel *timeDecrementLabel;
    
    NSTimer *timer;
    int currMinute;
    int currSeconds;
    
   IBOutlet UIButton  *resendButton;
    
    int resendSMSCount;
    
}
@property (nonatomic, strong) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (nonatomic, strong) CreateUserResponse *createUserResponseObj;
@property (nonatomic, strong) NSString *userPhoneNumber;
@property (nonatomic, strong) NSString *userOTP;




@end
