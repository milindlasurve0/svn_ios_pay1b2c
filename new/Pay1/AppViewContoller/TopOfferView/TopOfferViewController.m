//
//  TopOfferViewController.m
//  Pay1
//
//  Created by Annapurna on 11/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "TopOfferViewController.h"
#import "OfferDetailList.h"
#import "DBManager.h"
#import "TopOfferCell.h"
#import "UIImage+Utility.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "UIImageView+WebCache.h"
#import "GiftDetailViewController.h"
#import "MyLikeService.h"
#import "MyLikeResponse.h"
#import "GiftsService.h"
#import "GiftCriteria.h"
#import "GiftResponse.h"
#import "GiftList.h"
#import "GiftCategoryService.h"
#import "GiftCategoryResponse.h"
#import "GiftCategoryList.h"
@interface TopOfferViewController ()

@end

@implementation TopOfferViewController

- (void)viewDidLoad {
    isLikedClicked = FALSE;
    topNextValue = 0;
    
    initialPosition = 1000;
    
    
    if([UIScreen mainScreen].bounds.size.width == 320){
        
        topOfferTV.frame = CGRectMake(0, 64, 320, [UIScreen mainScreen].bounds.size.height - 64);
    }
    
    noGiftFoundLabel.hidden = TRUE;
    
    NSLog(@"Values :%@ %@ %@ %@",_giftOfferID,_giftOfferName,_searchKeyWord,_giftOfferTextID);
    
    
    imageURLArray = [[NSMutableArray alloc]init];
    imageThumbNailArray = [[NSMutableArray alloc]init];
    shopNameArray = [[NSMutableArray alloc]init];
    priceArray = [[NSMutableArray alloc]init];
    offerDescArray = [[NSMutableArray alloc]init];
    dealArray = [[NSMutableArray alloc]init];
    offerArray = [[NSMutableArray alloc]init];
    likesArray = [[NSMutableArray alloc]init];
    offerPriceArray = [[NSMutableArray alloc]init];
    
    offerDisplayArray = [[NSMutableArray alloc]init];
    
    NSArray *myLikeArray = [[Session shared].objSignInResponse.offerLikes componentsSeparatedByString:@","];
    
    if(myLikeArray.count > 0){
        for(int i=0;i<myLikeArray.count;i++){
            [likesArray addObject:[myLikeArray objectAtIndex:i]];
        }
    }
    
    
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    
    
    
    
    dbManager = [[DBManager alloc] initWithDatabaseFilename:@"dbgiftdata.sql"];
    
    
    NSString * likeQuery = [NSString stringWithFormat:@"SELECT FREEBIEOFFERID FROM GIFTTABLE WHERE FREEBIELIKE=\"%d\"",1];
    NSLog(@"MYLIKES :%@",[dbManager loadDataFromDB:likeQuery]);
    
    for(NSArray *arr in [dbManager loadDataFromDB:likeQuery]){
    NSLog(@"array :%@",[arr objectAtIndex:0]);
        
    [ likesArray addObject:[arr objectAtIndex:0]];
    }
    
    
   
    
     NSLog(@"#likesArray :%@",likesArray);
    
    
    if([Session shared].isFromSearch == TRUE){
        
   [offerDisplayArray removeAllObjects];
        
        [offerArray removeAllObjects];
        
        //offerDisplayArray = [[NSMutableArray alloc]init];

        
    [self setNaviationButtonWithText:@"SEARCH" selector:@selector(backClicked:) isLeft:TRUE isReduceSize:FALSE isMyFont:18.0];

        
    for (id viewToRemove in [self.navigationController.navigationBar subviews]){
    if ([viewToRemove isMemberOfClass:[UISearchBar class]])
    [viewToRemove removeFromSuperview];
    }
        
    NSLog(@"IsFromSearch :%@",_searchKeyWord);
    NSString * query = [NSString stringWithFormat:@"select * from GIFTTABLE WHERE FREEBIEDEALNAME LIKE '%%%@%%'  OR FREEBIELAT LIKE '%%%@%%' OR FREEBIELNG LIKE '%%%@%%' OR FREEBIEADDRESS LIKE '%%%@%%' OR FREEBIEAREA LIKE '%%%@%%' OR FREEBIECITY LIKE '%%%@%%' OR FREEBIESTATE LIKE '%%%@%%' OR FREEBIEOFFERNAME LIKE '%%%@%%'",_searchKeyWord,_searchKeyWord,_searchKeyWord,_searchKeyWord,_searchKeyWord,_searchKeyWord,_searchKeyWord,_searchKeyWord];
        
    NSLog(@"#Query is :%@",query);
    [dbManager executeQuery:query];
        
    
        
        
        
        
    NSArray *theArray = [[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]];
    NSLog(@"theArray :%@ %lu",theArray,(unsigned long)theArray.count);
        
    if(theArray.count == 0){
    noGiftFoundLabel.hidden = FALSE;
    topOfferTV.hidden = TRUE;
    }
      
    else{
    NSInteger indexOfImageURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEURL"];
    for(int i=0;i<theArray.count;i++){
    [imageURLArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfImageURL]];
    }
        
    NSInteger indexOfShopNameURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEDEALNAME"];
    for(int i=0;i<theArray.count;i++){
    [shopNameArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfShopNameURL]];
    }
    NSInteger indexOfAreaURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIELOGOURL"];
    for(int i=0;i<theArray.count;i++){
    [imageThumbNailArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfAreaURL]];
    }
    NSInteger indexOfOfferURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIESHORTDESC"];
    for(int i=0;i<theArray.count;i++){
    [offerDescArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOfferURL]];
    }
    NSInteger indexOfPriceURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEMINAMOUNT"];
    for(int i=0;i<theArray.count;i++){
    [priceArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfPriceURL]];
    }
        
    NSInteger indexOfDeal = [dbManager.arrColumnNames indexOfObject:@"FREEBIEDEALID"];
    for(int i=0;i<theArray.count;i++){
    [dealArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfDeal]];
    }
        
    NSInteger indexOfOffer = [dbManager.arrColumnNames indexOfObject:@"FREEBIEOFFERID"];
    for(int i=0;i<theArray.count;i++){
    [offerArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOffer]];
    }
    NSInteger indexOfOfferPriceURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEOFFERPRICE"];
    for(int i=0;i<theArray.count;i++){
    [offerPriceArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOfferPriceURL]];
    }
    }
        
        
        for(int i=0;i<offerArray.count;i++){
            [offerDisplayArray addObject:[offerArray objectAtIndex:i]];
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [topOfferTV reloadData];
        });
        
    }
    
    
    else{
        
        [self setNaviationButtonWithText:_giftOfferName selector:@selector(backClicked:) isLeft:TRUE isReduceSize:FALSE isMyFont:18.0];

        
        NSLog(@"In Else");
        
        if(!isLikedClicked && !_isComingFrom)
        [self callGiftTopOfferAPIService];
        else if(_isComingFrom && !isLikedClicked)
        [self callGiftCategoryService];
        
        
        
        
        
    }
         
        
   

    
}

-(void)callGiftCategoryService{
    [self showHUDOnView];
    GiftCategoryService *service = [[GiftCategoryService alloc]init];
    [service callGiftCategoryService:self.giftCriteria callback:^(NSError *error, GiftCategoryResponse *objGiftCatResponse) {
        if(error)return ;
        else{
            if(objGiftCatResponse.giftCatDataArray.count >0){
                isToCallWebService = TRUE;
                GiftList *objList = [objGiftCatResponse.giftCatDataArray objectAtIndex:0];
                offerIDArray= [objList.details componentsSeparatedByString:@","];
                
                for(int i=0;i<offerIDArray.count;i++){
                    [offerDisplayArray addObject:[offerIDArray objectAtIndex:i]];
                }
                for(int i=0;i<offerIDArray.count;i++){
                    NSString *offserValue = [offerIDArray objectAtIndex:i];
                    NSString * query = [NSString stringWithFormat:@"select * from GIFTTABLE WHERE FREEBIEOFFERID=\"%@\"",offserValue];
                    
                    NSArray *theArray = [[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]];
                    
                    
                    NSInteger indexOfImageURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEURL"];
                    for(int i=0;i<theArray.count;i++){
                        [imageURLArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfImageURL]];
                    }
                    
                    NSInteger indexOfShopNameURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEDEALNAME"];
                    for(int i=0;i<theArray.count;i++){
                        [shopNameArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfShopNameURL]];
                    }
                    NSInteger indexOfAreaURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIELOGOURL"];
                    for(int i=0;i<theArray.count;i++){
                        [imageThumbNailArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfAreaURL]];
                    }
                    NSInteger indexOfOfferURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIESHORTDESC"];
                    for(int i=0;i<theArray.count;i++){
                        [offerDescArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOfferURL]];
                    }
                    NSInteger indexOfPriceURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEMINAMOUNT"];
                    for(int i=0;i<theArray.count;i++){
                        [priceArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfPriceURL]];
                    }
                    
                    NSInteger indexOfDeal = [dbManager.arrColumnNames indexOfObject:@"FREEBIEDEALID"];
                    for(int i=0;i<theArray.count;i++){
                        [dealArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfDeal]];
                    }
                    
                    
                    NSInteger indexOfOffer = [dbManager.arrColumnNames indexOfObject:@"FREEBIEOFFERID"];
                    for(int i=0;i<theArray.count;i++){
                        [offerArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOffer]];
                    }
                    
                    NSInteger indexOfOfferPriceURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEOFFERPRICE"];
                    for(int i=0;i<theArray.count;i++){
                        [offerPriceArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOfferPriceURL]];
                    }
                }
            }
            else{
                NSLog(@"NoOffers");
                isToCallWebService = FALSE;
            }
        }
        
        [topOfferTV reloadData];
        [self hideHUDOnView];
    }];

    
        
   // }];
}


-(void)callGiftTopOfferAPIService{
   // [offerDisplayArray removeAllObjects];
    
    [self showHUDOnView];
    GiftsService *giftService = [[GiftsService alloc]init];
    [giftService callGiftService:self.giftCriteria callback:^(NSError *error, GiftResponse *objGiftResponse) {
        if(error)return ;
        else{
            
            if(objGiftResponse.giftArrayList.count >0){
                isToCallWebService = TRUE;
            GiftList *objList = [objGiftResponse.giftArrayList objectAtIndex:0];
            offerIDArray= [objList.details componentsSeparatedByString:@","];
                
                for(int i=0;i<offerIDArray.count;i++){
                    [offerDisplayArray addObject:[offerIDArray objectAtIndex:i]];
                }
            for(int i=0;i<offerIDArray.count;i++){
            NSString *offserValue = [offerIDArray objectAtIndex:i];
            NSString * query = [NSString stringWithFormat:@"select * from GIFTTABLE WHERE FREEBIEOFFERID=\"%@\"",offserValue];
                
            NSArray *theArray = [[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]];
                
                
            NSInteger indexOfImageURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEURL"];
            for(int i=0;i<theArray.count;i++){
            [imageURLArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfImageURL]];
            }
                
                NSInteger indexOfShopNameURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEDEALNAME"];
                for(int i=0;i<theArray.count;i++){
                    [shopNameArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfShopNameURL]];
                }
                NSInteger indexOfAreaURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIELOGOURL"];
                for(int i=0;i<theArray.count;i++){
                    [imageThumbNailArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfAreaURL]];
                }
                NSInteger indexOfOfferURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIESHORTDESC"];
                for(int i=0;i<theArray.count;i++){
                    [offerDescArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOfferURL]];
                }
                NSInteger indexOfPriceURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEMINAMOUNT"];
                for(int i=0;i<theArray.count;i++){
                    [priceArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfPriceURL]];
                }
                
                NSInteger indexOfDeal = [dbManager.arrColumnNames indexOfObject:@"FREEBIEDEALID"];
                for(int i=0;i<theArray.count;i++){
                    [dealArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfDeal]];
                }
                
                
                NSInteger indexOfOffer = [dbManager.arrColumnNames indexOfObject:@"FREEBIEOFFERID"];
                for(int i=0;i<theArray.count;i++){
                    [offerArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOffer]];
                }
                
                NSInteger indexOfOfferPriceURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEOFFERPRICE"];
                for(int i=0;i<theArray.count;i++){
                    [offerPriceArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOfferPriceURL]];
                }
            }
        }
            else{
                NSLog(@"NoOffers");
                isToCallWebService = FALSE;
            }
        }
        
        [topOfferTV reloadData];
        [self hideHUDOnView];
    }];
  
}

-(GiftCriteria *)giftCriteria{
    GiftCriteria *criteria = [[GiftCriteria alloc]init];
    criteria.next  =  [NSString stringWithFormat:@"%d",topNextValue];
//@"0";
    criteria.type = _giftOfferTextID;
    return criteria;
}

#pragma mark - TableView Methods.

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return offerDisplayArray.count; //imageURLArray.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier1 = @"TopOfferCell";
    objTopOfferCell = (TopOfferCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
    
    if(objTopOfferCell==nil){
    [[NSBundle mainBundle] loadNibNamed:@"TopOfferCell" owner:self options:nil];
    }
    
    
    TopOfferViewController *vc = self;
    
    [objTopOfferCell.offerImageView setImageWithURL:[NSURL URLWithString:[imageURLArray objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"deal_default.png"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
    UIImage *newImage = [UIImage imageWithImage:image  scaledToSize:CGSizeMake(320,212)];
    objTopOfferCell.offerImageView.image = newImage;
    }usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)UIActivityIndicatorViewStyleGray];
   
    [objTopOfferCell.offerThumbNailImageView setImageWithURL:[NSURL URLWithString:[imageThumbNailArray objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"deal_default.png"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
    UIImage *newImage = [UIImage imageWithImage:image  scaledToSize:CGSizeMake(320,212)];
    objTopOfferCell.offerThumbNailImageView.image = newImage;
    }usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)UIActivityIndicatorViewStyleGray];
    
    objTopOfferCell.offerPointLabel.text = [priceArray objectAtIndex:indexPath.row];
    
    objTopOfferCell.offerDetailLabel.text = [offerDescArray objectAtIndex:indexPath.row];
    objTopOfferCell.offerDetailLabel.numberOfLines = 0;
    [objTopOfferCell.offerDetailLabel sizeToFit];
    
    
    objTopOfferCell.tag  =  [[offerDisplayArray objectAtIndex:indexPath.row] integerValue]; //[[offerIDArray objectAtIndex:indexPath.row] integerValue];
    
    
    for(int i=0;i<likesArray.count;i++){
        if(objTopOfferCell.tag == [[likesArray objectAtIndex:i] integerValue]){
            // if([[offerArray objectAtIndex:i] integerValue] == [[likesArray objectAtIndex:i] integerValue]){
            //  NSLog(@"++++++");
            [objTopOfferCell.heartButton setBackgroundImage:[UIImage imageNamed:@"red_heart.png"] forState:UIControlStateNormal];
            
        }
        
    }
    
  /*  objTopOfferCell.tag  =  [[offerDisplayArray objectAtIndex:indexPath.row] integerValue]; //[[offerIDArray objectAtIndex:indexPath.row] integerValue];
    
    
    for(int i=0;i<likesArray.count;i++){
    if(objTopOfferCell.tag == [[likesArray objectAtIndex:i] integerValue]){
        // if([[offerArray objectAtIndex:i] integerValue] == [[likesArray objectAtIndex:i] integerValue]){
  //  NSLog(@"++++++");
    [objTopOfferCell.heartButton setBackgroundImage:[UIImage imageNamed:@"red_heart.png"] forState:UIControlStateNormal];
            
    }

    }*/
    
    NSString *name = [[shopNameArray objectAtIndex:indexPath.row] stringByReplacingOccurrencesOfString:@"%27" withString:@"'"];
    objTopOfferCell.offerNameLabel.text = name; //[shopNameArray objectAtIndex:indexPath.row];
    
    if(![[offerPriceArray objectAtIndex:indexPath.row]isEqualToString:@"0"]){
        NSString *ofPrice = [offerPriceArray objectAtIndex:indexPath.row];
        
         objTopOfferCell.rupeeLabel.text = [NSString stringWithFormat:@"%@",ofPrice];
        
        if(ofPrice.length > 3){
        objTopOfferCell.rupeeImageView.frame = CGRectMake(12, 12, 18, 18);
        objTopOfferCell.rupeeLabel.frame = CGRectMake(32, 9, 38, 21);
        objTopOfferCell.plusImageView.frame = CGRectMake(75, 16, 10, 10);
        }
        
        if(ofPrice.length == 3){
            objTopOfferCell.rupeeImageView.frame = CGRectMake(22, 12, 18, 18);
            objTopOfferCell.rupeeLabel.frame = CGRectMake(43, 9, 38, 21);
            objTopOfferCell.plusImageView.frame = CGRectMake(72, 16, 10, 10);
        }

        else{
            objTopOfferCell.rupeeImageView.frame = CGRectMake(25, 12, 18, 18);

        objTopOfferCell.rupeeLabel.frame = CGRectMake(45, 9, 38, 21);
        objTopOfferCell.plusImageView.frame = CGRectMake(72, 16, 10, 10);
        }
        
        //[NSString stringWithFormat:@"%@",[offerPriceArray objectAtIndex:indexPath.row]];

        [objTopOfferCell.rupeeLabel sizeToFit];
    }
    else{
        objTopOfferCell.rupeeLabel.hidden = TRUE;
        objTopOfferCell.rupeeImageView.hidden = TRUE;
        objTopOfferCell.plusImageView.hidden = TRUE;
    }
    
    
    
  
    
    [objTopOfferCell setOnHeartClick:^{
    [vc callMyLikeService:indexPath.row];
    }];
    
    
    
    return objTopOfferCell;

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 270;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    GiftDetailViewController *giftDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"giftDetailViewC"];
    giftDetailVC.giftID = [dealArray objectAtIndex:indexPath.row];
    giftDetailVC.giftOfferID = [offerArray objectAtIndex:indexPath.row];
    giftDetailVC.shopName = [shopNameArray objectAtIndex:indexPath.row];
    giftDetailVC.hidesBottomBarWhenPushed = TRUE;
    [self.navigationController pushViewController:giftDetailVC animated:YES];
}
/*
- (void)scrollViewDidEndDragging:(UIScrollView *)aScrollView willDecelerate:(BOOL)decelerate
{
    
    CGPoint offset = aScrollView.contentOffset;
    NSLog(@"#TableScrolled %f",offset.y);

    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = 30;
    if(y > h + reload_distance) {
        
        topNextValue ++;
          [self callGiftTopOfferAPIService];

        
    }
    
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pointNow = scrollView.contentOffset;
    NSLog(@"#PointNow :%f %f",pointNow.x,pointNow.y);

}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.y<pointNow.y) {
        
      //  NSLog(@"UP");
       isMovingDirection = FALSE;
    }
    
    else if (scrollView.contentOffset.y>pointNow.y) {
        
        // NSLog(@"DOWN");
        topNextValue ++;
        


        if(!isMovingDirection){
            isMovingDirection = TRUE;

        //[self callGiftTopOfferAPIService];
        }
    }
}*/

- (void)scrollViewDidScroll:(UIScrollView *)scrollView_
{
    if([Session shared].isFromSearch == FALSE){
    CGFloat actualPosition = scrollView_.contentOffset.y;
    
   // NSLog(@"ActualPosition :%f",actualPosition);
    
    if(actualPosition > initialPosition && isToCallWebService == TRUE){
        isMovingDirection = FALSE;
    }
    
     if (actualPosition > initialPosition && isMovingDirection == FALSE) {
     NSLog(@"------------------>");
         initialPosition = initialPosition +1000;
         isMovingDirection = TRUE;
        topNextValue ++;
         
         if(!_isComingFrom)
        [self callGiftTopOfferAPIService];
         else if(_isComingFrom)
        [self callGiftCategoryService];

       // [self callGiftTopOfferAPIService];
    }
    }
}

#pragma mark - MyLike Service

-(void)callMyLikeService:(NSInteger )index{
    isLikedClicked = TRUE;
    
    [likesArray removeAllObjects];
    
    MyLikeService *service = [[MyLikeService alloc]init];
    [service callMyLikeService:[offerArray objectAtIndex:index] callback:^(NSError *error, MyLikeResponse *likeResponse) {
    if(error)return ;
    else{
    if(likeResponse.likeStatus == TRUE){
    NSLog(@"Check It's Like");
    [self updateLikeContentInDB:index likeValue:@"1"];
    }
    if(likeResponse.likeStatus == FALSE){
    NSLog(@"Check It's UnLike");
    [self updateLikeContentInDB:index likeValue:@"0"];
    }
    else
    NSLog(@"Error!");
    }
    }];
}

-(void)updateLikeContentInDB:(NSInteger)index likeValue:(NSString *)likeValue{
 //  NSString * query = [NSString stringWithFormat:@"select * from GIFTSTABLE WHERE FREEBIEOFFERID=\"%@\"",offserValue];
    NSString *query = [NSString stringWithFormat:@"UPDATE GIFTTABLE SET FREEBIELIKE=\"%d\" WHERE FREEBIEOFFERID=\"%@\"",[likeValue intValue],[offerArray objectAtIndex:index]];
    NSLog(@"#QueryLikeUnLike :%@",query);
    [dbManager executeQuery:query];
    
    if (dbManager.affectedRows != 0) {
    NSLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
        //[delegate editingInfoWasFinished];
    }
    else{
        NSLog(@"Could not execute the query.");
    }
    [self viewWillAppear:TRUE];
    
    
}

-(void)backClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
