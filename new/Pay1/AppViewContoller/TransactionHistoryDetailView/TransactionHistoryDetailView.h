//
//  TransactionHistoryDetailView.h
//  Pay1
//
//  Created by webninjaz on 11/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"

@class TransactionList;

@interface TransactionHistoryDetailView : BaseViewController{
    IBOutlet UILabel *successLabel;
    IBOutlet UILabel *phoneNumberLabel;
    IBOutlet UILabel *moneyLabel;
    IBOutlet UILabel *transactionIDlabel;
    IBOutlet UILabel *transactionDateLabel;
    IBOutlet UILabel *modeOfPaymentLabel;
    
    IBOutlet UILabel *markLabel;
    
    
    IBOutlet UILabel *typeLabel;
    
    IBOutlet UILabel *postPaidTxtLabel;
    
}



@property (nonatomic,strong) TransactionList *transactionList;

@end
