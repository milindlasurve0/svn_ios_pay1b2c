//
//  TransactionHistoryDetailView.m
//  Pay1
//
//  Created by webninjaz on 11/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "TransactionHistoryDetailView.h"
#import "TransactionList.h"

@interface TransactionHistoryDetailView ()

@end

@implementation TransactionHistoryDetailView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    markLabel.hidden = TRUE;
    
    [self setNaviationButtonWithText:@"Transaction History" selector:@selector(backClicked:) isLeft:TRUE isReduceSize:TRUE isMyFont:18.0];

    NSLog(@"The transaction data is%@",self.transactionList.transactionPhoneNumber);
    NSLog(@"The transaction status is%@",self.transactionList.transactionStatus);
    
    
    
    
    NSLog(@"The money is%@",self.transactionList.transactionAmount);
    NSLog(@"The operator name is%@",self.transactionList.transactionOperatorName);
    NSLog(@"The amout list is%@",self.transactionList.transactionAmount);
    NSLog(@"The transaction id is%@",self.transactionList.transactionID);
    NSLog(@"The transaction time is%@",self.transactionList.transactionDateTime);
    NSLog(@"The transaction payment mode is%@",self.transactionList.transactionPaymentMode);
    
    phoneNumberLabel.text = self.transactionList.transactionPhoneNumber;
    successLabel.text = self.transactionList.transactionStatus;
    
    
    
    
    moneyLabel.text = [NSString stringWithFormat:@"Rs. %@",self.transactionList.transactionAmount];
    transactionIDlabel.text = self.transactionList.transactionID;
    transactionDateLabel.text = self.transactionList.transactionDateTime;
    modeOfPaymentLabel.text = self.transactionList.transactionPaymentMode;
    
    if(![_transactionList.paymentMode isEqual:[NSNull null]]){
        markLabel.hidden = FALSE;
        
        markLabel.text = _transactionList.paymentMode;
        
    }
    
    if([_transactionList.transactionTransCategory isEqualToString:@"refill"]){
        typeLabel.text = @"Wallet topup";
        
        [postPaidTxtLabel setHidden:TRUE];

        
        moneyLabel.frame = CGRectMake(16, phoneNumberLabel.frame.origin.y + phoneNumberLabel.frame.size.height + 25, 136, 21);

    }
    else{
        
        if([_transactionList.transactionTransCategory isEqualToString:@"billing"]){
            [postPaidTxtLabel setHidden:FALSE];

            
            postPaidTxtLabel.text = [NSString stringWithFormat:@"(%@ = %@ + %d service charge inclusive of all taxes.)",_transactionList.transactionAmount,_transactionList.transactionAmount,[_transactionList.transactionAmount intValue] - [_transactionList.transactionBaseAmount intValue]];
        }
        
        else{
            [postPaidTxtLabel setHidden:TRUE];
            
            moneyLabel.frame = CGRectMake(16, phoneNumberLabel.frame.origin.y + phoneNumberLabel.frame.size.height + 25, 136, 21);
        }

        
        typeLabel.text = _transactionList.transactionOperatorName; //@"Wallet topup";
 
    }

    
    
    
    if ([self.transactionList.transactionStatus isEqualToString:@"1"]) {
        successLabel.text = @"PENDING";
        successLabel.textColor = [UIColor greenColor];
    }
    
    if ([self.transactionList.transactionStatus isEqualToString:@"2"]) {
        successLabel.text = @"SUCCESS";
        successLabel.textColor = [UIColor greenColor];
    }
    
    if ([self.transactionList.transactionStatus isEqualToString:@"3"] || [self.transactionList.transactionStatus isEqualToString:@"4"]) {
        successLabel.text = @"FAILURE";
        successLabel.textColor = [UIColor redColor];
    }
    
    
    //if (self.transactionList.transactionOperatorName == (id)[NSNull null] || self.transactionList.transactionOperatorName.length == 0){
    
    
    // Do any additional setup after loading the view.
}


-(void)backClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
