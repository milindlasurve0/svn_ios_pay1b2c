//
//  OperatorListViewController.m
//  Pay1
//
//  Created by Annapurna on 15/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "OperatorListViewController.h"
#import "OperatorList.h"
#import "MainViewController.h"
#import "DBManager.h"
#import "UIImage+Utility.h"
#import "OperatorTableCell.h"
@interface OperatorListViewController ()

@end

@implementation OperatorListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    operatorTV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    
    operatorNameArray = [[NSMutableArray alloc]init];
    operatorImageArray = [[NSMutableArray alloc]init];
    
    [self hideNavBackButton];
    
    
    [self setNaviationButtonWithText:@"Operator" selector:@selector(backClicked:) isLeft:TRUE isReduceSize:FALSE isMyFont:18.0];

    if(_operatorType == kServiceTypeDataCard){
    [self getDataCardDataFromDB];
    }
    if(_operatorType == kServiceTypeDTH){
    [self getDTHDataFromDB];
    }
    if(_operatorType == kServiceTypeMobile){
    [self getMobileDataFromDB];
    }
    if(_operatorType == kServiceTypePostPaid){
    [self getPostPaidDataFromDB];
    }
    
    
    [operatorTV reloadData];
    
}

-(void)getMobileDataFromDB{
    
   DBManager * dbManager = [[DBManager alloc]initWithDatabaseFilename:@"operatorDB.sql"];

    NSString *query = [NSString stringWithFormat:@"select * from operator WHERE OPERATOR_KEY=\"%@\"",@"Mobile"];//@"select * from operator WHERE OPERATOR_KEY=\"%@\",@"Mobile"";
   // NSLog(@"Query is :%@",query);
   // NSLog(@"DB is Mobile:%@",[[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]]);

    
    NSArray *theArray = [[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]];
    
    NSInteger indexOfOperatorName = [dbManager.arrColumnNames indexOfObject:@"OPERATOR_NAME"];
    for(int i=0;i<theArray.count;i++){
    [operatorNameArray addObject:[[theArray objectAtIndex:i] objectAtIndex:indexOfOperatorName]];
    }
    
    
    
    
    NSArray *opArray = [operatorNameArray copy];
    
    NSArray *finalArray = [opArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    operatorNameArray = [finalArray mutableCopy];
    
    operatorImageArray  = [[NSMutableArray alloc]initWithObjects:@"m_aircel.png",@"m_airtel.png",@"m_bsnl.png",@"m_idea.png",@"m_mtnl.png",@"m_mts.png",@"m_reliance_cdma.png",@"m_reliance.png",@"m_docomo.png",@"m_indicom.png",@"m_uninor.png",@"m_videocon.png",@"m_vodafone.png", nil];
    
}
-(void)getDataCardDataFromDB{
    DBManager * dbManager = [[DBManager alloc]initWithDatabaseFilename:@"operatorDB.sql"];
    NSString *query = [NSString stringWithFormat:@"select * from operator WHERE OPERATOR_KEY=\"%@\"",@"DataCard"];
    
    
    NSArray *theArray = [[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]];

    NSInteger indexOfOperatorName = [dbManager.arrColumnNames indexOfObject:@"OPERATOR_NAME"];
    for(int i=0;i<theArray.count;i++){
    [operatorNameArray addObject:[[theArray objectAtIndex:i] objectAtIndex:indexOfOperatorName]];
    }
    
  
    NSArray *opArray = [operatorNameArray copy];
    
    NSArray *finalArray = [opArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    operatorNameArray = [finalArray mutableCopy];
    
    operatorImageArray  = [[NSMutableArray alloc]initWithObjects:@"m_aircel.png",@"m_airtel.png",@"m_bsnl.png",@"m_idea.png",@"m_mtnl.png",@"m_mts.png",@"m_reliance.png",@"m_docomo.png",@"m_vodafone.png", nil];

}
-(void)getPostPaidDataFromDB{
    DBManager * dbManager = [[DBManager alloc]initWithDatabaseFilename:@"operatorDB.sql"];
    NSString *query = [NSString stringWithFormat:@"select * from operator WHERE OPERATOR_KEY=\"%@\"",@"PostPaid"];
    
    NSArray *theArray = [[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]];
    
    NSInteger indexOfOperatorName = [dbManager.arrColumnNames indexOfObject:@"OPERATOR_NAME"];
    for(int i=0;i<theArray.count;i++){
    [operatorNameArray addObject:[[theArray objectAtIndex:i] objectAtIndex:indexOfOperatorName]];
    }
    
    
    NSArray *opArray = [operatorNameArray copy];
    
    NSArray *finalArray = [opArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    operatorNameArray = [finalArray mutableCopy];

    NSLog(@"#operatorNameArray :%@",operatorNameArray);

    
    operatorImageArray  = [[NSMutableArray alloc]initWithObjects:@"m_airtel.png",@"m_bsnl.png",@"m_docomo.png",@"m_idea.png",@"m_reliance_cdma.png",@"m_indicom.png",@"m_vodafone.png", nil];

}
-(void)getDTHDataFromDB{
    DBManager * dbManager = [[DBManager alloc]initWithDatabaseFilename:@"operatorDB.sql"];

    NSString *query = [NSString stringWithFormat:@"select * from operator WHERE OPERATOR_KEY=\"%@\"",@"DTH"];
    
    NSArray *theArray = [[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]];
    
    NSInteger indexOfOperatorName = [dbManager.arrColumnNames indexOfObject:@"OPERATOR_NAME"];
    for(int i=0;i<theArray.count;i++){
    [operatorNameArray addObject:[[theArray objectAtIndex:i] objectAtIndex:indexOfOperatorName]];
    }
    
    
    
    operatorImageArray  = [[NSMutableArray alloc]initWithObjects:@"m_airtel.png",@"d_bigtv.png",@"dishtv_logo.jpg",@"d_sundirect.png",@"d_tatasky.png",@"d_videocon.png", nil];

}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return operatorNameArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"OperatorTableCell";
    
    objOperatorCell = (OperatorTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(objOperatorCell==nil){
        [[NSBundle mainBundle] loadNibNamed:@"OperatorTableCell" owner:self options:nil];
    }
   
    
    objOperatorCell.operatorLogoImageView.image = [UIImage imageNamed:[operatorImageArray objectAtIndex:indexPath.row]];
    objOperatorCell.operatorLabel.text = [operatorNameArray objectAtIndex:indexPath.row];;


    return objOperatorCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [operatorTV deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *opName = [operatorNameArray objectAtIndex:indexPath.row];
    NSString *planOPID = [[self selectOperatorIDFromOperatorName:opName] objectAtIndex:0];
    NSString *opID = [[self selectOperatorIDFromOperatorName:opName] objectAtIndex:1];
    NSString *opSTV = [[self selectOperatorIDFromOperatorName:opName] objectAtIndex:2];
    NSString *opMinAmount = [[self selectOperatorIDFromOperatorName:opName] objectAtIndex:3];
    NSString *opMaxAmount = [[self selectOperatorIDFromOperatorName:opName] objectAtIndex:4];
    NSString *opCode = [[self selectOperatorIDFromOperatorName:opName] objectAtIndex:5];
    NSString *opLength  = [[self selectOperatorIDFromOperatorName:opName] objectAtIndex:6];
    NSString *opPrefix =  [[self selectOperatorIDFromOperatorName:opName] objectAtIndex:7];
    
    NSString *opServiceChargeSlab =  [[self selectOperatorIDFromOperatorName:opName] objectAtIndex:8];
    NSString *opServiceChargeAmount =  [[self selectOperatorIDFromOperatorName:opName] objectAtIndex:9];
    NSString *opServiceTaxPercent =  [[self selectOperatorIDFromOperatorName:opName] objectAtIndex:10];
    
    
    NSLog(@"#CheckNameAndID :%@ %@ %@ %@ %@ %@ %@ %@",opName,opID,opSTV,opMinAmount,opMaxAmount,opServiceChargeSlab,opServiceChargeAmount,opServiceTaxPercent);
    
    if (_onOperatorSelectClick) {
    _onOperatorSelectClick(opName,planOPID,opID,opSTV,opMinAmount,opMaxAmount,opCode,opLength,opPrefix,opServiceChargeSlab,opServiceChargeAmount,opServiceTaxPercent);
    }
    [self.navigationController popViewControllerAnimated:TRUE];
}

-(NSMutableArray *)selectOperatorIDFromOperatorName:(NSString *)operatorName{
    DBManager * dbManager = [[DBManager alloc]initWithDatabaseFilename:@"operatorDB.sql"];
    
    NSString *query ;
    
    if(_operatorType == kServiceTypeDataCard){
    query = [NSString stringWithFormat:@"select OPERATOR_PRODUCT_ID,OPERATOR_ID,OPERATOR_STV,OPERATOR_MIN,OPERATOR_MAX,OPERATOR_CODE,OPERATOR_LENGTH,OPERATOR_PREFIX,OPERATOR_CHARGES_SLAB,OPERATOR_SERVICE_CHARGE_AMOUNT,OPERATOR_SERVICE_TAX_PERCENT from operator WHERE OPERATOR_KEY=\"%@\" AND OPERATOR_NAME=\"%@\"",@"DataCard",operatorName];//@"select * from operator WHERE OPERATOR_KEY=\"%@\",@"Mobile"";
    }
    if(_operatorType == kServiceTypeDTH){
    query = [NSString stringWithFormat:@"select OPERATOR_PRODUCT_ID,OPERATOR_ID,OPERATOR_STV,OPERATOR_MIN,OPERATOR_MAX,OPERATOR_CODE,OPERATOR_LENGTH,OPERATOR_PREFIX,OPERATOR_CHARGES_SLAB,OPERATOR_SERVICE_CHARGE_AMOUNT,OPERATOR_SERVICE_TAX_PERCENT from operator WHERE OPERATOR_KEY=\"%@\" AND OPERATOR_NAME=\"%@\"",@"DTH",operatorName];//@"select * from operator WHERE OPERATOR_KEY=\"%@\",@"Mobile"";
    }
    if(_operatorType == kServiceTypeMobile){
    query = [NSString stringWithFormat:@"select OPERATOR_PRODUCT_ID,OPERATOR_ID,OPERATOR_STV,OPERATOR_MIN,OPERATOR_MAX,OPERATOR_CODE,OPERATOR_LENGTH,OPERATOR_PREFIX,OPERATOR_CHARGES_SLAB,OPERATOR_SERVICE_CHARGE_AMOUNT,OPERATOR_SERVICE_TAX_PERCENT from operator WHERE OPERATOR_KEY=\"%@\" AND OPERATOR_NAME=\"%@\"",@"Mobile",operatorName];//@"select * from operator WHERE OPERATOR_KEY=\"%@\",@"Mobile"";
    }
    if(_operatorType == kServiceTypePostPaid){
    query = [NSString stringWithFormat:@"select OPERATOR_PRODUCT_ID,OPERATOR_ID,OPERATOR_STV,OPERATOR_MIN,OPERATOR_MAX,OPERATOR_CODE,OPERATOR_LENGTH,OPERATOR_PREFIX,OPERATOR_CHARGES_SLAB,OPERATOR_SERVICE_CHARGE_AMOUNT,OPERATOR_SERVICE_TAX_PERCENT from operator WHERE OPERATOR_KEY=\"%@\" AND OPERATOR_NAME=\"%@\"",@"PostPaid",operatorName];//@"select * from operator WHERE OPERATOR_KEY=\"%@\",@"Mobile"";
    }
    
    NSLog(@"Query is :%@",query);
    NSLog(@"DB is Mobile:%@",[[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]]);
    
    NSMutableArray *selectedOpArray = [[dbManager loadDataFromDB:query]objectAtIndex:0];
    
  //  NSLog(@"selectedOpArray :%@",selectedOpArray);
    
    return selectedOpArray;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(void)backClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
