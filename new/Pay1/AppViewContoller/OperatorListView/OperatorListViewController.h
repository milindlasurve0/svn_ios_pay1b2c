//
//  OperatorListViewController.h
//  Pay1
//
//  Created by Annapurna on 15/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"
#import "EnumType.h"
#import "DBManager.h"
@class OperatorTableCell;
typedef void (^OnOperatorSelectClick)(NSString *operatorName,NSString *planOperatorID,NSString *operatorID,NSString *operatorSTV,NSString *operatorMinAmount,NSString *operatorMaxAmount,NSString *operatorCode,NSString *operatorLength,NSString *operatorPrefix,NSString *operatorChargeSlab,NSString *operatorServiceChargeAmount,NSString *operatorServiceTaxPercent);

@interface OperatorListViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>{
    IBOutlet UITableView *operatorTV;
    NSMutableArray *operatorNameArray;
    NSMutableArray *operatorImageArray;
    
    IBOutlet OperatorTableCell *objOperatorCell;
   
}

@property (nonatomic, assign) ServiceType operatorType;

@property (nonatomic, strong) DBManager *dbManager;

@property (nonatomic, copy) OnOperatorSelectClick  onOperatorSelectClick;


@end
