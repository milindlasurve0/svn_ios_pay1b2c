//
//  TransactionHistoryViewController.h
//  Pay1
//
//  Created by webninjaz on 19/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"

@class TransactionHistoryView;
@class TransactionCustomCell;

@interface TransactionHistoryViewController : BaseViewController<UITextFieldDelegate,UITextFieldDelegate,UITableViewDataSource>{
    
    IBOutlet UITableView *transactionTableView;
    IBOutlet TransactionCustomCell *transactionCustomCell;
    
    IBOutlet UIImageView *transactionImageView;
    IBOutlet UILabel *transactionLabel;
    
    int transValue;
    
    UIView *bottomView;
    UIButton *bottomButton;
    
}

@property (nonatomic,strong) NSMutableArray *transactionArray;


@end
