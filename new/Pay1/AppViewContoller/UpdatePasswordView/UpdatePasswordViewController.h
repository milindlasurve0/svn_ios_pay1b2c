//
//  UpdatePasswordViewController.h
//  Pay1
//
//  Created by Annapurna on 12/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"
@class TPKeyboardAvoidingScrollView;
//@class NoNetworkView;
@interface UpdatePasswordViewController : BaseViewController<UITextFieldDelegate>{
    
    IBOutlet UITextField *otpTxtField;
    IBOutlet UITextField *newPasswordTxtField;
    IBOutlet UITextField *retypePasswordField;
    
    IBOutlet UIView *otpTxtSepView;
    IBOutlet UIView *newPasswordTxtSepView;
    IBOutlet UIView *retypePasswordSepView;
    
    IBOutlet UIButton *confirmButton;
    IBOutlet UIButton *resendButton;
    
    IBOutlet UILabel *verificationLabel;
    
    // NoNetworkView *objNoNetworkView;
    // IBOutlet UIImageView *bgImgView;
    
    IBOutlet UITextField *otpErrorTxtView;
    
    IBOutlet UIImageView *otpErrorImgView;
    
    

    
}

@property (nonatomic, strong) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (nonatomic, assign) BOOL isUserLoggedIn;
@property (nonatomic, strong) NSString *pswdOTP;


@end
