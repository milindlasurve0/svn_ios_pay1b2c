//
//  UpdatePasswordViewController.m
//  Pay1
//
//  Created by Annapurna on 12/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "UpdatePasswordViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "UpdatePasswordService.h"
#import "UpdatePasswordCriteria.h"
#import "UpdatePasswordResponse.h"
#import "NoNetworkView.h"
#import "CustomAlertView.h"
#import "OptionViewController.h"
@interface UpdatePasswordViewController ()

@end

@implementation UpdatePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self hideNavBackButton];
    
    [self setNaviationButtonWithText:@"Reset Password" selector:@selector(backClicked:) isLeft:TRUE isReduceSize:TRUE isMyFont:18.0];

    
    
   /* UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0.0f, 0.0f, 20.0f, 20.0f)];
    [btn addTarget:self action:@selector(backClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btn setImage:[UIImage imageNamed:@"move_back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *eng_btn = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = eng_btn;*/
    
    
    verificationLabel.text = [NSString stringWithFormat:@"%@%@",@"You will receive an OTP via SMS. Kindly enter below.",[Constant getUserPhoneNumber]];
    _scrollView.contentSize = CGSizeMake(self.view.frame.size.width, 350);
    
    [self removeAllErrorViewFromSuperView];
    
    [self setPlaceHolderColor:otpTxtField];
    [self setPlaceHolderColor:newPasswordTxtField];
    [self setPlaceHolderColor:retypePasswordField];
    
    
    
    [self setViewOfButton:resendButton];
    [self setConfirmVIewOfButton:confirmButton];
    
    [self setFrameForTxtField];
}

-(void)setFrameForTxtField{
    otpTxtField.frame = CGRectMake(17, 75, self.view.frame.size.width - 30, 35);
    otpTxtSepView.frame = CGRectMake(20, otpTxtField.frame.origin.y + otpTxtField.frame.size.height + 5, self.view.frame.size.width - 40, 1);

    
    newPasswordTxtField.frame = CGRectMake(17, otpTxtField.frame.origin.y + otpTxtField.frame.size.height + 25, self.view.frame.size.width - 30, 35);
    newPasswordTxtSepView.frame = CGRectMake(20, newPasswordTxtField.frame.origin.y + newPasswordTxtField.frame.size.height + 5, self.view.frame.size.width - 40, 1);

    retypePasswordField.frame = CGRectMake(17, newPasswordTxtField.frame.origin.y + newPasswordTxtField.frame.size.height + 25, self.view.frame.size.width - 30, 35);
    retypePasswordSepView.frame = CGRectMake(20, retypePasswordField.frame.origin.y + retypePasswordField.frame.size.height + 5, self.view.frame.size.width - 40, 1);
    
    
    resendButton.frame = CGRectMake(confirmButton.frame.origin.x + confirmButton.frame.size.width + 10, retypePasswordSepView.frame.origin.y + retypePasswordSepView.frame.size.height + 33, 140, 35);
    
}



-(IBAction)confirrmClick:(id)sender{
    NSLog(@"#NoNetworkFound");
    
    [otpTxtField resignFirstResponder];
    [newPasswordTxtField resignFirstResponder];
    [retypePasswordField resignFirstResponder];
    
    
    NSString *convertMD5 = [NSString stringWithFormat:@"%@",[Constant getUserPhoneNumber]];
    NSString *phNumMD5 = [Constant generateMD5:convertMD5];
    NSString *passwordMD5 = [Constant generateMD5:otpTxtField.text];
    
    NSString *finalMD5 = [NSString stringWithFormat:@"%@%@",phNumMD5,passwordMD5];
    
    
    if(otpTxtField.text.length == 0 || ![finalMD5 isEqualToString:_pswdOTP]){
    NSLog(@"Please enter OTP");
        
    otpTxtSepView.backgroundColor = [UIColor redColor];
    otpErrorImgView.frame = CGRectMake(self.view.frame.size.width - 40, otpTxtSepView.frame.origin.y - 30, 25, 25);
    otpErrorTxtView.frame = CGRectMake(130, otpTxtSepView.frame.origin.y + otpTxtSepView.frame.size.height, self.view.frame.size.width - 150, 35);
    otpErrorTxtView.placeholder = @"Please enter correct OTP";

    [self.scrollView addSubview:otpErrorTxtView];
    [self.scrollView addSubview:otpErrorImgView];
        
    return;
    }
    
    
 
    
   else if(newPasswordTxtField.text.length == 0){
    NSLog(@"Please Enter Passwords");
        
    newPasswordTxtSepView.backgroundColor = [UIColor redColor];
    otpErrorImgView.frame = CGRectMake(self.view.frame.size.width - 45, newPasswordTxtSepView.frame.origin.y - 30, 25, 25);
    otpErrorTxtView.frame = CGRectMake(150, newPasswordTxtSepView.frame.origin.y + newPasswordTxtSepView.frame.size.height, self.view.frame.size.width - 170, 35);
    otpErrorTxtView.placeholder = @"Please Enter Passwords";

    [self.scrollView addSubview:otpErrorTxtView];
    [self.scrollView addSubview:otpErrorImgView];
        
    
    return;
    }
    
    
    
    
   else if(newPasswordTxtField.text.length < 4){
    NSLog(@"Please enter password of four charecter");
        
    newPasswordTxtSepView.backgroundColor = [UIColor redColor];
    otpErrorImgView.frame = CGRectMake(self.view.frame.size.width - 40, newPasswordTxtSepView.frame.origin.y - 30, 25, 25);
    otpErrorTxtView.frame = CGRectMake(50, newPasswordTxtSepView.frame.origin.y + newPasswordTxtSepView.frame.size.height, self.view.frame.size.width - 70, 35);
    otpErrorTxtView.placeholder = @"Please enter password of four charecter";

    [self.scrollView addSubview:otpErrorTxtView];
    [self.scrollView addSubview:otpErrorImgView];

    return;
    }
    
   else if(![retypePasswordField.text isEqualToString:newPasswordTxtField.text]){
    NSLog(@"New Password and Retype Password must be same");
        
        
    retypePasswordSepView.backgroundColor = [UIColor redColor];
    otpErrorImgView.frame = CGRectMake(self.view.frame.size.width - 40, retypePasswordSepView.frame.origin.y - 35, 25, 25);
    otpErrorTxtView.frame = CGRectMake(30, retypePasswordSepView.frame.origin.y + retypePasswordSepView.frame.size.height, self.view.frame.size.width - 50, 35);
    otpErrorTxtView.placeholder = @"New Password and Retype Password must be same";
    [self.scrollView addSubview:otpErrorTxtView];
    [self.scrollView addSubview:otpErrorImgView];

    return;
    }
    
    else{
    [self showHUDOnView];
        
        if(![Constant checkNetworkConnection]){
            
            [self showNoNetworkView];
            
            _scrollView.scrollEnabled = FALSE;
            confirmButton.userInteractionEnabled = FALSE;
            resendButton.userInteractionEnabled = FALSE;
            
            [objNoNetworkView setOnRetryClick:^{
                NSLog(@"WeHaveToNavigateToCartPage");
                [objNoNetworkView removeFromSuperview];
                _scrollView.scrollEnabled = TRUE;
                confirmButton.userInteractionEnabled = TRUE;
                resendButton.userInteractionEnabled = TRUE;
            }];
            return;
        }
    else
        
    [self callUpdatePasswordService];
    }
    
}

-(void)callUpdatePasswordService{
    UpdatePasswordService *objUpdateService = [[UpdatePasswordService alloc]init];
    [objUpdateService callUpdatePasswordService:self.criteria callback:^(NSError *error, UpdatePasswordResponse *objUpdatePswdResponse) {
    [self hideHUDOnView];
        
    if(error)return ;
    else{
    //[self showCustomAlertView];
        
        [Constant saveUserPassword:newPasswordTxtField.text];
        
        
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"RESET PASSWORD" message:objUpdatePswdResponse.desc delegate:self cancelButtonTitle:@"DONE" otherButtonTitles:nil, nil];
        
        [alert show];
            
            
            
   /* [objCustomAlertView setTextOnCustomView:@"Reset Password" messageText:objUpdatePswdResponse.description];e
            
    [objCustomAlertView setOnButtonClick:^{
    NSLog(@"WeHaveToNavigateToCartPage");
    [dimView removeFromSuperview];
    [objCustomAlertView removeFromSuperview];
    _scrollView.scrollEnabled = TRUE;
    confirmButton.userInteractionEnabled = TRUE;
    resendButton.userInteractionEnabled = TRUE;
                
    [self performSegueWithIdentifier:@"revealVC" sender:self];
    }];*/
    }
        
    }];
}

-(UpdatePasswordCriteria *)criteria{
    UpdatePasswordCriteria *objCriteria = [[UpdatePasswordCriteria alloc]init];
    objCriteria.otpNum = [otpTxtField text];
    objCriteria.userPassword = newPasswordTxtField.text;
    objCriteria.confirmPassword = retypePasswordField.text;
    objCriteria.userMobNum = [Constant getUserPhoneNumber];
    objCriteria.currentPassword = [otpTxtField text];
    
    return objCriteria;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(otpErrorTxtView.hidden == FALSE && textField.text.length > 0){
    [otpErrorTxtView removeFromSuperview];
    [otpErrorImgView removeFromSuperview];
    }
    
    if(textField == otpTxtField){
    otpTxtSepView.backgroundColor = [UIColor lightGrayColor];
    }
    
    if(textField == newPasswordTxtField){
    newPasswordTxtSepView.backgroundColor = [UIColor lightGrayColor];
    }
    
    if(textField == retypePasswordField){
    retypePasswordSepView.backgroundColor = [UIColor lightGrayColor];
    }

    
    return TRUE;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return TRUE;
}

-(void)backClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIAlert

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    AppDelegate *appDelegate =  (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if(buttonIndex == 0){
    if(!_isUserLoggedIn){
    [appDelegate doLogout];
    }
    else{
        NSLog(@"#NAvControllerArray :%lu",(unsigned long)appDelegate.tabBarController.viewControllers.count);
        
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray:self.navigationController.viewControllers];
        
        // [navigationArray removeAllObjects];    // This is just for remove all view controller from navigation stack.
        [navigationArray removeObjectAtIndex: 3];  // You can pass your index here
        [navigationArray removeObjectAtIndex: 2];  // You can pass your index here
        [navigationArray removeObjectAtIndex: 1];  // You can pass your index here


        self.navigationController.viewControllers = navigationArray;
        
        

    [appDelegate.tabBarController setSelectedIndex:0];
        
       
    }
    }
}

#pragma mark - Resend Password Method

-(IBAction)resendButtonClicked:(id)sender{
    [self showHUDOnView];
    [self callForgotPasswordServiceBlock];
}

-(void)callForgotPasswordServiceBlock{
    UpdatePasswordService *objService = [[UpdatePasswordService alloc]init];
    [objService callForgotPasswordService:[Constant getUserPhoneNumber] vtype:@"1" callback:^(NSError *error, UpdatePasswordResponse *objForgotPswdRespons) {
    if(error)return ;
    else{
    [self hideHUDOnView];
        
    _pswdOTP = objForgotPswdRespons.messageOTP;
    }
    }];
    
}


-(void)removeAllErrorViewFromSuperView{
    
   if([UIScreen mainScreen].bounds.size.height == 480){
    otpErrorTxtView.frame = CGRectMake(149, 128, self.view.frame.size.width - 170, 35);

    }
    
    if([UIScreen mainScreen].bounds.size.height == 568){
    otpErrorTxtView.frame = CGRectMake(149, 128, self.view.frame.size.width - 170, 35);
  
    }
    
    
    [self setUIForErrorViews:otpErrorTxtView];
    
    
    
    [otpErrorTxtView removeFromSuperview];
    
    
    [otpErrorImgView removeFromSuperview];
   
    
}

-(void)showValidationAlertView:(UIView *)errorAlertView{
    errorAlertView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
    errorAlertView.transform = CGAffineTransformMakeScale(1.2, 1.2);
    [errorAlertView setAlpha:1.0];
        
    [self.scrollView addSubview:errorAlertView];
    } completion:^(BOOL finished){
        
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
