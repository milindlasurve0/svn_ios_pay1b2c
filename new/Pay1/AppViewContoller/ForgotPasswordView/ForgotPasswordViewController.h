//
//  ForgotPasswordViewController.h
//  Pay1
//
//  Created by Annapurna on 04/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"
@class TPKeyboardAvoidingScrollView;

@interface ForgotPasswordViewController : BaseViewController<UITextFieldDelegate>{
    
    IBOutlet UITextField *otpTxtField;
    IBOutlet UITextField *newPasswordTxtField;
    IBOutlet UITextField *retypePasswordField;
    
    IBOutlet UIButton *confirmButton;
    IBOutlet UIButton *resendButton;
    
}

@property (nonatomic, strong) IBOutlet TPKeyboardAvoidingScrollView *scrollView;


@end
