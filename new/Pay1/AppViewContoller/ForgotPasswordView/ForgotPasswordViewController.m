//
//  ForgotPasswordViewController.m
//  Pay1
//
//  Created by Annapurna on 04/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface ForgotPasswordViewController ()

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _scrollView.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);

   // [self hideNavBackButton];
    [self setPlaceHolderColor:otpTxtField];
    [self setPlaceHolderColor:newPasswordTxtField];
    [self setPlaceHolderColor:retypePasswordField];
    
    
   
    [self setViewOfButton:resendButton];
    [self setConfirmVIewOfButton:confirmButton];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return TRUE;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end



