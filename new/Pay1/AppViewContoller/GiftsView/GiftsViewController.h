//
//  GiftsViewController.h
//  Pay1
//
//  Created by Annapurna on 16/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"
#import "EnumType.h"
@class GiftCustomCell;
@class GiftCategoryCustomCell;
@class MyLikesCell;
@class MyGiftsCell;
@interface GiftsViewController : BaseViewController<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>{
    NSMutableDictionary *distanceDic;
    NSMutableArray *offerIDSArray;
    IBOutlet UIScrollView *headerScrollView;
    IBOutlet UIScrollView *pageScrollView;
    IBOutlet UIView *headerView;
    
    IBOutlet UIButton *giftButton;
    IBOutlet UIButton *nearYouButton;
    IBOutlet UIButton *categoryButton;
    IBOutlet UIButton *myGiftButton;
    IBOutlet UIButton *myLikesButton;
    
    IBOutlet GiftCustomCell *objGiftCell;
    IBOutlet GiftCategoryCustomCell *objGiftCatCustomCell;
    IBOutlet MyLikesCell *objMyLikesCell;
    IBOutlet MyGiftsCell *objMyGiftCell;
    
    IBOutlet UITableView *giftTV;
    IBOutlet UITableView *categoryTV;
    
    NSMutableArray *firstIndexGiftArray;
    
    
    NSMutableArray *giftCatArray;

    
    NSMutableArray *giftDataArray;
    NSMutableArray *giftCategoryArray;
    NSMutableArray *myGiftDataArray;
    NSMutableArray *myLikesDataArray;
    NSMutableArray *nearYouGiftArray;
    
    BOOL isLoadedOnce;
    
    GiftSelectedType giftSelectType;
    
    IBOutlet UIScrollView *giftScrollView;
    
    NSTimer *imageTimer;
    
    
    IBOutlet UIView *underLineView;
    
    UIView *coverView;
    
   
    
    
    NSMutableArray *imageURLArray;
    NSMutableArray *shopNameArray;
    NSMutableArray *areaArray;
    NSMutableArray *offerArray;
    NSMutableArray *priceArray;
    
    NSMutableArray *shopLatArray;
    NSMutableArray *shopLongArray;
    
    NSMutableArray *dealIDArrayList;
    NSMutableArray *offerIDArrayList;

    
    IBOutlet UITableView *myLikesTV;
    IBOutlet UITableView *myGiftsTV;
    IBOutlet UITableView *nearYouTV;
    
    UISearchBar *theSearchBar;
    NSString *searchBarText;
    
    NSMutableArray *distanceArray;
    
    IBOutlet UIView *bottomView;
    
    IBOutlet UIImageView *noGiftNearYouImgView;
    IBOutlet UILabel *noGiftNearYouLabel;
    
    IBOutlet UIImageView *noMyGiftsImgView;
    IBOutlet UILabel *noMyGiftsLabel;
    
    IBOutlet UIImageView *noMyLikesImgView;
    IBOutlet UILabel *noMyLikesLabel;
    
    NSMutableArray *likesArray;
    
    
    BOOL checkFromWhere;
    
    BOOL isGiftClickedType;
    
    CGPoint pointNow;

}

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;


@end
