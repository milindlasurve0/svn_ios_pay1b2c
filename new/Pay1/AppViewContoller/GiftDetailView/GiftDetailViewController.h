//
//  GiftDetailViewController.h
//  Pay1
//
//  Created by Annapurna on 19/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"
#import "EnumType.h"
@class GiftDetailResponse;
@class DetailImageView;
@class GiftDetailCommonView;
@class RateView;
@class GetMyDealList;


@interface GiftDetailViewController : BaseViewController<UIScrollViewDelegate,GMSMapViewDelegate,UIGestureRecognizerDelegate>
{
    GMSMapView *mapView;
    
    
    IBOutlet UIImageView *giftImageView;
    IBOutlet UIImageView *thumbImageView;
    IBOutlet UILabel *giftTitleLabel;
    IBOutlet UILabel *giftAddressLabel;
    IBOutlet UILabel *giftPriceLabel;
    
    
    IBOutlet UIView *detailInfoView;
    
    
    IBOutlet UIView *detailInfoSep1View;
    IBOutlet UIView *detailInfoSep2View;
    
    IBOutlet UIView *mapSep1View;
    IBOutlet UIView *mapSep2View;
    
    IBOutlet UIView *last1SepView;
    IBOutlet UIView *last2SepView;
    
    IBOutlet UIScrollView *scrollView;
    
    
    NSMutableArray *offerDetailArray;
    NSMutableArray *locationDetailArrayList;
    
    UILabel *offerDetailLabel;
    UIImageView *dotImgView;
    
    float labelY;
    UILabel *addressLabel;
    
    IBOutlet UIButton *callButton;
    IBOutlet UIButton *getITButton;
    
    BOOL isMoreClicked;
    
    
    IBOutlet UIWebView *offerDescWebView;
    IBOutlet UIWebView *offerWebView;
    
    IBOutlet UIView *mapContainerView;
    
    GiftDetailResponse *giftDetailResponseObj;
    
    UILabel *photoTextLabel;
    UIView *photoSepView;
    UIView *photoContainerView;
    UIView *photoLastSepView1;
    UIView *photoLastSepView2;
    UIView *photoLastSepView;
    
    UIImageView *photoImageView;
    
    
    IBOutlet UIButton *moreButton;
    
    IBOutlet UIView *firstSepView;
    IBOutlet UIView *secondSepView;
    IBOutlet UIView *thirdSepView;
    UILabel *shopNameLabel;
    UILabel *shopAddressLabel;
    
    NSString *shopNameStr;
    
    
    DetailImageView *objDetailImageView;

    float webViewHeight;
    
    NSString *shopLat;
    NSString *shopLong;
    
    IBOutlet UIView *giftDescContainerView;
    IBOutlet UIView *myGiftDetailContainerView;
    
    IBOutlet UIView *giftFirst1SepView;
    IBOutlet UIView *giftSepView;
    IBOutlet UIView *giftFirst2SepView;

    IBOutlet UILabel *mobileNumLabel;
    IBOutlet UILabel *voucherCodeLabel;
    IBOutlet UILabel *expiryDateLabel;
    
    IBOutlet UILabel *offerTxtLabel;
    IBOutlet UIView *offerRedView;
    
    IBOutlet UIButton *mapButton;
    IBOutlet UIButton *giftButton;
    
    NSMutableArray *likeArray;
    
    IBOutlet UIButton *heartButton;
    
    IBOutlet UIButton *dropButton;
    
    IBOutlet UILabel *myGiftTextLabel1;
    IBOutlet UILabel *myGiftTextLabel2;
    
    
    IBOutlet UIView *moreView;
    GiftDetailCommonView *commonView ;
    UIView *coverView;
    
    
    BOOL isAlert;
    
    IBOutlet UILabel *firstGiftLabel;
    IBOutlet UILabel *secondGiftLabel;
    
    IBOutlet UIButton *rateButton;
    
 //   NSMutableArray *saveDealArray;
    
    BOOL isGiftFound;
    
    IBOutlet UILabel *mobileTxtLabel,*voucherTxtLabel,*expiryTxtLabel;
    
    RateView *objRateView;
    
    BOOL isHeartClick;
    NSString *voucherCodeForRedeem;
    
    BOOL isForRedeem;
    
    IBOutlet UIView *giftBottomView;
    IBOutlet UIView *giftScrollView;
    
    
    UIView *urlSepView1;
    UIView * urlSepView;
    UIView *urlSepView2;
    
    UIView *urlDataView;
    
    NSString *voucherCodeStr;
    NSString *expiryDateStr;
    
    UIView *brandDescSepView1;
    UIView *brandDescSepView;
    UIView *brandDescSepView2;
    
    UIView *brandDescDataView;
    
    IBOutlet UILabel *offerPriceLabel;

    BOOL isOfferPriceFound;
    
    BOOL isMoveToRechargeView;
    
    IBOutlet UIImageView *rupeeImageView;
    
    NSString *actualPriceStr;
    
    UIButton *checkButton;
    
    BOOL isFromWallet;
    
    NSString *fromVouchers;
    
    IBOutlet UIImageView *plusImageView;
    
    
    NSString *giftPocketStr;
    
    BOOL isPurchasedGift;
    BOOL isLocationNotFound;
    
    IBOutlet UILabel *pinTxtLabel;
    IBOutlet UILabel *pinDataLabel;
    
    
    NSString *pinCodeStr;
    NSString *giftStatusStr;
}

@property (nonatomic, strong) NSString *giftOfferID;
@property (nonatomic, strong) NSString *giftID;
@property (nonatomic, strong) NSString *shopName;

@property (nonatomic, assign) GiftSelectedType giftType;
@property (nonatomic, strong) GetMyDealList *objMyGiftDealList;



@end
