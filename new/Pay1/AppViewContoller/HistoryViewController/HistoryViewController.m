//
//  HistoryViewController.m
//  Pay1
//
//  Created by webninjaz on 19/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "HistoryViewController.h"
#import "TransactionHistoryViewController.h"
#import "WalletHistoryViewController.h"
#import "CoinViewController.h"

@interface HistoryViewController ()

@end

@implementation HistoryViewController

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)


- (void)viewDidLoad {
    [super viewDidLoad];
  //  [self addOfferButtonOnAllView];
   /* revealViewController = [self revealViewController];
    
    [revealViewController panGestureRecognizer];
    [revealViewController tapGestureRecognizer];
    self.revealViewController.delegate = self;
    
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"navigation_sliding_bar.png"] style:UIBarButtonItemStyleDone target:self.revealViewController action:@selector(revealToggle:)];
    leftBarButton.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = leftBarButton;
    self.navigationItem.rightBarButtonItem = nil;
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];*/
    
    [self hideNavBackButton];
    
    [self setNaviationButtonWithText:@"BACK" selector:@selector(backClicked:) isLeft:TRUE isReduceSize:TRUE isMyFont:18.0];

    
    NSLog(@"FrameWidth :%f",self.view.frame.size.width);
    
    
    if ([UIScreen mainScreen].bounds.size.height == 480) {
    
        firstThinView.frame = CGRectMake(0, 120, 320, 1);
        
        
        
        historyLabel.frame = CGRectMake(93, 75, 149, 32);
        
        
        transactionHistoryImageView.frame = CGRectMake(126, firstThinView.frame.origin.y + firstThinView.frame.size.height + 30, 75, 75);
        firstRightArrowImageView.frame = CGRectMake(299, firstThinView.frame.origin.y + firstThinView.frame.size.height + 40, 11, 21);
        transactionLabel.frame = CGRectMake(74, transactionHistoryImageView.frame.origin.y + transactionHistoryImageView.frame.size.height +5, 193, 21);
        
        
        secondThinView.frame = CGRectMake(0, transactionLabel.frame.origin.y + transactionLabel.frame.size.height + 5, 320, 1);
        walletHistoryImageView.frame = CGRectMake(126, secondThinView.frame.origin.y + secondThinView.frame.size.height + 30, 75, 75);
        secondRightArrowImageView.frame = CGRectMake(299, secondThinView.frame.origin.y + secondThinView.frame.size.height + 40, 11, 21);
        walletLabel.frame = CGRectMake(55, walletHistoryImageView.frame.origin.y + walletHistoryImageView.frame.size.height + 10, 223, 21);
        
        
        thirdThinView.frame = CGRectMake(0, walletLabel.frame.origin.y + walletLabel.frame.size.height + 5, 320, 1);
        coinHistoryImageView.frame = CGRectMake(126, thirdThinView.frame.origin.y + thirdThinView.frame.size.height + 30, 75, 75);
        coinHistoryLabel.frame = CGRectMake(70, coinHistoryImageView.frame.origin.y + coinHistoryImageView.frame.size.height + 10, 193, 21);
        thirdRightArrowImageView.frame = CGRectMake(299, secondThinView.frame.origin.y + secondThinView.frame.size.height + 40, 11, 21);
        
        fouthThinView.frame = CGRectMake(0, coinHistoryLabel.frame.origin.y + coinHistoryLabel.frame.size.height + 5, 320, 1);
        
        
        transactionHistoryButton.frame = CGRectMake(0, firstThinView.frame.origin.y + firstThinView.frame.size.height, 320, 120);
        
        
        transactionHistoryButton.frame = CGRectMake(0, firstThinView.frame.origin.y + firstThinView.frame.size.height, 320, secondThinView.frame.origin.y - (firstThinView.frame.origin.y + firstThinView.frame.size.height));
        
        walletHistoryButton.frame = CGRectMake(0, secondThinView.frame.origin.y + secondThinView.frame.size.height, 320, thirdThinView.frame.origin.y - (secondThinView.frame.origin.y + secondThinView.frame.size.height));
        
        
        coinHistoryButton.frame = CGRectMake(0, thirdThinView.frame.origin.y + thirdThinView.frame.size.height, 320, fouthThinView.frame.origin.y - (thirdThinView.frame.origin.y + thirdThinView.frame.size.height));    }
    
    else if ([UIScreen mainScreen].bounds.size.height  == 568){
        
        firstThinView.frame = CGRectMake(0, 120, 320, 1);
        
        
        
        historyLabel.frame = CGRectMake(90, 80, 150, 32);
        
        
        transactionHistoryImageView.frame = CGRectMake(126, firstThinView.frame.origin.y + firstThinView.frame.size.height + 30, 75, 75);
        firstRightArrowImageView.frame = CGRectMake(299, firstThinView.frame.origin.y + firstThinView.frame.size.height + 50, 11, 21);
        transactionLabel.frame = CGRectMake(74, transactionHistoryImageView.frame.origin.y + transactionHistoryImageView.frame.size.height +5, 193, 21);

        
        secondThinView.frame = CGRectMake(0, transactionLabel.frame.origin.y + transactionLabel.frame.size.height + 5, 320, 1);
        walletHistoryImageView.frame = CGRectMake(126, secondThinView.frame.origin.y + secondThinView.frame.size.height + 30, 75, 75);
        secondRightArrowImageView.frame = CGRectMake(299, secondThinView.frame.origin.y + secondThinView.frame.size.height + 50, 11, 21);
        walletLabel.frame = CGRectMake(55, walletHistoryImageView.frame.origin.y + walletHistoryImageView.frame.size.height + 10, 223, 21);

        
        thirdThinView.frame = CGRectMake(0, walletLabel.frame.origin.y + walletLabel.frame.size.height + 5, 320, 1);
        coinHistoryImageView.frame = CGRectMake(126, thirdThinView.frame.origin.y + thirdThinView.frame.size.height + 30, 75, 75);
        coinHistoryLabel.frame = CGRectMake(70, coinHistoryImageView.frame.origin.y + coinHistoryImageView.frame.size.height + 10, 193, 21);
        thirdRightArrowImageView.frame = CGRectMake(299, thirdThinView.frame.origin.y + thirdThinView.frame.size.height + 50, 11, 21);

        fouthThinView.frame = CGRectMake(0, coinHistoryLabel.frame.origin.y + coinHistoryLabel.frame.size.height + 5, 320, 1);

        
        transactionHistoryButton.frame = CGRectMake(0, firstThinView.frame.origin.y + firstThinView.frame.size.height, 320, 120);
        
        
        transactionHistoryButton.frame = CGRectMake(0, firstThinView.frame.origin.y + firstThinView.frame.size.height, 320, secondThinView.frame.origin.y - (firstThinView.frame.origin.y + firstThinView.frame.size.height));
        
        walletHistoryButton.frame = CGRectMake(0, secondThinView.frame.origin.y + secondThinView.frame.size.height, 320, thirdThinView.frame.origin.y - (secondThinView.frame.origin.y + secondThinView.frame.size.height));


        coinHistoryButton.frame = CGRectMake(0, thirdThinView.frame.origin.y + thirdThinView.frame.size.height, 320, fouthThinView.frame.origin.y - (thirdThinView.frame.origin.y + thirdThinView.frame.size.height));


    
    } else if ([UIScreen mainScreen].bounds.size.height  == 667){
        
        firstThinView.frame = CGRectMake(0, 120, self.view.frame.size.width, 1);
        
        
        
        historyLabel.frame = CGRectMake(100, 75, 149, 32);
        
        
        transactionHistoryImageView.frame = CGRectMake(150, firstThinView.frame.origin.y + firstThinView.frame.size.height + 30, 75, 75);
        firstRightArrowImageView.frame = CGRectMake(self.view.frame.size.width - 21, firstThinView.frame.origin.y + firstThinView.frame.size.height + 55, 11, 21);
        transactionLabel.frame = CGRectMake(150 - 52, transactionHistoryImageView.frame.origin.y + transactionHistoryImageView.frame.size.height +5, 193, 21);
        
        
        secondThinView.frame = CGRectMake(0, transactionLabel.frame.origin.y + transactionLabel.frame.size.height + 5, self.view.frame.size.width, 1);
        walletHistoryImageView.frame = CGRectMake(150, secondThinView.frame.origin.y + secondThinView.frame.size.height + 30, 75, 75);
        secondRightArrowImageView.frame = CGRectMake(self.view.frame.size.width - 21, secondThinView.frame.origin.y + secondThinView.frame.size.height + 55, 11, 21);
        walletLabel.frame = CGRectMake(150- 70, walletHistoryImageView.frame.origin.y + walletHistoryImageView.frame.size.height + 10, 223, 21);
        
        
        thirdThinView.frame = CGRectMake(0, walletLabel.frame.origin.y + walletLabel.frame.size.height + 5, self.view.frame.size.width, 1);
        coinHistoryImageView.frame = CGRectMake(150, thirdThinView.frame.origin.y + thirdThinView.frame.size.height + 30, 75, 75);
        coinHistoryLabel.frame = CGRectMake(150-55, coinHistoryImageView.frame.origin.y + coinHistoryImageView.frame.size.height + 10, 193, 21);
        thirdRightArrowImageView.frame = CGRectMake(self.view.frame.size.width - 21, thirdThinView.frame.origin.y + thirdThinView.frame.size.height + 55, 11, 21);
        
        fouthThinView.frame = CGRectMake(0, coinHistoryLabel.frame.origin.y + coinHistoryLabel.frame.size.height + 5, self.view.frame.size.width, 1);
        
        
        transactionHistoryButton.frame = CGRectMake(0, firstThinView.frame.origin.y + firstThinView.frame.size.height, self.view.frame.size.width, 120);
        
        
        transactionHistoryButton.frame = CGRectMake(0, firstThinView.frame.origin.y + firstThinView.frame.size.height, self.view.frame.size.width, secondThinView.frame.origin.y - (firstThinView.frame.origin.y + firstThinView.frame.size.height));
        
        walletHistoryButton.frame = CGRectMake(0, secondThinView.frame.origin.y + secondThinView.frame.size.height, self.view.frame.size.width, thirdThinView.frame.origin.y - (secondThinView.frame.origin.y + secondThinView.frame.size.height));
        
        
        coinHistoryButton.frame = CGRectMake(0, thirdThinView.frame.origin.y + thirdThinView.frame.size.height, self.view.frame.size.width, fouthThinView.frame.origin.y - (thirdThinView.frame.origin.y + thirdThinView.frame.size.height));
        
    }
    else{
        
    }

}


-(IBAction)transactionHistoryClicked:(id)sender{
    TransactionHistoryViewController *transactionHistoryVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TransactionHistoryView"];
    [self.navigationController pushViewController:transactionHistoryVC animated:YES];
}

-(IBAction)walletHistoryClicked:(id)sender{
    WalletHistoryViewController *walletHistoryVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WalletHistoryView"];
    [self.navigationController pushViewController:walletHistoryVC animated:YES];
}

-(IBAction)coinHistoryClicked:(id)sender{
    CoinViewController *coinHistoryVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CoinView"];
    [self.navigationController pushViewController:coinHistoryVC animated:YES];
}

-(void)backClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Reveal
/*
- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if (position == FrontViewPositionRight) {
    NSLog(@"Done Right");
        
    coverView =  [self onAddDimViewOnSuperView:self.view];
        
    [self.view setUserInteractionEnabled:FALSE];
        
    [self.view addSubview:coverView];
    }
    
    if (position == FrontViewPositionLeft) {
    [self.view setUserInteractionEnabled:TRUE];
        
    [coverView removeFromSuperview];
    }
    
}*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
