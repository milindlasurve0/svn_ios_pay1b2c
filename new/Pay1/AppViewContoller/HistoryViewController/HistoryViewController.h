//
//  HistoryViewController.h
//  Pay1
//
//  Created by webninjaz on 19/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"

@interface HistoryViewController : BaseViewController{
    IBOutlet UIImageView *firstThinLineImageView;
    IBOutlet UIImageView *secondThinLineImageView;
    
    IBOutlet UIImage *transactionHistoryImage;
    IBOutlet UIImageView *transactionHistoryImageView;
    IBOutlet UIImage *walletHistoryImage;
    IBOutlet UIImageView *walletHistoryImageView;
    IBOutlet UIImage *coinHistoryImage;
    IBOutlet UIImageView *coinHistoryImageView;
    IBOutlet UIImage *giftHistoryImage;
    IBOutlet UIImageView *giftHistoryImageView;
    
    IBOutlet UILabel *transactionLabel;
    IBOutlet UILabel *walletLabel;
    IBOutlet UILabel *coinHistoryLabel;
    IBOutlet UILabel *historyLabel;
    
    
    IBOutlet UIImage *firstRightArrowImage;
    IBOutlet UIImageView *firstRightArrowImageView;
    IBOutlet UIImage *secondRightArrowImage;
    IBOutlet UIImageView *secondRightArrowImageView;
    IBOutlet UIImage *thirdRightArrowImage;
    IBOutlet UIImageView *thirdRightArrowImageView;
    
    
    IBOutlet UIView *firstThinView;
    IBOutlet UIView *secondThinView;
    IBOutlet UIView *thirdThinView;
    IBOutlet UIView *fouthThinView;
    
    
    IBOutlet UIButton *transactionHistoryButton;
    IBOutlet UIButton *walletHistoryButton;
    IBOutlet UIButton *coinHistoryButton;
    
    UIView *coverView;

}

@end
