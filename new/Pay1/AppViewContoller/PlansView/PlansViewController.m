//
//  PlansViewController.m
//  Pay1
//
//  Created by Annapurna on 15/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "PlansViewController.h"
#import "PlanCustomCell.h"
#import "PlanList.h"
#import "DBManager.h"
#import "GetPlanResponse.h"
@interface PlansViewController ()

@end

@implementation PlansViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    planNotFoundLabel.hidden = TRUE;
    
    
    
    topUpTV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    threeGTV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    otherTV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    twoGTV.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    
    NSLog(@"#OpID :%@ %@",_selectOperatorID,_selectOperatorName);
    
    if([UIScreen mainScreen].bounds.size.width == 320){
    dataTwoButton.frame = CGRectMake(threeGButton.frame.origin.x + threeGButton.frame.size.width + 10, 9, 87, 30);
    otherButton.frame = CGRectMake(dataTwoButton.frame.origin.x + dataTwoButton.frame.size.width + 10, 9, 73, 30);
    topUpButton.frame = CGRectMake(otherButton.frame.origin.x + otherButton.frame.size.width + 10, 9, 64, 30);
    }
    
    circleTV.frame = CGRectMake(12, 74, self.view.frame.size.width - 20, self.view.frame.size.height - 100);
    
    circleNameArrayList = [[NSMutableArray alloc]init];
    planAmountArrayList = [[NSMutableArray alloc]init];
    planValidityArrayList = [[NSMutableArray alloc]init];
    planDescArrayList = [[NSMutableArray alloc]init];
    
    
    [self hideNavBackButton];
    
    isCityTapped = FALSE;
    
    circleTV.layer.cornerRadius = 5;
    circleTV.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    circleTV.layer.borderWidth = 5;
    [circleTV removeFromSuperview];
    
    [threeGView removeFromSuperview];
    [blackHeaderView removeFromSuperview];
    
    UITapGestureRecognizer *cityTapGes = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cityViewTapped:)];
    cityTapGes.delegate = self;
    [cityView addGestureRecognizer:cityTapGes];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0.0f, 0.0f, 20.0f, 20.0f)];
    [btn addTarget:self action:@selector(backClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btn setImage:[UIImage imageNamed:@"move_back.png"] forState:UIControlStateNormal];
    UIBarButtonItem *eng_btn = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = eng_btn;
    
    self.title = [NSString stringWithFormat:@"Plans - %@",_selectOperatorName];
    
    pageScrollView.pagingEnabled = YES;
    pageScrollView.showsHorizontalScrollIndicator = NO;
    pageScrollView.delegate = self;

    [self fillCircleDictionary];
    
    if(_isShowPlanDetails == TRUE){
        
    isCityTapped = TRUE;
        
    [self.view addSubview:blackHeaderView];
    blackHeaderView.frame = CGRectMake(0, 122, 375, 50);
    pageScrollView.frame = CGRectMake(0, 172, 375,[UIScreen mainScreen].bounds.size.height - 172 );
    threeGView.frame = CGRectMake(0, 0, pageScrollView.frame.size.width, pageScrollView.frame.size.height);
    twoGView.frame = CGRectMake(375, 0, pageScrollView.frame.size.width, pageScrollView.frame.size.height);
    otherView.frame = CGRectMake(750, 0, pageScrollView.frame.size.width, pageScrollView.frame.size.height);
    topUpView.frame = CGRectMake(1125, 0, pageScrollView.frame.size.width, pageScrollView.frame.size.height);

        
        
   // [pageScrollView addSubview:threeGView];
        
    cityTxtLabel.text = _selectCirName;

        
    if(_objServiceType == kServiceTypeMobile || _objServiceType == kServiceTypePostPaid){
        
    [pageScrollView setContentSize:CGSizeMake(pageScrollView.frame.size.width * 4, [UIScreen mainScreen].bounds.size.height - 172)];

        
    }
        
        
    if(_objServiceType == kServiceTypeDTH ){
    objDataSelectedType = kDataTypeTopup;
    cityView.hidden = TRUE;
    threeGButton.hidden = TRUE;
    dataTwoButton.hidden = TRUE;
    otherButton.hidden = TRUE;
        
    threeGView.hidden = TRUE;
        
    blackHeaderView.frame = CGRectMake(0, 64, 375, 50);
       
    [topUpButton setTitle:@"TOPUP - PLANS" forState:UIControlStateNormal];
            
    [topUpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    topUpButton.frame = CGRectMake(self.view.frame.origin.x + 5, 9, 150, 30);
            
    headerUnderLineView.frame = CGRectMake(topUpButton.frame.origin.x +20 , 35, topUpButton.titleLabel.frame.origin.x + topUpButton.titleLabel.frame.size.width + 30, 3);

    pageScrollView.frame = CGRectMake(0,blackHeaderView.frame.origin.y + blackHeaderView.frame.size.height , 375, self.view.frame.size.height - 114);
        
    topUpView.frame = CGRectMake(0, 0,pageScrollView.frame.size.width, pageScrollView.frame.size.height );
    topUpTV.frame = CGRectMake(0, 0, topUpView.frame.size.width, topUpView.frame.size.height);

        
   // [pageScrollView setContentSize:CGSizeMake(pageScrollView.frame.size.width , pageScrollView.frame.size.height)];

    }
        
        
    if(_objServiceType == kServiceTypeDataCard ){
    otherButton.hidden = TRUE;
    topUpButton.hidden = TRUE;
        
    [pageScrollView setContentSize:CGSizeMake(pageScrollView.frame.size.width * 2,[UIScreen mainScreen].bounds.size.height - 172)];
    threeGView.frame = CGRectMake(0, 0, pageScrollView.frame.size.width, pageScrollView.frame.size.height);
    twoGView.frame = CGRectMake(375, 0, pageScrollView.frame.size.width, pageScrollView.frame.size.height);
    }
   [self getDataFromDBForPlanType];
        
    }
}

-(void)fillCircleDictionary{
    
    NSDictionary *cicrcleDic1 = [NSDictionary dictionaryWithObjectsAndKeys:@"Andhra Pradesh",@"circle_name",@"AP",@"circle_id",nil];
    
    NSDictionary *cicrcleDic2 = [NSDictionary dictionaryWithObjectsAndKeys:@"Assam",@"circle_name",@"AS",@"circle_id",nil];
    
    NSDictionary *cicrcleDic3 = [NSDictionary dictionaryWithObjectsAndKeys:@"Chennai",@"circle_name",@"CH",@"circle_id",nil];
    
    NSDictionary *cicrcleDic4 = [NSDictionary dictionaryWithObjectsAndKeys:@"Delhi NCR",@"circle_name",@"DL",@"circle_id",nil];
    
    
    NSDictionary *cicrcleDic5 = [NSDictionary dictionaryWithObjectsAndKeys:@"Gujrat",@"circle_name",@"GJ",@"circle_id",nil];
    NSDictionary *cicrcleDic21 = [NSDictionary dictionaryWithObjectsAndKeys:@"Haryana",@"circle_name",@"NE",@"circle_id",nil];
    NSDictionary *cicrcleDic7 = [NSDictionary dictionaryWithObjectsAndKeys:@"Himachal Pradesh",@"circle_name",@"HP",@"circle_id",nil];
    
    NSDictionary *cicrcleDic8 = [NSDictionary dictionaryWithObjectsAndKeys:@"Jammu & Kashmir",@"circle_name",@"JK",@"circle_id",nil];
    
    NSDictionary *cicrcleDic9 = [NSDictionary dictionaryWithObjectsAndKeys:@"Jharkhand",@"circle_name",@"BR",@"circle_id",nil];
    
    NSDictionary *cicrcleDic10 = [NSDictionary dictionaryWithObjectsAndKeys:@"Karnatka",@"circle_name",@"KA",@"circle_id",nil];
    
    NSDictionary *cicrcleDic22 = [NSDictionary dictionaryWithObjectsAndKeys:@"Kerala",@"circle_name",@"KL",@"circle_id",nil];

    
    NSDictionary *cicrcleDic11 = [NSDictionary dictionaryWithObjectsAndKeys:@"Kolkata",@"circle_name",@"KO",@"circle_id",nil];
    
    NSDictionary *cicrcleDic12 = [NSDictionary dictionaryWithObjectsAndKeys:@"Madhya Pradesh",@"circle_name",@"MP",@"circle_id",nil];
    
    NSDictionary *cicrcleDic13 = [NSDictionary dictionaryWithObjectsAndKeys:@"Maharashtra",@"circle_name",@"MH",@"circle_id",nil];
    
    NSDictionary *cicrcleDic14 = [NSDictionary dictionaryWithObjectsAndKeys:@"Mumbai",@"circle_name",@"MU",@"circle_id",nil];
    
    NSDictionary *cicrcleDic15 = [NSDictionary dictionaryWithObjectsAndKeys:@"Odhisha",@"circle_name",@"OR",@"circle_id",nil];
    
    NSDictionary *cicrcleDic16 = [NSDictionary dictionaryWithObjectsAndKeys:@"Punjab",@"circle_name",@"PB",@"circle_id",nil];
    NSDictionary *cicrcleDic17 = [NSDictionary dictionaryWithObjectsAndKeys:@"Rajasthan",@"circle_name",@"RJ",@"circle_id",nil];
    NSDictionary *cicrcleDic18 = [NSDictionary dictionaryWithObjectsAndKeys:@"Tamil Nadu",@"circle_name",@"TN",@"circle_id",nil];
    
    NSDictionary *cicrcleDic6 = [NSDictionary dictionaryWithObjectsAndKeys:@"Tripura",@"circle_name",@"NE",@"circle_id",nil];
    
    NSDictionary *cicrcleDic23 = [NSDictionary dictionaryWithObjectsAndKeys:@"Uttar Pradesh (East)",@"circle_name",@"UE",@"circle_id",nil];

    NSDictionary *cicrcleDic19 = [NSDictionary dictionaryWithObjectsAndKeys:@"Uttarakhand",@"circle_name",@"UW",@"circle_id",nil];


    
    
    

    NSDictionary *cicrcleDic20 = [NSDictionary dictionaryWithObjectsAndKeys:@"West Bengal",@"circle_name",@"WB",@"circle_id",nil];
    
    
    

    
    [circleNameArrayList addObject:cicrcleDic1];
    [circleNameArrayList addObject:cicrcleDic2];
    [circleNameArrayList addObject:cicrcleDic3];
    [circleNameArrayList addObject:cicrcleDic4];
    [circleNameArrayList addObject:cicrcleDic5];
    
    [circleNameArrayList addObject:cicrcleDic21];

    
    [circleNameArrayList addObject:cicrcleDic7];
    

    
    [circleNameArrayList addObject:cicrcleDic8];
    [circleNameArrayList addObject:cicrcleDic9];
    [circleNameArrayList addObject:cicrcleDic10];
    
    [circleNameArrayList addObject:cicrcleDic22];

    
    [circleNameArrayList addObject:cicrcleDic11];
    [circleNameArrayList addObject:cicrcleDic12];
    [circleNameArrayList addObject:cicrcleDic13];
    [circleNameArrayList addObject:cicrcleDic14];
    [circleNameArrayList addObject:cicrcleDic15];
    [circleNameArrayList addObject:cicrcleDic16];
    [circleNameArrayList addObject:cicrcleDic17];
    [circleNameArrayList addObject:cicrcleDic18];
    
    [circleNameArrayList addObject:cicrcleDic6];
    
    [circleNameArrayList addObject:cicrcleDic23];


    
    [circleNameArrayList addObject:cicrcleDic19];
    [circleNameArrayList addObject:cicrcleDic20];
    



  //  NSLog(@"#Values :%@",circleNameArrayList);

}

#pragma mark - Database

-(void)getDataFromDBForPlanType{
    
    [planValidityArrayList removeAllObjects];
    [planAmountArrayList removeAllObjects];
    [planDescArrayList removeAllObjects];
    
    NSString *planTypeStr ;
    
    if(objDataSelectedType == kDataTypeThreeG){
        planTypeStr = @"3G";
    }
    if(objDataSelectedType == kDataTypeOther){
        planTypeStr = @"Other";
    }
    if(objDataSelectedType == kDataTypeTopup){
    planTypeStr = @"Topup";
    if(_objServiceType == kServiceTypeDTH){
    planTypeStr = @"Topup-Plans";
    }
    }
    if(objDataSelectedType == kDataTypeTwoG){
    planTypeStr = @"Data/2G";
        
        
    }
    
    
    
   //   NSString *query = [NSString stringWithFormat:@"select * from plandatatable WHERE circle_id=\"%@\" AND plan_type=\"%@\" AND operator_name=\"%@\"",_selectCirID,planTypeStr,_selectOperatorName];
    
    
    DBManager *  objDBManager = [[DBManager alloc]initWithDatabaseFilename:@"plansdb.sql"];

    NSString *query = [NSString stringWithFormat:@"select * from plandatatable WHERE circle_id=\"%@\" AND plan_type=\"%@\" AND operator_id=\"%@\"  ORDER BY plan_amount ASC",_selectCirID,planTypeStr,_selectOperatorID];
    
  //  NSString *query = [NSString stringWithFormat:@"select * from plandatatable WHERE circle_id=\"%@\" AND plan_type=\"%@\" AND operator_id=\"%@\"  ORDER BY plan_amount ASC",_selectCirID,planTypeStr,@"15"];
    
 //    NSString *query = [NSString stringWithFormat:@"select * from plandatatable WHERE circle_id=\"%@\" AND plan_type=\"%@\" AND operator_id=\"%@\"  ORDER BY plan_amount ASC",_selectCirID,planTypeStr,@"15"];
    
   //  NSString *query = [NSString stringWithFormat:@"select * from plandatatable WHERE circle_id=\"%@\" AND plan_type=\"%@\" AND operator_id=\"%@\"  ORDER BY plan_amount ASC",_selectCirID,planTypeStr,@"15"];
    
   //  NSString *query = [NSString stringWithFormat:@"select * from plandatatable WHERE circle_id=\"%@\" AND operator_id=\"%@\"",_selectCirID,@"15"];
    
   // NSString *query = [NSString stringWithFormat:@"select * from plandatatable"];

//   NSLog(@"#PlanQuery :%@",query);
    
  //  NSLog(@"DB of Plan:%lu %@",(unsigned long)[[NSArray alloc] initWithArray:[objDBManager loadDataFromDB:query]].count,[[NSArray alloc] initWithArray:[objDBManager loadDataFromDB:query]]);
    
    
    NSArray *theArray = [[NSArray alloc] initWithArray:[objDBManager loadDataFromDB:query]];
    
   

   // NSLog(@"#TheArrayPlan :%@",theArray);
    
    [self hideHUDOnView];
    
    if(objDataSelectedType == kDataTypeThreeG && theArray.count == 0){
       // [self showAlert];
        
        if(theArray.count == 0){
        pageScrollView.hidden = TRUE;
            blackHeaderView.hidden = TRUE;
            
            planNotFoundLabel.hidden = FALSE;

        }
        
    }
    
   else if(objDataSelectedType == kDataTypeTopup && theArray.count == 0){
      //  [self showAlert];
        if(theArray.count == 0){
            pageScrollView.hidden = TRUE;
            blackHeaderView.hidden = TRUE;
            
            planNotFoundLabel.hidden = FALSE;

        }
       
        
    }
    
   else if( objDataSelectedType == kDataTypeOther && theArray.count == 0){
      //  [self showAlert];
        
        if(theArray.count == 0){
            pageScrollView.hidden = TRUE;
            blackHeaderView.hidden = TRUE;
            
            planNotFoundLabel.hidden = FALSE;

        }
       

    }
    
   else if(objDataSelectedType == kDataTypeTwoG && theArray.count == 0){
      //  [self showAlert];
        
        if(theArray.count == 0){
            pageScrollView.hidden = TRUE;
            blackHeaderView.hidden = TRUE;
            
            planNotFoundLabel.hidden = FALSE;

        }
       

    }
    
    
    else{
        
        planNotFoundLabel.hidden = TRUE;

        
        pageScrollView.hidden = FALSE;
        blackHeaderView.hidden = FALSE;
        
       /* if(threeGTV.hidden == TRUE){
            threeGTV.hidden = FALSE;
        }
        
        if(twoGTV.hidden == TRUE){
            twoGTV.hidden = FALSE;
        }
        
        if(otherTV.hidden == TRUE){
            otherTV.hidden = FALSE;
        }
        
        if(topUpTV.hidden == TRUE){
            topUpTV.hidden = FALSE;
        }*/

        
        
        
        
    NSInteger indexOfPlanrName = [objDBManager.arrColumnNames indexOfObject:@"plan_validity"];
    for(int i=0;i<theArray.count;i++){
    [planValidityArrayList addObject:[[theArray objectAtIndex:i] objectAtIndex:indexOfPlanrName]];
    }
    
    NSInteger indexOfPlanAmountID = [objDBManager.arrColumnNames indexOfObject:@"plan_amount"];
    for(int i=0;i<theArray.count;i++){
    [planAmountArrayList addObject:[[theArray objectAtIndex:i] objectAtIndex:indexOfPlanAmountID]];
    }
    
    NSInteger indexOfPlanDesc = [objDBManager.arrColumnNames indexOfObject:@"plan_description"];
    for(int i=0;i<theArray.count;i++){
    [planDescArrayList addObject:[[theArray objectAtIndex:i] objectAtIndex:indexOfPlanDesc]];
    }
    
    
    [self.view addSubview:blackHeaderView];
        
        
    if(objDataSelectedType == kDataTypeThreeG){
    [pageScrollView addSubview:threeGTV];
    [threeGTV reloadData];
    }
    if(objDataSelectedType == kDataTypeTwoG){
    twoGTV.delegate = self;
    twoGTV.dataSource = self;
    [twoGTV reloadData];
    }
    if(objDataSelectedType == kDataTypeOther){
    otherTV.delegate = self;
    otherTV.dataSource = self;
    [otherTV reloadData];
    }
    if(objDataSelectedType == kDataTypeTopup){
    topUpTV.delegate = self;
    topUpTV.dataSource = self;
    [topUpTV reloadData];
    }
    }
 
}

#pragma mark - TableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == circleTV){
    return circleNameArrayList.count;
    }
    
    else{
        return planAmountArrayList.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    // Dequeue the cell.
    if(tableView == circleTV ){
    NSLog(@"#InCircleTable");
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [[circleNameArrayList objectAtIndex:indexPath.row]objectForKey:@"circle_name"];
    cell.textLabel.textColor = [UIColor darkGrayColor];
    cell.textLabel.font = [UIFont fontWithName:@"Open Sans" size:17];
    
    return cell;
    }
    
    else{
    static NSString *planTableIdentifier = @"PlanCustomCell";
    if(objDataSelectedType == kDataTypeThreeG){
            
    objPlanCustomCell=(PlanCustomCell *)[tableView dequeueReusableCellWithIdentifier:planTableIdentifier];
            
    if(objPlanCustomCell==nil){
    [[NSBundle mainBundle] loadNibNamed:@"PlanCustomCell" owner:self options:nil];
    }
            
        
            
    NSString *validity = [planValidityArrayList objectAtIndex:indexPath.row];
    NSString *amount = [planAmountArrayList objectAtIndex:indexPath.row];
    NSString *desc = [planDescArrayList objectAtIndex:indexPath.row];
    [objPlanCustomCell showPlanData:validity planAmount:amount planDesc:desc];
            
        
            
    return objPlanCustomCell;
    }
        
    if(objDataSelectedType == kDataTypeTwoG){
            
    objPlanCustomCell=(PlanCustomCell *)[tableView dequeueReusableCellWithIdentifier:planTableIdentifier];
            
    if(objPlanCustomCell==nil){
    [[NSBundle mainBundle] loadNibNamed:@"PlanCustomCell" owner:self options:nil];
    }
        
    NSString *validity = [planValidityArrayList objectAtIndex:indexPath.row];
    NSString *amount = [planAmountArrayList objectAtIndex:indexPath.row];
    NSString *desc = [planDescArrayList objectAtIndex:indexPath.row];
    [objPlanCustomCell showPlanData:validity planAmount:amount planDesc:desc];
            
    return objPlanCustomCell;
    }
        
        
   if(objDataSelectedType == kDataTypeOther){
            
    objPlanCustomCell=(PlanCustomCell *)[tableView dequeueReusableCellWithIdentifier:planTableIdentifier];
            
    if(objPlanCustomCell==nil){
    [[NSBundle mainBundle] loadNibNamed:@"PlanCustomCell" owner:self options:nil];
    }
       
       
    NSString *validity = [planValidityArrayList objectAtIndex:indexPath.row];
    NSString *amount = [planAmountArrayList objectAtIndex:indexPath.row];
    NSString *desc = [planDescArrayList objectAtIndex:indexPath.row];
    [objPlanCustomCell showPlanData:validity planAmount:amount planDesc:desc];
            
    return objPlanCustomCell;
    }
        
        
    if(objDataSelectedType == kDataTypeTopup){
            
    objPlanCustomCell=(PlanCustomCell *)[tableView dequeueReusableCellWithIdentifier:planTableIdentifier];
        
    if(objPlanCustomCell==nil){
    [[NSBundle mainBundle] loadNibNamed:@"PlanCustomCell" owner:self options:nil];
    }
    NSString *validity = [planValidityArrayList objectAtIndex:indexPath.row];
    NSString *amount = [planAmountArrayList objectAtIndex:indexPath.row];
    NSString *desc = [planDescArrayList objectAtIndex:indexPath.row];
        
    [objPlanCustomCell showPlanData:validity planAmount:amount planDesc:desc];
            
    return objPlanCustomCell;
    }
 
    }
    
    
    

    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(tableView == circleTV){
        
    if(_isShowPlanDetails == FALSE){
    objDataSelectedType = kDataTypeThreeG;
    }

    NSString *cirID  = [[circleNameArrayList objectAtIndex:indexPath.row]objectForKey:@"circle_id"];
    _selectCirName  = [[circleNameArrayList objectAtIndex:indexPath.row]objectForKey:@"circle_name"];
        
    _selectCirID = cirID;
        
    cityTxtLabel.text = _selectCirName;

    NSLog(@"#CirID :%@",cirID);
        
        [self hideAndShowCircleTV];

        
    [self getDataFromDBForPlanType];

    }
    
    else{
    NSString *planAmount = [planAmountArrayList objectAtIndex:indexPath.row];

    
    if((tableView == topUpTV || tableView == threeGTV || tableView == otherTV || tableView == twoGTV) && (_objServiceType == kServiceTypeMobile || _objServiceType == kServiceTypePostPaid)){
       
        
    NSLog(@"#CheckNameAndID :%@",planAmount);
        
    if (_onSelectPlan) {
    _onSelectPlan(planAmount,kServiceTypeMobile);
    }
    [self.navigationController popViewControllerAnimated:YES];
    }
    
    if((tableView == threeGTV || tableView == twoGTV) && _objServiceType == kServiceTypeDataCard){
    if (_onSelectPlan) {
    _onSelectPlan(planAmount,kServiceTypeDataCard);
    }
    [self.navigationController popViewControllerAnimated:YES];

    }
    
    if(tableView == topUpTV &&  _objServiceType == kServiceTypeDTH){
    if (_onSelectPlan) {
    _onSelectPlan(planAmount,kServiceTypeDTH);
    }
    [self.navigationController popViewControllerAnimated:YES];

    }
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == topUpTV || tableView == threeGTV || tableView == twoGTV || tableView == otherTV){
    NSString *validity = [planValidityArrayList objectAtIndex:indexPath.row];
    
    CGSize maximumLabelSize = CGSizeMake([UIScreen mainScreen].bounds.size.width - 20,9999);
    
    CGRect textRect = [validity
                       boundingRectWithSize:maximumLabelSize
                       options:NSStringDrawingUsesLineFragmentOrigin
                       attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Open Sans" size:12.0]}
                       context:nil];

    
    CGSize size = textRect.size;
    
    return size.height + 100;
    }
    
    else{
        return 44;
    }
}

#pragma Gesture

-(void)cityViewTapped:(UITapGestureRecognizer *)sender{
    if(isCityTapped == TRUE){
    isCityTapped = FALSE;
    }
    
    [self hideAndShowCircleTV];
}

-(void)hideAndShowCircleTV{
  //  NSLog(@"#IsCircle :%d %@",isCityTapped,circleNameArrayList);
    
    
    if(isCityTapped == FALSE){
    [UIView animateWithDuration:0.75 animations:^{
    [self.view addSubview:circleTV];
    [circleTV reloadData];
    } completion:^(BOOL finished) {
    }];
    }
    else{
    [UIView animateWithDuration:0.75 animations:^{
    [circleTV removeFromSuperview];
    } completion:^(BOOL finished) {
    }];
    }
    
  //  if(isCityTapped == FALSE)
    isCityTapped = !isCityTapped;
}


#pragma mark - Click Events


-(IBAction)threeGClicked:(id)sender{
    
    int contentOffSet = pageScrollView.contentOffset.x ;
    
    NSLog(@"#TheContentOffSet is :%d",contentOffSet);
    
    
    [pageScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    objDataSelectedType = kDataTypeThreeG;
    [self setTextColorAccrdingToClick:kDataTypeThreeG];
    
  
}

-(IBAction)dataTwoGClicked:(id)sender{
    
    [pageScrollView setContentOffset:CGPointMake(375, 0) animated:YES];

    
    objDataSelectedType = kDataTypeTwoG;
    
    [self setTextColorAccrdingToClick:kDataTypeTwoG];
    

}

-(IBAction)otherClicked:(id)sender{
    
    [pageScrollView setContentOffset:CGPointMake(750, 0) animated:YES];

    
    objDataSelectedType = kDataTypeOther;
    
    [self setTextColorAccrdingToClick:kDataTypeOther];
    

}

-(IBAction)topUpClicked:(id)sender{
    
    [pageScrollView setContentOffset:CGPointMake(1125, 0) animated:YES];

    
    objDataSelectedType = kDataTypeTopup;
    
    [self setTextColorAccrdingToClick:kDataTypeTopup];
    

}

#pragma mark - ScrollView

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    int contentOffset = pageScrollView.contentOffset.x;
    
  //  NSLog(@"#Scroll :%d",contentOffset);
    if(scrollView == pageScrollView){
    if(contentOffset == 0){
        
    objDataSelectedType = kDataTypeThreeG;
    [self setTextColorAccrdingToClick:kDataTypeThreeG];
    }
    
    if(contentOffset == 375){

    objDataSelectedType = kDataTypeTwoG;
    [self setTextColorAccrdingToClick:kDataTypeTwoG];
    }
    
    if(contentOffset == 750){
    objDataSelectedType = kDataTypeOther;
    [self setTextColorAccrdingToClick:kDataTypeOther];
    }
    
    if(contentOffset == 1125){
    objDataSelectedType = kDataTypeTopup;
    [self setTextColorAccrdingToClick:kDataTypeTopup];
    }
    }
}

-(void)setTextColorAccrdingToClick:(BOOL)isClickedType{
    [planDescArrayList removeAllObjects];
    [planAmountArrayList removeAllObjects];
    [planValidityArrayList removeAllObjects];
    
    
    [self showHUDOnView];
    
    if(objDataSelectedType == kDataTypeThreeG){
    [threeGButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [dataTwoButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [otherButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [topUpButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    headerUnderLineView.frame = CGRectMake(threeGButton.frame.origin.x , 35, threeGButton.titleLabel.frame.origin.x + threeGButton.titleLabel.frame.size.width, 3);

    }
    
    if(objDataSelectedType == kDataTypeTwoG){
    [dataTwoButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [threeGButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [otherButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [topUpButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    headerUnderLineView.frame = CGRectMake(dataTwoButton.frame.origin.x +3, 35, dataTwoButton.titleLabel.frame.origin.x + dataTwoButton.titleLabel.frame.size.width, 3);
    }
    
    if(objDataSelectedType == kDataTypeOther){
    [otherButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [dataTwoButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [threeGButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [topUpButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    headerUnderLineView.frame = CGRectMake(otherButton.frame.origin.x +6, 35, otherButton.titleLabel.frame.origin.x + otherButton.titleLabel.frame.size.width, 3);
    }
    
    if(objDataSelectedType == kDataTypeTopup){
    [topUpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [dataTwoButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [otherButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [threeGButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    headerUnderLineView.frame = CGRectMake(topUpButton.frame.origin.x +6, 35, topUpButton.titleLabel.frame.origin.x + topUpButton.titleLabel.frame.size.width, 3);
        
    }
    
    [self getDataFromDBForPlanType];

}

-(void)showAlert{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Pay1" message:@"No Plans are avaliable" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}



-(void)backClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
