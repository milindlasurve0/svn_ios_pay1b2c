//
//  FAQViewController.h
//  Pay1
//
//  Created by webninjaz on 16/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"

@interface FAQViewController : BaseViewController{
    IBOutlet UITableView *faqTableView;
    NSString *url;
    NSMutableDictionary *parsedJSON;
    IBOutlet UIWebView *webView;
    NSString *newStr;
}

@property (nonatomic,strong) NSMutableArray *faqDataArray;

@end
