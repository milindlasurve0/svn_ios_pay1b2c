//
//  WalletMoneyViewController.h
//  Pay1
//
//  Created by Annapurna on 13/07/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"

@interface WalletMoneyViewController : BaseViewController<UITextFieldDelegate>{
    IBOutlet UILabel *walletDetailTxtLabel;
    IBOutlet UIButton *confirmButton;
    IBOutlet UIButton *cancelButton;
    IBOutlet UITextField *txtField;
    
    IBOutlet UITextField *errortxtField;
    IBOutlet UIImageView *errorImageView;
    IBOutlet UIView *sepView;
    
    UIView *coverView;
    
    NSString *initialWalletBalance;
}
@property (nonatomic, assign) BOOL isFillWallet;


@end
