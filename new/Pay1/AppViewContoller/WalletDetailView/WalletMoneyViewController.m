//
//  WalletMoneyViewController.m
//  Pay1
//
//  Created by Annapurna on 13/07/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "WalletMoneyViewController.h"
#import "RechargeService.h"
#import "RechargeResponse.h"
#import "RechargeTransactionViewController.h"
@interface WalletMoneyViewController ()

@end

@implementation WalletMoneyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self hideNavBackButton];
    
    initialWalletBalance = [Constant getUserWalletBalance];
    
    
    [errorImageView removeFromSuperview];
    [errortxtField removeFromSuperview];
    
    if(_isFillWallet == TRUE){
    [self setNaviationButtonWithText:@"Online card payment" selector:@selector(backClicked:) isLeft:TRUE isReduceSize:FALSE isMyFont:18.0];
    }
    if(_isFillWallet == FALSE){
    [self setNaviationButtonWithText:@"Redeem coupon code" selector:@selector(backClicked:) isLeft:TRUE isReduceSize:FALSE isMyFont:18.0];
    walletDetailTxtLabel.text = @"Redeem your Pay1 coupon code here.";
    txtField.placeholder = @"COUPON CODE";
    txtField.keyboardType = UIKeyboardTypeDefault;

    }
    
    [self setViewOfButton:cancelButton];
    [self setConfirmVIewOfButton:confirmButton];
    
    [self addSpaceBetweenTextFieldOriginAndText:txtField];
    
    [self setUIForErrorViews:errortxtField];

}



-(IBAction)confirmClick:(id)sender{
    NSLog(@"#ConfirmClick");
    [txtField resignFirstResponder];
    [self showHUDOnView];
    
    if(txtField.text.length == 0){
    [self.view addSubview:errortxtField];
    [self.view addSubview:errorImageView];
    sepView.backgroundColor = [UIColor redColor];
        
    return;
    }
    
    else{
    [self showHUDOnView];

    if(_isFillWallet == TRUE){
    [self callFillWalletServiceBlock];
    }
    if(_isFillWallet == FALSE){
    [self callRedeemCouponCodeServiceBlock];
    }
    }
}

-(void)callFillWalletServiceBlock{
    RechargeService *objRechargeService = [[RechargeService alloc]init];
    [objRechargeService callOnlineWalletRefillService:txtField.text callback:^(NSError *error, RechargeResponse *objOnlineRefillResponse) {
    if(error)return ;
    else{
    [self hideHUDOnView];

    if([objOnlineRefillResponse.status isEqualToString:@"success"]){
    [self navigateToRechargeTransactionView:objOnlineRefillResponse];
    }
    if([objOnlineRefillResponse.status isEqualToString:@"failure"]){
    [self showAlertWithMessage:@"WALLET REFILL" alertMessage:objOnlineRefillResponse.descMessage];
    }

    }
    }];
}

-(void)callRedeemCouponCodeServiceBlock{
    [self hideHUDOnView];
   RechargeService *objRechargeService = [[RechargeService alloc]init];
    [objRechargeService callCouponRedeemService:txtField.text callback:^(NSError *error, RechargeResponse *objCouponRechargeResponse) {
    if(error)return ;
    else{
    if([objCouponRechargeResponse.status isEqualToString:@"success"]){
         //   [self navigateToRechargeTransactionView:objOnlineRefillResponse];
    NSLog(@"Coupon is Redeemed Successfully %@ %@",objCouponRechargeResponse.closingBalance,[Constant getUserWalletBalance]);
        
    [Constant saveUserWalletBalance:objCouponRechargeResponse.closingBalance];
        
    NSLog(@"After Coupon is Redeemed Successfully %@ ",[Constant getUserWalletBalance]);

        
    coverView = [self onAddDimViewOnSuperView:self.view];
    [coverView addSubview:[self showUIForSuccessRecharge]];
    }
    if([objCouponRechargeResponse.status isEqualToString:@"failure"]){
    [self showAlertWithMessage:@"COUPON CODE" alertMessage:objCouponRechargeResponse.descMessage];
    }
    }
    }];
}

-(void)showAlertWithMessage:(NSString *)alertTitle alertMessage:(NSString *)alertMessage{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:@"DONE" otherButtonTitles:nil, nil];
    [alert show];
}

-(void)navigateToRechargeTransactionView:(RechargeResponse *)response{

    RechargeTransactionViewController *objRechargeTVC = [self.storyboard instantiateViewControllerWithIdentifier:@"rechargeTransactionView"];
    objRechargeTVC.amountToRecharge = txtField.text;
    objRechargeTVC.objRechargeResponse = response;
    objRechargeTVC.typeStr = @"WALLET TOPUP";
    objRechargeTVC.operatorName = @"WALLET TOPUP";
    objRechargeTVC.numberToRecharge = [Constant getUserPhoneNumber];
    objRechargeTVC.isComingFrom = TRUE;
    objRechargeTVC.isComingFromGift = FALSE;
    objRechargeTVC.hidesBottomBarWhenPushed = TRUE;
    [self.navigationController pushViewController:objRechargeTVC animated:YES];
}

-(IBAction)cancelClick:(id)sender{
    NSLog(@"#CancelClick");
    txtField.text = @"";
    [txtField resignFirstResponder];
}
-(void)backClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return TRUE;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(textField.text.length > 0){
    [errorImageView removeFromSuperview];
    [errortxtField removeFromSuperview];
        
    sepView.backgroundColor = [UIColor lightGrayColor];
    }
    
    
    
    return TRUE;
}

-(UIView *)showUIForSuccessRecharge{
    
    UIView * cancelView = [[UIView alloc]initWithFrame:CGRectMake(20, 64+50, [UIScreen mainScreen].bounds.size.width - 40, 350)];
        
    cancelView.backgroundColor = [UIColor whiteColor];
        
    UILabel *statusLabel ;
    UILabel *txtLabel;
        
        
    statusLabel = [[UILabel alloc]initWithFrame:CGRectMake(cancelView.frame.origin.x + 70, 10, 100, 21)];
    statusLabel.text = @"SUCCESS";
    statusLabel.textAlignment = NSTextAlignmentCenter;
    txtLabel.textAlignment = NSTextAlignmentCenter;
    txtLabel =  [[UILabel alloc]initWithFrame:CGRectMake(80, 64, 206, 21)];
            
    txtLabel.text = @"WALLET TOPUP";
    
    statusLabel.font = [UIFont fontWithName:@"Open Sans" size:18];
    [cancelView addSubview:statusLabel];
        
        
    [self setTextOnLabel:txtLabel fontSize:18.0];
        
    [cancelView addSubview:txtLabel];
        
        
    UIView *detailInfoView = [[UIView alloc]initWithFrame:CGRectMake(50, txtLabel.frame.origin.y + txtLabel.frame.size.height + 30, 190, 60)];
    UIImageView *logoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 60, 60)];
    logoImageView.layer.borderColor = [UIColor grayColor].CGColor;
    logoImageView.layer.borderWidth = 1;
        
    [self setRoundedCornerForImageView:logoImageView];
        
        
    UIImageView *operatotrLogoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(1, 1, 60, 60)];
    operatotrLogoImageView.image = [UIImage imageNamed:@"wallet_top_ups.png"];

    
    detailInfoView.layer.borderColor = [UIColor grayColor].CGColor;
    detailInfoView.layer.borderWidth = 1;
    [logoImageView addSubview:operatotrLogoImageView];
        
    [detailInfoView addSubview:logoImageView];
        
    UIView *horiSepView = [[UIView alloc]initWithFrame:CGRectMake(logoImageView.frame.origin.x + logoImageView.frame.size.width, logoImageView.frame.origin.y + logoImageView.frame.size.height/2, 130 , 1)];
    horiSepView.backgroundColor = [UIColor lightGrayColor];
    [detailInfoView addSubview:horiSepView];
        
    UILabel *numLabel = [[UILabel alloc]initWithFrame:CGRectMake(logoImageView.frame.origin.x + logoImageView.frame.size.width + 10, 5, 100, 21)];
    numLabel.text = [Constant getUserPhoneNumber];
    [self setTextOnLabel:numLabel fontSize:17.0];
    numLabel.textAlignment = NSTextAlignmentCenter;
        
        
    [detailInfoView addSubview:numLabel];
        
        
        
    UILabel *amountLabel = [[UILabel alloc]initWithFrame:CGRectMake(logoImageView.frame.origin.x + logoImageView.frame.size.width + 10, horiSepView.frame.origin.y + horiSepView.frame.size.height + 5, 100, 21)];
    amountLabel.text = [NSString stringWithFormat:@"Rs. %d",[[Constant getUserWalletBalance] intValue] - [initialWalletBalance intValue]] ; //txtField.text;
    [self setTextOnLabel:amountLabel fontSize:17.0];
    amountLabel.textAlignment = NSTextAlignmentCenter;
        
    [detailInfoView addSubview:amountLabel];
        
        
    [cancelView addSubview:detailInfoView];
        
    
        
    UIView *missedInfoView = [[UIView alloc]initWithFrame:CGRectMake(20, detailInfoView.frame.origin.y + detailInfoView.frame.size.height + 38, 190, 60)];
    UIImageView *phoneImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 10, 30, 30)];
    logoImageView.layer.borderColor = [UIColor grayColor].CGColor;
    logoImageView.layer.borderWidth = 1;
    phoneImageView.image = [UIImage imageNamed:@"telephone_mark.png"];
        
        
    UILabel *infoLabel = [[UILabel alloc]initWithFrame:CGRectMake(phoneImageView.frame.origin.x + phoneImageView.frame.size.width + 10, 5,(missedInfoView.frame.origin.x+ missedInfoView.frame.size.width - 30), 80)];
    infoLabel.text = @"Give us a missed call to raise the compliant of this transaction on 02267242266";
    infoLabel.font = [UIFont fontWithName:@"Open Sans" size:12];
    infoLabel.lineBreakMode = NSLineBreakByWordWrapping;
    infoLabel.numberOfLines = 0;
    [infoLabel sizeToFit];
    
        
    [missedInfoView addSubview:phoneImageView];
    [missedInfoView addSubview:infoLabel];
        
    [cancelView addSubview:missedInfoView];
        
        
    UIView *horizontalSepView;
        
    horizontalSepView = [[UIView alloc]initWithFrame:CGRectMake(0, missedInfoView.frame.origin.y + missedInfoView.frame.size.height + 20, cancelView.frame.size.width , 1)];
        
        
    horizontalSepView.backgroundColor = [UIColor lightGrayColor];
    [cancelView addSubview:horizontalSepView];
    
    
    UIButton *okButton = [UIButton buttonWithType:UIButtonTypeCustom];
    okButton.frame = CGRectMake(0, horizontalSepView.frame.origin.y + horizontalSepView.frame.size.height , (cancelView.frame.origin.x + cancelView.frame.size.width ),  cancelView.frame.size.height - (horizontalSepView.frame.origin.y + horizontalSepView.frame.size.height));
    [okButton setTitle:@"DONE" forState:UIControlStateNormal];
    okButton.titleLabel.font = [UIFont fontWithName:@"OpenSans-SemiBold" size:17];
    [okButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [okButton addTarget:self action:@selector(okCLicked:) forControlEvents:UIControlEventTouchUpInside];
    [cancelView addSubview:okButton];
    
    
    return cancelView;
}


-(void)okCLicked:(id)sender{
    NSLog(@"#OK Clicked");
    [self.navigationController popViewControllerAnimated:YES];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
