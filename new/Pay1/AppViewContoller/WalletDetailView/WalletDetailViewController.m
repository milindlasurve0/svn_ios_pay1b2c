//
//  WalletDetailViewController.m
//  Pay1
//
//  Created by Annapurna on 13/07/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "WalletDetailViewController.h"
#import "WalletMoneyViewController.h"
#import "StoreLocationViewController.h"
#import "CoinViewController.h"
#import "Session.h"
@interface WalletDetailViewController ()

@end

@implementation WalletDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self hideNavBackButton];
    
    scrollView.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, self.view.frame.size.height+50);
    
    if([UIScreen mainScreen].bounds.size.width == 320){
    locateShopImageView.frame = CGRectMake(120, 88, 100,100);
    cardPaymentImageView.frame = CGRectMake(120, 203, 100,100);
    redeemCouponImageView.frame = CGRectMake(120, 325, 100,100);

    }
    
    NSLog(@"FrameWidth :%f",self.view.frame.size.width);
    
    
   
    

}

-(void)viewWillAppear:(BOOL)animated{
    [self setTwoNavigationButtonsWithImages:@"money_sliding_menu.png" secondImage:@"coin_sling_menu.png" thirdImage:@"magnifying_glass.png" isLeft:FALSE selector1:@selector(moneyButtonClicked:) selector2:@selector(coinButtonClicked:) selector3:@selector(searchBtnClicked:) isFromGift:FALSE ];
    
    [self setMyWalletDataOnView];

}

-(void)setMyWalletDataOnView{
    walletTextLabel.text = [NSString stringWithFormat:@"Rs. %@",[Session shared].objSignInResponse.userWalletBalance];
}

-(IBAction)storeLocationClicked:(id)sender{
    StoreLocationViewController *storeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"storeViewC"];
    storeVC.hidesBottomBarWhenPushed = TRUE;
    [self.navigationController pushViewController:storeVC animated:YES];
}

-(IBAction)onlinePaymentClicked:(id)sender{
    [self navigateToWalletMoney:TRUE];
}
-(IBAction)redeemCouponClicked:(id)sender{
    [self navigateToWalletMoney:FALSE];
}


-(void)navigateToWalletMoney:(BOOL)isFill{
    WalletMoneyViewController *walletMoneyVC = [self.storyboard instantiateViewControllerWithIdentifier:@"walletMoneyVC"];
    walletMoneyVC.isFillWallet = isFill;
    walletMoneyVC.hidesBottomBarWhenPushed = TRUE;
    [self.navigationController pushViewController:walletMoneyVC animated:YES];
}

-(void)coinButtonClicked:(id)sender{
    NSLog(@"#CoinClicked");
    
    CoinViewController *coinVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CoinView"];
    coinVC.hidesBottomBarWhenPushed = TRUE;
    [self.navigationController pushViewController:coinVC animated:YES];
}

-(void)moneyButtonClicked:(id)sender{
    NSLog(@"#Wallet");
}

-(void)searchBtnClicked:(id)sender{
    NSLog(@"#Search");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
