//
//  CoinViewController.h
//  Pay1
//
//  Created by webninjaz on 01/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"

@class CoinHistoryService;
@class CoinHistoryCriteria;
@class CoinHistoryResponse;
@class CoinTableViewCell;

@interface CoinViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource>{
    
    IBOutlet UITableView *coinTableView;
    IBOutlet UILabel *coinHistoryLabel;
    IBOutlet UIImageView *coinHistoryImageView;
    IBOutlet CoinTableViewCell *coinTableViewCell;
}

@property (nonatomic,strong) NSMutableArray *coinArray;

@end
