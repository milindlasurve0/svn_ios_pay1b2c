//
//  GiftDetailMapViewController.m
//  Pay1
//
//  Created by Annapurna on 06/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//
#import "GiftDetailMapViewController.h"
#import "GiftLocationList.h"
#import "CurrentLocation.h"
#import "MapCustomCell.h"
#import "UIImage+Utility.h"
#import "GiftOfferDetailList.h"
#import "CustomInfoWindow.h"
#import "GiftDetailCommonView.h"
#import "GiftSingleViewController.h"
@interface GiftDetailMapViewController ()

@end

@implementation GiftDetailMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _mapView.delegate = self;
    
    [self setRoundedCornerForImageView:thumbImageView];
    
    [self hideNavBackButton];
    NSString *name = [_shopName stringByReplacingOccurrencesOfString:@"%27" withString:@"'"];
    
    if(name.length > 20){
    if([UIScreen mainScreen].bounds.size.width == 320)
    [self setNaviationButtonWithText:[NSString stringWithFormat:@"Way to %@",name] selector:@selector(backClicked:) isLeft:TRUE isReduceSize:FALSE isMyFont:13.0];
    else
    [self setNaviationButtonWithText:[NSString stringWithFormat:@"Way to %@",name] selector:@selector(backClicked:) isLeft:TRUE isReduceSize:FALSE isMyFont:15.0];
    }
    else
    [self setNaviationButtonWithText:[NSString stringWithFormat:@"Way to %@",name] selector:@selector(backClicked:) isLeft:TRUE isReduceSize:FALSE isMyFont:18.0];

    
    isLocationCount = TRUE;
        
    shopNameLabel.text = _shopName;
        
    thumbImageView.image = _thumbImage;
    mapCOntainerView.frame = CGRectMake(0, 64, self.view.frame.size.width,240);
        
   
    
    [mapCOntainerView addSubview:_mapView];
    
    if([UIScreen mainScreen].bounds.size.height < 569){
    mapLocationsTV.frame = CGRectMake(0, 78, self.view.frame.size.width, 230);
    }
    
    [self moveMapAccrdingToCamera:0];

}

-(void)moveMapAccrdingToCamera:(NSInteger)index{
    
    GiftLocationList *locList = [_giftDetailArray objectAtIndex:index];
    
   // NSLog(@"%@ %@",locList.city,locList.state);
    
    cityName = locList.city;
    stateName = locList.state;

    [_mapView animateToZoom:15];

    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[[locList locationLat] floatValue] longitude:[[locList locationLong] floatValue] zoom:15.0];
    [_mapView animateToCameraPosition:camera];

    double latitude1 = [[locList locationLat] floatValue];
    double longitude1 = [[locList locationLong] floatValue];
    
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude1,longitude1);

    _mapView.myLocationEnabled = TRUE;

    GMSMarker *marker = [[GMSMarker alloc]init];
    marker.position = position;
   
    if(_objGiftType == kSelectedTypeMyGift){
    marker.icon = [UIImage imageNamed:@"gift.png"];
    }
    else
    marker.icon = [UIImage imageNamed:@"gift_locator_map.png"];

    marker.map = _mapView;
    
    marker.snippet = locList.address;
 
}

- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
    CustomInfoWindow *view =  [[[NSBundle mainBundle] loadNibNamed:@"CustomInfoWindow" owner:self options:nil] objectAtIndex:0];
    
    view.frame = CGRectMake(self.view.frame.origin.x + 30, 0, 200, 100);
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 10,view.frame.size.width - 10, 21)];
    titleLabel.text = @"Deal Location"; //marker.title; //postTitle; //@"dfyudfhj";
    titleLabel.textColor = [UIColor whiteColor];
    [view addSubview:titleLabel];
    
    UILabel *addressLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, titleLabel.frame.origin.y + titleLabel.frame.size.height + 5,view.frame.size.width - 10, 60)];
    addressLabel.text = [NSString stringWithFormat:@"%@,\n%@, %@",marker.snippet,cityName,stateName];  //postTitle; //@"dfyudfhj";
    [self setTextOnLabel:addressLabel fontSize:11.0];
    addressLabel.textColor = [UIColor whiteColor];

    [view addSubview:addressLabel];
    
    return view;
}

- (BOOL)mapView:(GMSMapView*)mapView didTapMarker:(GMSMarker*)marker
{
    [mapView setSelectedMarker:marker];
    return YES;
}


#pragma mark - TableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _giftDetailArray.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier1 = @"MapCustomCell";
    objMapCustomCell = (MapCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
    
    if(objMapCustomCell==nil){
    [[NSBundle mainBundle] loadNibNamed:@"MapCustomCell" owner:self options:nil];
    }
    
    GiftDetailMapViewController *vc = self;
    
    GiftLocationList *objLocList = [_giftDetailArray objectAtIndex:indexPath.row];
    
    [objMapCustomCell showGiftLocationOnMap:objLocList];
    
    [objMapCustomCell setOnPhoneButtonCLick:^(NSString *phNumber) {
    [vc showViewForPhoneNumber:phNumber rowIndex:indexPath.row];
    }];
    
    [objMapCustomCell setOnMapButtonCLick:^(NSString *giftLat, NSString *giftLong, NSString *giftAddress) {
    [vc moveToSinglePinOnMapView:giftLat longOfGift:giftLong addressOfGift:giftAddress];
    }];
    
    return objMapCustomCell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 135;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    GiftLocationList *objLocList = [_giftDetailArray objectAtIndex:indexPath.row];
    NSLog(@"#SelectedLatAndLong :%@ %@",objLocList.locationLat,objLocList.locationLong);
    
    [_mapView clear];
    [self moveMapAccrdingToCamera:indexPath.row];
}


-(void)showViewForPhoneNumber:(NSString *)phoneNumber rowIndex:(NSInteger)rowIndex{
    
    NSLog(@"#ThePhoneNumber :%@",phoneNumber);
    
  //  GiftDetailMapViewController *vc = self;
  
    
    coverView =  [self onAddDimViewOnSuperView:self.view];
    
    UITapGestureRecognizer *gesRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTapped:)];
    gesRecognizer.numberOfTapsRequired = 1;
    [coverView addGestureRecognizer:gesRecognizer];

    commonView = [[GiftDetailCommonView alloc]initWithFrame:CGRectMake(10, self.view.frame.origin.y + 150, self.view.frame.size.width , 250)];
    [self.view addSubview:commonView];
    
  //  [coverView addSubview:commonView];

    
    [commonView showCommonViewForGiftDetails:_giftDetailArray fromView:FALSE rowID:rowIndex shopName:@"CALL LIST"];
    
    [commonView setOnCrossClick:^{
    [commonView removeFromSuperview];
    [coverView removeFromSuperview];
    }];


}

-(void)moveToSinglePinOnMapView:(NSString *)latOfGift longOfGift:(NSString *)longOfGift addressOfGift:(NSString *)addressOfGift{
    GiftSingleViewController *giftSingleVC = [self.storyboard instantiateViewControllerWithIdentifier:@"GiftSingleMapVC"];
    giftSingleVC.locationLat = latOfGift;
    giftSingleVC.locationLong = longOfGift;
    giftSingleVC.locationAddress = addressOfGift;
    giftSingleVC.shopName = _shopName;
    
    [self.navigationController pushViewController:giftSingleVC animated:YES];
}

-(void)viewTapped:(UITapGestureRecognizer *)recognzier{
    [commonView removeFromSuperview];
    [coverView removeFromSuperview];
}
#pragma mark - Move Back

-(void)backClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
