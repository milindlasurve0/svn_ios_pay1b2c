//
//  SupportViewController.m
//  Pay1
//
//  Created by webninjaz on 16/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "SupportViewController.h"
#import "MailCreator.h"
#import "Constant.h"
@interface SupportViewController ()

@end

@implementation SupportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self hideNavBackButton];
    [self setNaviationButtonWithText:@"Support" selector:@selector(backClicked:) isLeft:TRUE isReduceSize:TRUE isMyFont:18.0];
    
    if([UIScreen mainScreen].bounds.size.width == 320){
    [self setTextOnLabel:mailLabel fontSize:15.0];
    [self setTextOnLabel:missedNumLabel fontSize:15.0];
    }

}



-(IBAction)callComplaint:(id)sender{
    if([@"02267242266" isEqualToString:@"Not Avilable"]){
    return;
    }
    
    else{
    NSLog(@"The Number is%@",@"02267232266");
    UIDevice *device = [UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"]) {
    NSString *phoneNumber = [NSString stringWithFormat:@"tel://%@",@"02267232266"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }
    }
}


-(IBAction)messageSharingClicked:(id)sender{
    
    mailClient = [[MailCreator alloc]init];
    NSString *string = @"Support";
    NSMutableString *mutableString = [string mutableCopy];
    [mailClient setEmailSubject:mutableString];
    [mailClient setRecipient:@"listen@pay1.in"];
    
    NSString *info = [NSString stringWithFormat:@"\n%@ <br>" "%@ \n",@"Thanks & Regards",[Constant getUserPhoneNumber]];

    
    
    [mailClient setEmailBody:(NSMutableString *)info];
    [mailClient mail:self];

}

-(void)backClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Reveal

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    
    if (position == FrontViewPositionRight) {
    NSLog(@"Done Right");
        
    coverView =  [self onAddDimViewOnSuperView:self.view];
        
    [self.view setUserInteractionEnabled:FALSE];
        
    [self.view addSubview:coverView];
    }
    
    if (position == FrontViewPositionLeft) {
    [self.view setUserInteractionEnabled:TRUE];
        
    [coverView removeFromSuperview];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
