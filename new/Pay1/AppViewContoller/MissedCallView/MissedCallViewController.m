//
//  MissedCallViewController.m
//  Pay1
//
//  Created by Annapurna on 14/07/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "MissedCallViewController.h"
#import "MissedCallCustomCell.h"
#import "DBManager.h"
#import "GetOperatorService.h"
#import "QuickPayResponse.h"
#import "EditQuickPayCriteria.h"
#import "GetOperatorService.h"
@interface MissedCallViewController ()

@end

@implementation MissedCallViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [noMissedCallFound removeFromSuperview];
    
    [self hideNavBackButton];
    
    [self setNaviationButtonWithText:@"BACK" selector:@selector(backClicked:) isLeft:TRUE isReduceSize:TRUE isMyFont:18.0];
    
    UIView *sepView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, missedTV.frame.size.width, 1)];
    sepView.backgroundColor = [UIColor lightGrayColor];
    missedTV.tableHeaderView = sepView;

    
    if([UIScreen mainScreen].bounds.size.width == 320){
    txtLabel.font = [UIFont fontWithName:@"Open Sans" size:13.0];
    missedTV.frame = CGRectMake(0, 187, 375 , self.view.frame.size.height - 187);
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    quickPayNumberArray = [[NSMutableArray alloc]init];
    quickPayNameArray = [[NSMutableArray alloc]init];
    quiclPayAmountArray = [[NSMutableArray alloc]init];
    operatorIDArray = [[NSMutableArray alloc]init];
    quickPayDelegateArray = [[NSMutableArray alloc]init];
    quickPayFlagArray = [[NSMutableArray alloc]init];
    quickPayIDArray = [[NSMutableArray alloc]init];
    quickPaySTVArray = [[NSMutableArray alloc]init];
    quickPayMissedCallArray = [[NSMutableArray alloc]init];
    quickPayOperatorNameArray = [[NSMutableArray alloc]init];
    
  //  NSLog(@"#MissedCallNumberArray :%@",quickPayMissedCallArray);
    
    if([Constant getQuickPayRecord] == TRUE){
    [self callQuickPayService];
    }
    else{
    [self showMissedCallDataFromDB];
    }
}


-(void)callQuickPayService{
    GetOperatorService *objService = [[GetOperatorService alloc]init];
    [objService callQuickPayService:^(NSError *error, QuickPayResponse *objQuickResponse) {
    if(error)return ;
    else{
    [self showMissedCallDataFromDB];
        

    }
    }];
}


-(void)showMissedCallDataFromDB{
    DBManager *  objDBManager = [[DBManager alloc]initWithDatabaseFilename:@"quickPayDB.sql"];
    
    
   //   NSString *query = [NSString stringWithFormat:@"select * from QuickTable"];

    
   NSString *query = [NSString stringWithFormat:@"select * from QuickTable where quickPayFlag=\"%@\"",@"1"];
    
   // NSLog(@"#MissedData :%@",[objDBManager loadDataFromDB:query]);
    
    if([[NSArray alloc] initWithArray:[objDBManager loadDataFromDB:query]].count >0){
        
    NSArray *theArray = [[NSArray alloc] initWithArray:[objDBManager loadDataFromDB:query]];
        
 //  NSLog(@"#MissedData :%@",theArray);
    
    NSInteger indexOfQuickFlag = [objDBManager.arrColumnNames indexOfObject:@"quickPayFlag"];
    for(int i=0;i<theArray.count;i++){
    [quickPayFlagArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfQuickFlag]];
            // NSLog(@"#Check Flag :%@",[[theArray objectAtIndex:i] objectAtIndex:indexOfQuickFlag]);
            
    }
    NSInteger indexOfNameURL = [objDBManager.arrColumnNames indexOfObject:@"quickPayName"];
    for(int i=0;i<theArray.count;i++){
    //  NSLog(@"#Check Name :%@",[[theArray objectAtIndex:i] objectAtIndex:indexOfLogoURL]);
    [quickPayNameArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfNameURL]];
    }
        
    NSInteger indexOfLogoURL = [objDBManager.arrColumnNames indexOfObject:@"quickPayOperatorName"];
    for(int i=0;i<theArray.count;i++){
  //  NSLog(@"#Check Name :%@",[[theArray objectAtIndex:i] objectAtIndex:indexOfLogoURL]);
    [quickPayOperatorNameArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfLogoURL]];
    }
        
    NSInteger indexOfAmountName = [objDBManager.arrColumnNames indexOfObject:@"quickPayAmount"];
    for(int i=0;i<theArray.count;i++){
    [quiclPayAmountArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfAmountName]];
   // NSLog(@"#Check Amount :%@",[[theArray objectAtIndex:i] objectAtIndex:indexOfAmountName]);

    }
        
    NSInteger indexOfNumber = [objDBManager.arrColumnNames indexOfObject:@"quickPayMobileNumber"];
    for(int i=0;i<theArray.count;i++){
    [quickPayNumberArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfNumber]];
   // NSLog(@"#Check Mobile Number :%@",[[theArray objectAtIndex:i] objectAtIndex:indexOfNumber]);

    }
        
    NSInteger indexOfOperatorName = [objDBManager.arrColumnNames indexOfObject:@"quickPayProductID"];
    for(int i=0;i<theArray.count;i++){
    [operatorIDArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOperatorName]];
  //  NSLog(@"#Check Mobile ID :%@",[[theArray objectAtIndex:i] objectAtIndex:indexOfOperatorName]);

    }
        
    NSInteger indexOfOperatorSTV = [objDBManager.arrColumnNames indexOfObject:@"quickPaySTV"];
    for(int i=0;i<theArray.count;i++){
    [quickPaySTVArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOperatorSTV]];
   // NSLog(@"#Check STV :%@",[[theArray objectAtIndex:i] objectAtIndex:indexOfOperatorSTV]);

    }
        
   
        
    NSInteger indexOfQuickPayID = [objDBManager.arrColumnNames indexOfObject:@"quickPayID"];
    for(int i=0;i<theArray.count;i++){
    [quickPayIDArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfQuickPayID]];
  //  NSLog(@"#Check PayID :%@",[[theArray objectAtIndex:i] objectAtIndex:indexOfQuickPayID]);

    }
        
        
    NSInteger indexOfQuickDelegate = [objDBManager.arrColumnNames indexOfObject:@"quickPayDelegate"];
    for(int i=0;i<theArray.count;i++){
    [quickPayDelegateArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfQuickDelegate]];
 //   NSLog(@"#Check PayDeleaget :%@",[[theArray objectAtIndex:i] objectAtIndex:indexOfQuickDelegate]);

    }
        
    NSInteger indexOfMissedCall = [objDBManager.arrColumnNames indexOfObject:@"quickPayMissCallNumber"];
    for(int i=0;i<theArray.count;i++){
    [quickPayMissedCallArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfMissedCall]];
   // NSLog(@"#Check Missed Number :%@",[[theArray objectAtIndex:i] objectAtIndex:indexOfMissedCall]);

   }
        
        
   [missedTV reloadData];
        
    }
    
    else{
    [rechargeMsgView removeFromSuperview];
    [missedTV removeFromSuperview];
        
    [self.view addSubview:noMissedCallFound];

    
    }
    

  // NSLog(@"#Number and Operator :%@ %@ %lu %lu",quickPayMissedCallArray,quickPayDelegateArray,(unsigned long)quickPayMissedCallArray.count,(unsigned long)quickPayDelegateArray.count);
}

#pragma Table View Delegates

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [quickPayNameArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"MissedCallCustomCell";
    
    objMissedCallCell = (MissedCallCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(objMissedCallCell==nil){
        [[NSBundle mainBundle] loadNibNamed:@"MissedCallCustomCell" owner:self options:nil];
    }
    
    
    objMissedCallCell.operatorLogoImageView.image = [UIImage imageNamed:[self giveLogoImageAccordingToOperator:[quickPayOperatorNameArray objectAtIndex:indexPath.row]]];
    
  
    
    objMissedCallCell.phoneNumLabel.text = [quickPayNumberArray objectAtIndex:indexPath.row];
    objMissedCallCell.priceLabel.text = [NSString stringWithFormat:@"Rs. %@",[quiclPayAmountArray objectAtIndex:indexPath.row]];  //[NSString stringWithFormat:@"Rs. %@",[quiclPayAmountArray objectAtIndex:indexPath.row]];
    
    if([[quickPayDelegateArray objectAtIndex:indexPath.row]isEqualToString:@"0"]){
    [objMissedCallCell.cellSwitch setOn:FALSE];
    objMissedCallCell.missedNumLabel.hidden = TRUE;
    }
    else{
    objMissedCallCell.missedNumLabel.hidden = FALSE;

    objMissedCallCell.missedNumLabel.text  = [NSString stringWithFormat:@"Missed call: %@",[quickPayMissedCallArray objectAtIndex:indexPath.row]];
    [objMissedCallCell.cellSwitch setOn:TRUE];
    }
    
    [objMissedCallCell.cellSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
    objMissedCallCell.cellSwitch.tag = indexPath.row;
    
    return objMissedCallCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}

-(void)changeSwitch:(UISwitch *)sender{
    NSLog(@"#UpdateSwitchIndex :%ld",(long)sender.tag);
    
    indexRow = sender.tag;
    
    [self callEditQuickPayService];
}
-(void)callEditQuickPayService{
    [self showHUDOnView];
    
    GetOperatorService *service = [[GetOperatorService alloc]init];
    [service callEditQuickPayService:self.criteria callback:^(NSError *error, QuickPayResponse *objEditQuickResponse) {
    if(error)return ;
    else{
    [self hideHUDOnView];
    if([objEditQuickResponse.missedNumber isEqual:[NSNull null]]){
    [self updateTheDB:@"0" missCallNum:objEditQuickResponse.missedNumber];
    [self showAlertView:@"Missed call recharge service deactivated."]; 
    }
    else{
    NSLog(@"#QuickPayisSuccessfullyUpdated");
        
    if([objEditQuickResponse.status isEqualToString:@"failure"]){
    [self showAlertView:[NSString stringWithFormat:@"No Missed Call Number Found."]];
        [self viewWillAppear:TRUE];

    }
    else{
    [self showAlertView:[NSString stringWithFormat:@"Missed call recharge service\nactivated\nTo recharge this number give missed call to %@",objEditQuickResponse.missedNumber]];
   [self updateTheDB:@"1" missCallNum:objEditQuickResponse.missedNumber];
    }

    }
    }
    }];
}

-(EditQuickPayCriteria *)criteria{
    EditQuickPayCriteria *objCriteria = [[EditQuickPayCriteria alloc]init];
    objCriteria.mobileNumber = [quickPayNumberArray objectAtIndex:indexRow];
    objCriteria.name = [quickPayNameArray objectAtIndex:indexRow];
    objCriteria.amount = [quiclPayAmountArray objectAtIndex:indexRow];
    objCriteria.operatorID = [operatorIDArray objectAtIndex:indexRow];
    NSLog(@"#Delegate Value :%@",[quickPayDelegateArray objectAtIndex:indexRow]);
    
    if([[quickPayDelegateArray objectAtIndex:indexRow] isEqualToString:@"0"]){
    objCriteria.delagteFlag = @"1";//[quickPayDelegateArray objectAtIndex:indexRow];
    }
    else{
    objCriteria.delagteFlag = @"0";
    }
    objCriteria.flag = [quickPayFlagArray objectAtIndex:indexRow];
    objCriteria.quickID = [quickPayIDArray objectAtIndex:indexRow];
    objCriteria.quickSTV = [quickPaySTVArray objectAtIndex:indexRow];
    objCriteria.confirmation = @"";
    return objCriteria;
    
}

-(void)updateTheDB:(NSString *)statusValue missCallNum:(NSString *)missCallNum{
    
    DBManager *  objDBManager = [[DBManager alloc]initWithDatabaseFilename:@"quickPayDB.sql"];

   // NSString *query = [NSString stringWithFormat:@"UPDATE QuickTable SET quickPayDelegate=\"%d\" WHERE quickPayID=\"%@\"",[statusValue intValue],[quickPayIDArray objectAtIndex:indexRow]];
    
     NSString *query = [NSString stringWithFormat:@"UPDATE QuickTable SET quickPayDelegate=\"%d\" AND quickPayMissCallNumber=\"%@\"  WHERE quickPayID=\"%@\"",[statusValue intValue],missCallNum,[quickPayIDArray objectAtIndex:indexRow]];
    
    NSLog(@"#QueryLikeUnLike :%@",query);
    [objDBManager executeQuery:query];
    
    if (objDBManager.affectedRows != 0) {
        NSLog(@"Query was executed successfully. Affected rows = %d", objDBManager.affectedRows);
        //[delegate editingInfoWasFinished];
    }
    else{
        NSLog(@"Could not execute the query.");
    }
    
    [self viewWillAppear:TRUE];
    
   
}

-(void)showAlertView:(NSString *)alertMessage{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"MISSED CALL RECHARGE" message:alertMessage delegate:self cancelButtonTitle:@"DONE" otherButtonTitles:nil, nil];
    [alert show];
}


-(void)backClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
