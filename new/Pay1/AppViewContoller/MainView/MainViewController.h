//
//  MainViewController.h
//  Pay1
//
//  Created by Annapurna on 04/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "BaseViewController.h"
#import "EnumType.h"
#import <AddressBookUI/AddressBookUI.h>
#import <AddressBook/AddressBook.h>
#import "OperatorList.h"
#import "EnumType.h"

@class DBManager;
@class TPKeyboardAvoidingScrollView;
@class SWRevealViewController;
@class QuickCustomCell;
typedef void (^onOperatorSelectBlockClick) (NSString *operatorID, NSString *operatorName);

@interface MainViewController : BaseViewController<UIScrollViewDelegate,UITextFieldDelegate,ABPeoplePickerNavigationControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate>{
    
    IBOutlet UIView *underLineView;
    IBOutlet UIButton *quickRechargeBtn;
    IBOutlet UIButton *mobileBtn;
    IBOutlet UIButton *dthBtn;
    IBOutlet UIButton *dataCardBtn;
    
    IBOutlet UIButton *payButtonMobile;
    
    IBOutlet UITextField *mobileNumTxtField;
    IBOutlet UITextField *operatorTxtField;
    IBOutlet UITextField *amountTxtField;
    
    IBOutlet UIView *mobileSepView;
    IBOutlet UIView *operatorSepView;
    IBOutlet UIView *amountSepView;
    
    IBOutlet UIView *quickRechargeView;
    IBOutlet UIView *mobileView;
    IBOutlet UIView *dthView;
    IBOutlet UIView *dataCardView;
    
    IBOutlet UIScrollView *pageScrollView;
    IBOutlet UIPageControl *pageControl;
    
  //  IBOutlet QuickCustomCell *objQuickCustomCell;
   
    
    
    IBOutlet UITextField *subscriberNumTxtFieldDTH;
    IBOutlet UITextField *amountTxtFieldDTH;
    IBOutlet UITextField *operatorTxtFieldDTH;
    
    
    IBOutlet UIView *subscriberSepViewDTH;
    IBOutlet UIView *amountSepViewDTH;
    IBOutlet UIView *operatorSepViewDTH;
    
    
    
    IBOutlet UITextField *mobileNumTxtFieldDC;
    IBOutlet UITextField *operatorTxtFieldDC;
    IBOutlet UITextField *amountTxtFieldDC;
    
    IBOutlet UIView *mobileSepViewDC;
    IBOutlet UIView *operatorSepViewDC;
    IBOutlet UIView *amountSepViewDC;
    
    IBOutlet UIButton *payButtonDTH;
    IBOutlet UIButton *payButtonDC;
    
    
    IBOutlet UIButton *locateShopButton;
    IBOutlet UIButton *payViaDTH;
    IBOutlet UIButton *payViaDC;
    
    
    IBOutlet UIView *optionView;
    
    
    
    
    IBOutlet UIButton *prepaidButton;
    IBOutlet UILabel *prepaidLabel;
    
    IBOutlet UIButton *postPaidButton;
    IBOutlet UILabel *postPaidLabel;
    
    
    ServiceType objServiceType;
    
    
    IBOutlet UIButton *contactsButton;
    IBOutlet UIButton *rightArrowButton;
    
    IBOutlet UIButton *rightArrowButtonDTH;
    IBOutlet UIButton *rightArrowButtonDC;
    
    
    UIView *coverView;
    IBOutlet UIScrollView *headerScrollView;
    
    
    NSString *selectedOperatorSTV;
    
    BOOL isNumberEntered;
    BOOL isMobileSwitchClicked;
    
    IBOutlet UICollectionView *quickCV;
    
    
    IBOutlet UIImageView *noQuickPayImageView;
    IBOutlet UILabel *noQuickPayLabel;
    
    NSMutableArray *quickPayLogoArray;
    NSMutableArray *quickPayNumArray;
    NSMutableArray *quickPayAmountArray;
    NSMutableArray *quickPayOperatorName;
    NSMutableArray *quickPaySTVArray;
    NSMutableArray *quickPayFlagArray;
    NSMutableArray *quickPayOperatorCode;
    NSMutableArray *quickPayDateArray;
    
    
    IBOutlet UIButton *mobileSwitchButton;
    IBOutlet UIButton *dthSwitchButton;
    IBOutlet UIButton *dataCardSwitchButton;
    
    
    IBOutlet UILabel *typeLabel;

    BOOL isSTVFound;
    
    BOOL isQuickPayFound;
    
    int scrollContentOffset;
    
    
    IBOutlet UIButton *checkPlanButtonMobile;
    IBOutlet UIButton *checkPlanButtonDataCard;
    IBOutlet UIButton *checkPlanButtonDTH;

    BOOL isRechargeDone;
    BOOL isFromWallet;
    
    UIView *myView;
    
    IBOutlet UIImageView *dotImageView;
    IBOutlet UITextField *errorTxtField;
    
    IBOutlet UIImageView *dotImageViewDTH;
    IBOutlet UITextField *errorTxtFieldDTH;
    
    IBOutlet UIImageView *dotImageViewDC;
    IBOutlet UITextField *errorTxtFieldDC;
    
    
    NSString *quickOperatorName,*mobileOperatorName,*dataCardOperatorName;
    NSString *quickOperatorID,*mobileOperatorID,*dataCardOperatorID,*dthOperatorID;
    NSString *mobileMinAmount,*dthMinAmount,*dataCardMinAmount;
    NSString *mobileMaxAmount,*dthMaxAmount,*dataCardMaxAmount;
    NSString *quickSTV,*mobileSTV;//,*dataCardSTV,*dthSTV;
    NSString *quickAmount,*mobileAmount,*dthAmount,*dataCardAmount;
    NSString *quickFlag;
    NSString *quickMobileNum,*mobileNum;
    NSString *mobilePlanOPID,*dataCardPlanOPID,*dthPlanOPID;
    NSString *mobileCircleID,*dataCardCircleID;
    NSString *mobileCircleName,*dataCardCircleName;


    NSString *dthOperatorLength, *dthOperatorPrefix;
    
    NSString *serviceChargeSlab,*serviceChargeAmount,*serviceTaxPercent;


  //  BOOL isDTHError;
    
    int maxOperatorLenght;
    
    IBOutlet UITextView *amountErrorMobTV,*amountErrorDTHTV,*amountErrorDCTV;
    
    
    UIButton *checkButton;
    
    NSString *amountForRecharge;
    NSString *numberForRecharge;
    NSString *operatorNameForRecharge;
    NSString *flagForRecharge;
    
    
    IBOutlet UIView *errorBlackViewMobile;
    IBOutlet UIView *errorBlackViewDTH;
    IBOutlet UIView *errorBlackViewDC;
    
    IBOutlet UILabel *errorLabelMobile;
    IBOutlet UILabel *errorLabelDTH;
    IBOutlet UILabel *errorLabelDC;
    
    
    CGPoint pointNow;
    
    BOOL isMovingDirection;
    
    NSString *stvValue;
    
    IBOutlet UILabel *postPaidInfoLabel;
    
    
    NSString *amountForPostPaidRecharge;
    NSString *quickPayBaseAmount;
    
    
    
}
@property (nonatomic, strong) IBOutlet TPKeyboardAvoidingScrollView *scrollViewMobile;
@property (nonatomic, strong) IBOutlet TPKeyboardAvoidingScrollView *scrollViewDTH;
@property (nonatomic, strong) IBOutlet TPKeyboardAvoidingScrollView *scrollViewDC;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (nonatomic, strong) ABPeoplePickerNavigationController *addressBookController;

@property (nonatomic, copy) onOperatorSelectBlockClick  onOperatorSelectBlockClick;
@property (nonatomic, assign) ServiceType operatorType;

@end
