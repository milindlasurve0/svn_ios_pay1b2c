//
//  StoreLocationViewController.m
//  Pay1
//
//  Created by Annapurna on 11/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "StoreLocationViewController.h"
#import "StoreLocationService.h"
#import "StoreLocationCriteria.h"
#import "StoreLocationResponse.h"
#import "CurrentLocation.h"
#import "StoreList.h"
#import "CustomInfoWindow.h"
#import "ShopDetailViewController.h"
#import "StoreAlertView.h"
#import "UIImage+Utility.h"
@interface StoreLocationViewController ()

@end

@implementation StoreLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self hideNavBackButton];
    [self setNaviationButtonWithText:@"Locate cash topup store" selector:@selector(backClicked:) isLeft:TRUE isReduceSize:FALSE isMyFont: 18.0];
    
    if(![CLLocationManager locationServicesEnabled] || [CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
        
        NSLog(@"Location Services Enabled");
        
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            UIAlertView *   alert = [[UIAlertView alloc] initWithTitle:@"App Permission Denied"
                                                               message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                                              delegate:nil
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
            
            alert.delegate = self;
            [alert show];
        }
        
        return;
    }
    
    else{
    if(![Constant getCheckButtonClick]){
    coverView =  [self onAddDimViewOnSuperView:self.view];
    objStoreAlertView  = [[StoreAlertView alloc]initWithFrame:CGRectMake(20, self.view.frame.origin.y + 150, [UIScreen mainScreen].bounds.size.width - 40 , 180)];

    [coverView addSubview:objStoreAlertView];
  
    
    [objStoreAlertView setOnCheckClick:^{
        [Constant saveCheckButtonClick:TRUE];
    }];
    
    [objStoreAlertView setOnDoneClick:^{
        [coverView removeFromSuperview];
    }];
    }
    
    
    
    
    
    _mapView.delegate = self;
    
    _mapView.settings.myLocationButton = YES;
    _mapView.myLocationEnabled = TRUE;
    _mapView.settings.consumesGesturesInView = NO;
    
    CLLocation *location = [CurrentLocation sharedLocation].currentLocation;
    
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude  longitude:location.coordinate.longitude  zoom:14.0];
        
     //   GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:19.1861  longitude:72.8486  zoom:14.0];

    
    [_mapView animateToCameraPosition:camera];
    
    
    CGPoint point = _mapView.center;
    CLLocationCoordinate2D coor = [_mapView.projection coordinateForPoint:point];
    
    NSLog(@"The Point center:%f %f",coor.latitude,coor.longitude);
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
    [_mapView addGestureRecognizer:panRecognizer];
    
    }
    
//    [self callStoreLocationService];

    
    
   // [self locationOnMapView];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: UIApplicationOpenSettingsURLString]];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

-(void)pan:(UIPanGestureRecognizer*) gestureRecognizer
{
    
    
   // NSLog(@"Swiped");
    if(gestureRecognizer.state == UIGestureRecognizerStateEnded)
    {
        
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithRegion:_mapView.projection.visibleRegion];
        
        CLLocationCoordinate2D northEast = bounds.northEast;
        
        CLLocation* mapCornerLocation = [[CLLocation alloc] initWithLatitude:northEast.latitude longitude:northEast.longitude];

        
        CGPoint point = _mapView.center;
        CLLocationCoordinate2D coor = [_mapView.projection coordinateForPoint:point];

        CLLocation* mapCenterLocation = [[CLLocation alloc] initWithLatitude:coor.latitude longitude:coor.longitude];
        
        distanceX = [mapCornerLocation distanceFromLocation:mapCenterLocation] / 1000;


       
        if (distanceX>= distanceY/2)
        {
            //move your map view and do whatever you want...
            
            myDoubleNumber = [NSNumber numberWithDouble:distanceX];
            
            [self callStoreLocationService];
        }
    }
}


-(void)viewDidAppear:(BOOL)animated{
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithRegion:_mapView.projection.visibleRegion];
    
    CLLocationCoordinate2D northEast = bounds.northEast;
    CLLocationCoordinate2D northWest = CLLocationCoordinate2DMake(bounds.northEast.latitude, bounds.southWest.longitude);
    CLLocationCoordinate2D southEast = CLLocationCoordinate2DMake(bounds.southWest.latitude, bounds.northEast.longitude);
    CLLocationCoordinate2D southWest = bounds.southWest;
    
    
    
    NSLog(@"The Point northEast:%f %f",northEast.latitude,northEast.longitude);
    NSLog(@"The Point northWest:%f %f",northWest.latitude,northWest.longitude);
    NSLog(@"The Point southEast:%f %f",southEast.latitude,southEast.longitude);
    NSLog(@"The Point southWest:%f %f",southWest.latitude,southWest.longitude);
    
    CGPoint point = _mapView.center;
    CLLocationCoordinate2D coor = [_mapView.projection coordinateForPoint:point];
    
    NSLog(@"The Point center:%f %f",coor.latitude,coor.longitude);
    
    
    CLLocation* mapCornerLocation = [[CLLocation alloc] initWithLatitude:northEast.latitude longitude:northEast.longitude];
    
   // CLLocation* mapCenterLocation = [[CLLocation alloc] initWithLatitude:[_mapView.camera target].latitude longitude:[_mapView.camera target].longitude];
    
    CLLocation* mapCenterLocation = [[CLLocation alloc] initWithLatitude:coor.latitude longitude:coor.longitude];
    
    initialX = coor.latitude;
    initialY = coor.longitude;


    distanceY = [mapCornerLocation distanceFromLocation:mapCenterLocation] / 1000;
    
    NSLog(@"The distanceY center:%f",distanceY);
    
    myDoubleNumber = [NSNumber numberWithDouble:distanceY];
    
     [self callStoreLocationService];
 
}

-(void)callStoreLocationService{
    [self setNaviationButtonWithActivityIndicator ];
    
    StoreLocationService *objService = [[StoreLocationService alloc]init];
    [objService callStoreLocationService:self.storeCriteria callback:^(NSError *error, StoreLocationResponse *objStoreResponse) {
    if(error)return ;
    else{
    [activityIndicator setHidden:TRUE];
    distanceX = 0.0;
   // NSLog(@"StoreFound :%@",objStoreResponse.storeLocationArray);
        
        
    storeDataArray = objStoreResponse.storeLocationArray;
    shopDataArray = objStoreResponse.storeLocationArray;
    [self plotShopOnMap:objStoreResponse.storeLocationArray];
    }
    }];
}

-(StoreLocationCriteria *)storeCriteria{
    StoreLocationCriteria *objCriteria = [[StoreLocationCriteria alloc]init];
    CLLocation *location = [CurrentLocation sharedLocation].currentLocation;
    objCriteria.userLat = [NSString stringWithFormat:@"%f",location.coordinate.latitude]; //[NSString stringWithFormat:@"%f",19.1861]; //[NSString stringWithFormat:@"%f",location.coordinate.latitude];
    objCriteria.userLong = [NSString stringWithFormat:@"%f",location.coordinate.longitude]; //[NSString stringWithFormat:@"%f",72.8486];
    objCriteria.userMobNum = [Constant getUserPhoneNumber];
   
    

    objCriteria.distance = [myDoubleNumber stringValue]; //[NSString stringWithFormat:@"%d",userDistance];
    return objCriteria;
}

-(void)plotShopOnMap:(NSMutableArray *)shopArray{
    [_mapView clear];
    for(int i=0;i<shopArray.count;i++){
    StoreList *objStoreList = [shopArray objectAtIndex:i];
    double latitude1 = [[objStoreList storeLat] floatValue];
    double longitude1 = [[objStoreList storeLong] floatValue];
        
        
        NSLog(@"#ShopLatandLong :%f %f",latitude1,longitude1);
        
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude1,longitude1);
        
        
    GMSMarker *marker = [[GMSMarker alloc]init];

        marker.position = position;
        
       
    marker.icon = [UIImage imageNamed:@"pay1_store.png"];
    if(objStoreList.storeShopName == NULL)
    {
        NSLog(@"NULL");
    }
        else
    marker.title = objStoreList.storeShopName;
        
        
        marker.snippet = [NSString stringWithFormat:@"%@,%@,%@,%@-%@",objStoreList.storeAddress,objStoreList.storeAreaName,objStoreList.storeCityName,objStoreList.storeStateName,objStoreList.storePinCode];
    marker.map = _mapView;
    
    }
}


- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
    CustomInfoWindow *view =  [[[NSBundle mainBundle] loadNibNamed:@"CustomInfoWindow" owner:self options:nil] objectAtIndex:0];
    
    
    for(int i=0;i<shopDataArray.count;i++){
    StoreList *objStoreList = [shopDataArray objectAtIndex:i];
        
        NSLog(@"ShopName and url :%@ %@",objStoreList.storeShopName,objStoreList.storeImageURL);
        
        if([objStoreList.storeShopName isEqual:[NSNull null]]){
            view.frame = CGRectMake(10, 0,  230, 120);
            
            
            UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 10,view.frame.size.width - 10, 21)];
            
            
            
            if(![marker.title isEqual:[NSNull null]])
                titleLabel.text = marker.title;
            
            // CGSize yourLabelSize1 = [titleLabel.text sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:17.0]}];
            
            
            titleLabel.textColor = [UIColor whiteColor];
            [view addSubview:titleLabel];
            
            
            UILabel *addressLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, titleLabel.frame.origin.y + titleLabel.frame.size.height + 5,view.frame.size.width - 10, 60)];
            addressLabel.text = marker.snippet; //postTitle; //@"dfyudfhj";
            
            
            
            CGSize yourLabelSize = [addressLabel.text sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:17.0]}];
            
            CGRect newFrame = addressLabel.frame;
            
            // newFrame.size.width = yourLabelSize.width;(To increase width according to the text size)
            newFrame.size.height = yourLabelSize.height;
            addressLabel.frame = newFrame;
            
            addressLabel.font = [UIFont fontWithName:@"Open Sans" size:13.0];
            addressLabel.textColor = [UIColor lightGrayColor];
            addressLabel.numberOfLines = 0;
            [addressLabel sizeToFit];
            
            
            newFrame = view.frame;
            newFrame.origin.y = 0;
            newFrame.size.height = addressLabel.frame.origin.y + addressLabel.frame.size.height + 40; //[YourChatCell BalloonViewMarginHeight];
            newFrame.size.width = addressLabel.frame.origin.x + addressLabel.frame.size.width + 80;
            view.frame = newFrame;
            
            
            
            [view addSubview:addressLabel];
        }

        
        else{
            
       if ([objStoreList.storeShopName isEqualToString:marker.title]){
           
           
           
                if([objStoreList.storeImageURL isEqual:[NSNull null]]){
                    
                    view.frame = CGRectMake(10, 0,  230, 120);
                    
                    
                    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 10,view.frame.size.width - 10, 21)];
                    
                    
                    
                    if(![marker.title isEqual:[NSNull null]])
                        titleLabel.text = marker.title;
                    
                   /*  CGSize yourLabelSize1 = [titleLabel.text sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:17.0]}];
                    
                    CGRect newFrame1 = titleLabel.frame;
                    
                    // newFrame.size.width = yourLabelSize.width;(To increase width according to the text size)
                    newFrame1.size.width = yourLabelSize1.width;
                    titleLabel.frame = newFrame1;*/
                    
                    
                    titleLabel.textColor = [UIColor whiteColor];
                    [view addSubview:titleLabel];
                    
                    
                    UILabel *addressLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, titleLabel.frame.origin.y + titleLabel.frame.size.height + 5,view.frame.size.width - 10, 60)];
                    addressLabel.text = marker.snippet; //postTitle; //@"dfyudfhj";
                    
                    
                    
                    CGSize yourLabelSize = [addressLabel.text sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:17.0]}];
                    
                    CGRect newFrame = addressLabel.frame;
                    
                    // newFrame.size.width = yourLabelSize.width;(To increase width according to the text size)
                    newFrame.size.height = yourLabelSize.height;
                    addressLabel.frame = newFrame;
                    
                    addressLabel.font = [UIFont fontWithName:@"Open Sans" size:13.0];
                    addressLabel.textColor = [UIColor lightGrayColor];
                    addressLabel.numberOfLines = 0;
                    [addressLabel sizeToFit];
                    
                    
                    newFrame = view.frame;
                    newFrame.origin.y = 0;
                    newFrame.size.height = addressLabel.frame.origin.y + addressLabel.frame.size.height + 40; //[YourChatCell BalloonViewMarginHeight];
                    newFrame.size.width = addressLabel.frame.origin.x + addressLabel.frame.size.width + 80;
                    view.frame = newFrame;
                    
                    
                    
                    [view addSubview:addressLabel];
                }
            
            
            else{
                
                view.frame = CGRectMake(10, 0,  [UIScreen mainScreen].bounds.size.width - 10, 120);
                
                
                UIImageView *shopImgView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 80, 60)];
                
                [shopImgView setImageWithURL:[NSURL URLWithString:objStoreList.storeImageURL] placeholderImage:[UIImage imageNamed:@"deal_default.png"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                    UIImage *newImage = [UIImage imageWithImage:image  scaledToSize:CGSizeMake(320,212)];
                    shopImgView.image = newImage;
                }usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)UIActivityIndicatorViewStyleGray];
                
                [view addSubview:shopImgView];
                
                
                UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, 10,view.frame.size.width - 10, 21)];
                
                if(![marker.title isEqual:[NSNull null]])
                    titleLabel.text = marker.title;
                
                
                
                titleLabel.textColor = [UIColor whiteColor];
                [view addSubview:titleLabel];
                
                
                UILabel *addressLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, titleLabel.frame.origin.y + titleLabel.frame.size.height + 5,view.frame.size.width - 110, 60)];
                addressLabel.text = marker.snippet; //postTitle; //@"dfyudfhj";
                
                CGSize yourLabelSize = [addressLabel.text sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:17.0]}];
                
                CGRect newFrame = addressLabel.frame;
                
                // newFrame.size.width = yourLabelSize.width;(To increase width according to the text size)
                newFrame.size.height = yourLabelSize.height;
                addressLabel.frame = newFrame;
                
                addressLabel.font = [UIFont fontWithName:@"Open Sans" size:13.0];
                addressLabel.textColor = [UIColor lightGrayColor];
                addressLabel.numberOfLines = 0;
                [addressLabel sizeToFit];
                
                
                newFrame = view.frame;
                newFrame.origin.y = 0;
                newFrame.size.height = addressLabel.frame.origin.y + addressLabel.frame.size.height + 40; //[YourChatCell BalloonViewMarginHeight];
                
                view.frame = newFrame;
                
                
                //[self setTextOnLabel:addressLabel fontSize:12.0];
                
                [view addSubview:addressLabel];
                
            }
        }
        }
        
        
    }
    
    
    return view;
}





/*
- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
    CustomInfoWindow *view =  [[[NSBundle mainBundle] loadNibNamed:@"CustomInfoWindow" owner:self options:nil] objectAtIndex:0];
    
    
    for(int i=0;i<shopDataArray.count;i++){
    StoreList *objStoreList = [shopDataArray objectAtIndex:i];
        
        if([marker.title isEqual:[NSNull null]]){
            view.frame = CGRectMake(10, 0,  230, 120);
            
            
            UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 10,view.frame.size.width - 10, 21)];
            
            
            
            if(![marker.title isEqual:[NSNull null]])
                titleLabel.text = marker.title;
            
            // CGSize yourLabelSize1 = [titleLabel.text sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:17.0]}];
            
            
            titleLabel.textColor = [UIColor whiteColor];
            [view addSubview:titleLabel];
            
            
            UILabel *addressLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, titleLabel.frame.origin.y + titleLabel.frame.size.height + 5,view.frame.size.width - 10, 60)];
            addressLabel.text = marker.snippet; //postTitle; //@"dfyudfhj";
            
            
            
            CGSize yourLabelSize = [addressLabel.text sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:17.0]}];
            
            CGRect newFrame = addressLabel.frame;
            
            // newFrame.size.width = yourLabelSize.width;(To increase width according to the text size)
            newFrame.size.height = yourLabelSize.height;
            addressLabel.frame = newFrame;
            
            addressLabel.font = [UIFont fontWithName:@"Open Sans" size:13.0];
            addressLabel.textColor = [UIColor lightGrayColor];
            addressLabel.numberOfLines = 0;
            [addressLabel sizeToFit];
            
            
            newFrame = view.frame;
            newFrame.origin.y = 0;
            newFrame.size.height = addressLabel.frame.origin.y + addressLabel.frame.size.height + 40; //[YourChatCell BalloonViewMarginHeight];
            newFrame.size.width = addressLabel.frame.origin.x + addressLabel.frame.size.width + 40;
            view.frame = newFrame;
            
            
            
            [view addSubview:addressLabel];
        }
        else{
        if([objStoreList.storeShopName isEqualToString:marker.title]){
            
            
            NSLog(@"ShopName and url :%@ %@",objStoreList.storeShopName,objStoreList.storeImageURL);
            
            if([objStoreList.storeImageURL isEqual:[NSNull null]]){
                
                view.frame = CGRectMake(10, 0,  230, 120);

        
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 10,view.frame.size.width - 10, 21)];
                
              
    
    if(![marker.title isEqual:[NSNull null]])
        titleLabel.text = marker.title;
    
               // CGSize yourLabelSize1 = [titleLabel.text sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:17.0]}];
                
    
    titleLabel.textColor = [UIColor whiteColor];
    [view addSubview:titleLabel];
    
    
        UILabel *addressLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, titleLabel.frame.origin.y + titleLabel.frame.size.height + 5,view.frame.size.width - 10, 60)];
        addressLabel.text = marker.snippet; //postTitle; //@"dfyudfhj";
                
                
                
                CGSize yourLabelSize = [addressLabel.text sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:17.0]}];
                
                CGRect newFrame = addressLabel.frame;
                
                // newFrame.size.width = yourLabelSize.width;(To increase width according to the text size)
                newFrame.size.height = yourLabelSize.height;
                addressLabel.frame = newFrame;
                
                addressLabel.font = [UIFont fontWithName:@"Open Sans" size:13.0];
                addressLabel.textColor = [UIColor lightGrayColor];
                addressLabel.numberOfLines = 0;
                [addressLabel sizeToFit];
                
                
                newFrame = view.frame;
                newFrame.origin.y = 0;
                newFrame.size.height = addressLabel.frame.origin.y + addressLabel.frame.size.height + 40; //[YourChatCell BalloonViewMarginHeight];
                newFrame.size.width = addressLabel.frame.origin.x + addressLabel.frame.size.width + 40;
                view.frame = newFrame;
                
                
                
        [view addSubview:addressLabel];
            }
            }
            
            else{
                
                view.frame = CGRectMake(10, 0,  [UIScreen mainScreen].bounds.size.width - 10, 120);

                
                UIImageView *shopImgView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 80, 60)];
                
                [shopImgView setImageWithURL:[NSURL URLWithString:objStoreList.storeImageURL] placeholderImage:[UIImage imageNamed:@"deal_default.png"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                    UIImage *newImage = [UIImage imageWithImage:image  scaledToSize:CGSizeMake(320,212)];
                    shopImgView.image = newImage;
                }usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)UIActivityIndicatorViewStyleGray];
                
                [view addSubview:shopImgView];
                
                
                UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, 10,view.frame.size.width - 10, 21)];
                
                if(![marker.title isEqual:[NSNull null]])
                    titleLabel.text = marker.title;
                
                
                
                titleLabel.textColor = [UIColor whiteColor];
                [view addSubview:titleLabel];
                
                
                UILabel *addressLabel = [[UILabel alloc]initWithFrame:CGRectMake(100, titleLabel.frame.origin.y + titleLabel.frame.size.height + 5,view.frame.size.width - 110, 60)];
                addressLabel.text = marker.snippet; //postTitle; //@"dfyudfhj";
                
                CGSize yourLabelSize = [addressLabel.text sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:17.0]}];
                
                CGRect newFrame = addressLabel.frame;
                
                // newFrame.size.width = yourLabelSize.width;(To increase width according to the text size)
                newFrame.size.height = yourLabelSize.height;
                addressLabel.frame = newFrame;
                
                addressLabel.font = [UIFont fontWithName:@"Open Sans" size:13.0];
                addressLabel.textColor = [UIColor lightGrayColor];
                addressLabel.numberOfLines = 0;
                [addressLabel sizeToFit];
                
                
                newFrame = view.frame;
                newFrame.origin.y = 0;
                newFrame.size.height = addressLabel.frame.origin.y + addressLabel.frame.size.height + 40; //[YourChatCell BalloonViewMarginHeight];
                
                view.frame = newFrame;

                
                //[self setTextOnLabel:addressLabel fontSize:12.0];
                
                [view addSubview:addressLabel];

            }
        }
        
        
    }
    
    
    return view;
}
*/
-(void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker{
    NSLog(@"TappedMarker %@",marker.title);
    
    for(int i=0;i<storeDataArray.count;i++){
        StoreList *storeListObj = [storeDataArray objectAtIndex:i];

        if([marker.title isEqualToString:storeListObj.storeShopName]){
            ShopDetailViewController *shopDVC = [self.storyboard instantiateViewControllerWithIdentifier:@"shopDetailVC"];
            
            shopDVC.objStoreList = storeListObj;
            
            [self.navigationController pushViewController:shopDVC animated:YES];
            
            break;
        }
    
        }
    
}

/*

-(void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position{
    
    
    
    NSLog(@"The distanceX center:%f %f",distanceX,distanceY);

    
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithRegion:_mapView.projection.visibleRegion];

    CLLocationCoordinate2D northEast = bounds.northEast;
    
    CGPoint point = _mapView.center;
    CLLocationCoordinate2D coor = [_mapView.projection coordinateForPoint:point];
    
    
    
    CLLocation* mapCornerLocation = [[CLLocation alloc] initWithLatitude:northEast.latitude longitude:northEast.longitude];
    
    CLLocation* mapCenterLocation = [[CLLocation alloc] initWithLatitude:coor.latitude longitude:coor.longitude];
    
       NSLog(@"The Point center:%f %f %f %f",mapCornerLocation.coordinate.latitude,mapCornerLocation.coordinate.longitude,mapCenterLocation.coordinate.latitude,mapCenterLocation.coordinate.longitude);
    
    distanceY = [mapCornerLocation distanceFromLocation:mapCenterLocation] / 1000;
    
    if(distanceY > 0.0){
        hasLaunchedFirstTime = TRUE;
        
        initialX = coor.latitude;
        initialY = coor.longitude;
        
        CLLocation* mapCenterXLocation = [[CLLocation alloc] initWithLatitude:initialX longitude:initialY];

       
        
        distanceX = [mapCornerLocation distanceFromLocation:mapCenterXLocation] / 1000;

     }

    
    

    
    

    
    


    
    distanceX = [mapCornerLocation distanceFromLocation:mapCenterLocation] / 1000;
    
    NSLog(@"The distanceX center:%f %f",distanceX,distanceY);
    
    myDoubleNumber = [NSNumber numberWithDouble:distanceX];
    
   // if(distanceX > distanceY/2){
    if(distanceX > distanceY){

        myDoubleNumber = 0;
        distanceY = distanceX;
      //  distanceX = 0;
        myDoubleNumber = [NSNumber numberWithDouble:distanceX];;
        [self callStoreLocationService];
        
        
        distanceX = 0;
        
    }
}*/

/*
-(void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position{
    CLLocation *location = [CurrentLocation sharedLocation].currentLocation;

    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude  longitude:location.coordinate.longitude  zoom:15.0];
    
    
    
    CLLocation *location1 = [[CLLocation alloc] initWithLatitude:0.0 longitude:0.0];
    CLLocation *location2 = [[CLLocation alloc] initWithLatitude:camera.target.latitude longitude:camera.target.longitude];
    NSLog(@"Distance i meters: %f", [location1 distanceFromLocation:location2]);
    
    
    CGPoint nePoint = CGPointMake(_mapView.bounds.origin.x + _mapView.bounds.size.width, _mapView.bounds.origin.y);
    CGPoint swPoint = CGPointMake((_mapView.bounds.origin.x), (_mapView.bounds.origin.y + _mapView.bounds.size.height));
    
    //Then transform those point into lat,lng values
    CLLocationCoordinate2D neCoord;
    neCoord = [_mapView.projection coordinateForPoint:nePoint];
    //   neCoord = [_mapView convertPoint:nePoint toCoordinateFromView:_mapView];
    
    CLLocationCoordinate2D swCoord;
    swCoord = [_mapView.projection coordinateForPoint:swPoint]; //[_mapView convertPoint:swPoint toCoordinateFromView:_mapView];
    
    
     NSLog(@"The Point :%f %f %f %f",_mapView.camera.target.latitude,_mapView.camera.target.longitude,neCoord.latitude,neCoord.longitude);
    
    CLLocation* mapLocation = [[CLLocation alloc] initWithLatitude:neCoord.latitude longitude:neCoord.longitude];
    
    CLLocation* userLocation = [[CLLocation alloc] initWithLatitude:[_mapView.camera target].latitude longitude:[_mapView.camera target].longitude];
    
    distanceX = [mapLocation distanceFromLocation:userLocation] / 1000;
    
    NSLog(@"#distanceX :%f %f",distanceX,distanceY/2);
    
    if(distanceX > distanceY/2){
    myDoubleNumber = 0;
    distanceY = distanceX;
    myDoubleNumber = [NSNumber numberWithDouble:distanceX];;
    [self callStoreLocationService];
   
    }


}

*/

-(UIActivityIndicatorView *) setNaviationButtonWithActivityIndicator{
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.frame = CGRectMake(0, 0, 30, 30);
    [activityIndicator startAnimating];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator] ;
    return activityIndicator ;
}
-(void)backClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
