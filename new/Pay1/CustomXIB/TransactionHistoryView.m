//
//  TransactionHistoryView.m
//  Pay1
//
//  Created by webninjaz on 19/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "TransactionHistoryView.h"

@implementation TransactionHistoryView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self awakeFromNib];
    }
    return self;
}

-(void)awakeFromNib {
    UIView* myView = [[[NSBundle mainBundle] loadNibNamed:@"TransactionHistoryView" owner:self options:nil] objectAtIndex:0];
    myView.backgroundColor = [UIColor whiteColor];
    
    [myView setFrame:CGRectMake(30, 10, self.frame.size.width - 30, 250)];
   // [noNetworkImgView setFrame:CGRectMake(0, 0, myView.frame.size.width, myView.frame.size.height)];
    myView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        myView.transform = CGAffineTransformMakeScale(1.2, 1.2);
        [myView setAlpha:1.0];
    } completion:^(BOOL finished){
        
    }];
    CALayer *l = [myView layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:15.0];
    //[myView addSubview:noNetworkImgView];
    [self addSubview:myView];
    
}

-(IBAction)onSubmitClicked:(id)sender{
    NSLog(@"#SubmitClick");
    if(_onSubmitClick)
        _onSubmitClick();
}

@end
