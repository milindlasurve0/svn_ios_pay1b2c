//
//  TransactionCancelAlertView.h
//  Pay1
//
//  Created by Annapurna on 10/07/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransactionCancelAlertView : UIView
{
    IBOutlet UIImageView *logoImageView;
    IBOutlet UIImageView *operatorLogoImageView;
    
    IBOutlet UILabel *numberLabel;
    IBOutlet UILabel *amountPriceLabel;
    IBOutlet UIView *detailInfoView;
    
    IBOutlet UILabel *coinLabel;
    IBOutlet UILabel *typeLabel;
    
    IBOutlet UILabel *successLabel;
    IBOutlet UIView *missedCallView;
    
    
    
    
    
    
    
}
-(void)PutDataOnCancelAlert:(NSString *)imageName phoneNumber:(NSString *)phoneNumber amount:(NSString *)amount type:(NSString *)type;
@property (nonatomic, copy)void (^onAwesomeClick)();
@property (nonatomic, copy)void (^onLaterClick)();




@end
