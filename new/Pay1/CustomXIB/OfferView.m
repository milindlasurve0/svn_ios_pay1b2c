//
//  OfferView.m
//  Pay1
//
//  Created by Annapurna on 05/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "OfferView.h"

@implementation OfferView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self awakeFromNib];
    }
    return self;
}
-(void)awakeFromNib {
    UIView* myView = [[[NSBundle mainBundle] loadNibNamed:@"OfferView" owner:self options:nil] objectAtIndex:0];
    [myView setFrame:CGRectMake(300, 300, self.frame.size.width, self.frame.size.height)];
    
    [self addSubview:myView];
}

-(IBAction)moneyClick:(id)sender{
    if(_onMoneyClick)
    _onMoneyClick();
}

-(IBAction)giftClick:(id)sender{
    if(_onGiftClick)
    _onGiftClick();
}

-(IBAction)rechargeClick:(id)sender{
    if(_onRechargeClick)
    _onRechargeClick();
}

-(IBAction)homeClick:(id)sender{
    if(_onHomeClick)
    _onHomeClick();
}

@end
