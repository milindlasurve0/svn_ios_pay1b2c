//
//  DatePickerView.h
//  Pay1
//
//  Created by webninjaz on 19/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DatePickerView : UIView{
    IBOutlet UIDatePicker *datePicker;
    IBOutlet UIButton *setButton;
    IBOutlet UIButton *cancelButton;
    
    
}

@property (nonatomic, copy) void (^onDatePickerSetClick)(NSString *selectedDate);
@property (nonatomic, copy) void (^onDatePickerCancelClick)();

@end
