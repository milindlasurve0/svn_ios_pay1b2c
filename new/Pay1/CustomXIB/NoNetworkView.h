//
//  NoNetworkView.h
//  Pay1
//
//  Created by Annapurna on 11/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoNetworkView : UIView
{
    IBOutlet UIImageView *noNetworkImgView;
    IBOutlet UIButton *retryButton;
}
@property (nonatomic, copy) void (^onRetryClick)();



@end
