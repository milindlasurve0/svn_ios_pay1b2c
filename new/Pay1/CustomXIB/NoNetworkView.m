//
//  NoNetworkView.m
//  Pay1
//
//  Created by Annapurna on 11/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "NoNetworkView.h"

@implementation NoNetworkView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self awakeFromNib];
    }
    return self;
}

-(void)awakeFromNib {
    UIView* myView = [[[NSBundle mainBundle] loadNibNamed:@"NoNetworkView" owner:self options:nil] objectAtIndex:0];
    myView.backgroundColor = [UIColor whiteColor];
    
   // [myView setFrame:CGRectMake(30, 10, self.frame.size.width - 30, 250)];
   // [noNetworkImgView setFrame:CGRectMake(0, 0, myView.frame.size.width, myView.frame.size.height)];
   // myView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        myView.transform = CGAffineTransformMakeScale(1.1, 1.1);
        [myView setAlpha:1.0];
    } completion:^(BOOL finished){
        
    }];
   /* CALayer *l = [myView layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:15.0];
    [myView addSubview:noNetworkImgView];*/
    [self addSubview:myView];
    
}

-(IBAction)retryClick:(id)sender{
    NSLog(@"#RetryClick");
    if(_onRetryClick)
    _onRetryClick();
}

@end
