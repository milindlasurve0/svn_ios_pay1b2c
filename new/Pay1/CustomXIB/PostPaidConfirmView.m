//
//  PostPaidConfirmView.m
//  Pay1
//
//  Created by webninjaz on 06/09/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "PostPaidConfirmView.h"
#import "TPKeyboardAvoidingScrollView.h"
@implementation PostPaidConfirmView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self awakeFromNib];
    }
    return self;
}
-(void)awakeFromNib {
    
    checkButton.selected = TRUE;
    
     myView = [[[NSBundle mainBundle] loadNibNamed:@"PostPaidConfirmView" owner:self options:nil] objectAtIndex:0];
    
    if([UIScreen mainScreen].bounds.size.width == 320){
        myView.frame = CGRectMake(5, 0, 290, 350);
    }
    
    errorTF.hidden = TRUE;
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = [NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],[[UIBarButtonItem alloc]initWithTitle:@"GO" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],nil];
    [numberToolbar sizeToFit];
    amountTF.inputAccessoryView = numberToolbar;
    [self addSubview:myView];
    
}

-(void)PutDataOnCancelAlert:(NSString *) phoneNumber chargeSlab:(NSString *)chargeSlab chargePercent:(NSString *)chargePercent chargeAmount:(NSString *)chargeAmount{
    mobileNumLabel.text = [NSString stringWithFormat:@"%@\n%@ .",@"Make payment of your mobile bill",phoneNumber];
    serviceChargeLabel.text = [NSString stringWithFormat:@"(%d = %@ + %@ service charge inclusive of all taxes.)",0,@"0",@"0"];
    
    chargeSlabStr = chargeSlab;
    chargePerStr = chargePercent;
    chargeAmountStr = chargeAmount;
    
    NSLog(@"Charges Value :%@ %@ %@",chargeSlab,chargePercent,chargeAmount);

}

-(IBAction)checkButtonCLick:(id)sender{
    
    if(_onCheckClick){
    if(checkButton.selected == TRUE){
        [checkButton setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
        _onCheckClick(TRUE);

    }
    
    if(checkButton.selected == FALSE){
        [checkButton setBackgroundImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
        _onCheckClick(FALSE);

    }

    }
    
    checkButton.selected = !checkButton.selected;
}


-(IBAction)proceedBtnClick:(id)sender{
    if(amountTF.text.length == 0){
        errorTF.hidden = FALSE;
        sepView.backgroundColor = [UIColor redColor];
        
        return;
    }
    
    
    if(_onProceedClick)
        _onProceedClick(netAmountForRecharge,amountTF.text,chargeAmountStr,chargePerStr);
}

-(IBAction)cancelBtnClick:(id)sender{
    if(_onCancelClick)
        _onCancelClick();
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    // NSLog(@"#MobileTextFieldLength :%d %d %d",textField.text.length,mobileNumTxtField.text.length,string.length);
    
    NSString *editedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    
    if(editedText.length >0){
    sepView.backgroundColor = [UIColor darkGrayColor];
        errorTF.hidden = TRUE;
    }
   
        
        NSLog(@"#TaxPecent :%@ %@ %@",chargeAmountStr,chargeSlabStr,chargePerStr);
        
        NSArray *items0 = [chargeSlabStr componentsSeparatedByString:@","];
        
        NSLog(@"items0 :%@",items0);
        
        
        
        NSArray *items1 = [[items0 objectAtIndex:0] componentsSeparatedByString:@":"];
        
        NSArray *items2 = [[items0 objectAtIndex:1] componentsSeparatedByString:@":"];
        
        
        NSLog(@"items1 :%@",items1);
        NSLog(@"items2 :%@",items2);
        
        NSString *firstPrice = [items1 objectAtIndex:0];
        NSString *secondPrice = [items1 objectAtIndex:1];
        
        NSString *thirdPrice = [items2 objectAtIndex:0];
        NSString *fourthPrice = [items2 objectAtIndex:1];
        
        NSLog(@"#CheckFirstText :%@",[editedText substringFromIndex:0]);
        
        NSString *firstAmountStr = [editedText substringFromIndex:0];
        
        //   if(editedText substringFromIndex:)
        
        if([firstAmountStr intValue]<=[firstPrice intValue]){
            serviceChargeLabel.text = [NSString stringWithFormat:@"(%d = %@ + %@ service charge inclusive of all taxes.)",[editedText intValue],editedText,@"0"];
            
            int amountPostPaid = [editedText intValue] + 0;
            
            
            netAmountForRecharge = [NSString stringWithFormat:@"%d",[editedText intValue] + amountPostPaid];
        }
        else if([firstAmountStr intValue] > [firstPrice intValue] && [firstAmountStr intValue]<= [thirdPrice intValue] ){
            serviceChargeLabel.text = [NSString stringWithFormat:@"(%d = %@ + %d service charge inclusive of all taxes.)",[editedText intValue]+[secondPrice intValue],editedText,[secondPrice intValue]];
            
            int amountPostPaid = [editedText intValue] + [secondPrice intValue];
            
            netAmountForRecharge = [NSString stringWithFormat:@"%d",amountPostPaid];
            
        }
        else{
            serviceChargeLabel.text = [NSString stringWithFormat:@"(%d = %@ + %d service charge inclusive of all taxes.)",[editedText intValue]+[fourthPrice intValue],editedText,[fourthPrice intValue]];
            
            int amountPostPaid = [editedText intValue] + [fourthPrice intValue];
            
            
            netAmountForRecharge = [NSString stringWithFormat:@"%d",amountPostPaid];
            
        }
        
        
        NSLog(@"#AmountForPostPaidRecharge :%@",netAmountForRecharge);
        
        
        // rechargeAmount = [editedText intValue]+ 10;
    
    return TRUE;
    }


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return TRUE;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{       // return NO to disallow editing.
    [myView setFrame:CGRectMake(0,-100,[UIScreen mainScreen].bounds.size.width - 20,350)];
    return TRUE;
}

-(void)cancelNumberPad{
    [self cancelBtnClick:self];
}

-(void)doneWithNumberPad{
    [self proceedBtnClick:self];
}

@end
