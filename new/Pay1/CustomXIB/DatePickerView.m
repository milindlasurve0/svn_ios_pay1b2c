//
//  DatePickerView.m
//  Pay1
//
//  Created by webninjaz on 19/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "DatePickerView.h"

@implementation DatePickerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self awakeFromNib];
    }
    return self;
}


-(void)awakeFromNib {
    UIView* myView = [[[NSBundle mainBundle] loadNibNamed:@"DatePickerView" owner:self options:nil] objectAtIndex:0];
    myView.backgroundColor = [UIColor whiteColor];
    
    
    [myView setFrame:CGRectMake(30, 10, self.frame.size.width - 60, 240)];
    datePicker.frame = CGRectMake(0, 0,myView.frame.size.width , 210);
        
    setButton.frame = CGRectMake(10, datePicker.frame.origin.y + datePicker.frame.size.height  , 100, 40);
    cancelButton.frame = CGRectMake(setButton.frame.origin.x + setButton.frame.size.width + 10, datePicker.frame.origin.y + datePicker.frame.size.height , 100, 40);

    cancelButton.layer.cornerRadius = 5;
    cancelButton.layer.borderColor = [UIColor blackColor].CGColor;
    cancelButton.layer.borderWidth = 1;
    
    setButton.layer.cornerRadius = 5;
    setButton.layer.borderColor = [UIColor blackColor].CGColor;
    setButton.layer.borderWidth = 1;

    
    
 //   [myView setFrame:CGRectMake(30, 10, self.frame.size.width - 30, 250)];
    myView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
    myView.transform = CGAffineTransformMakeScale(1.2, 1.2);
    [myView setAlpha:1.0];
    } completion:^(BOOL finished){
        
    }];
    CALayer *l = [myView layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:15.0];
    [self addSubview:myView];
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSLog(@"DOB %@",[ud stringForKey:@"dob"]);
    
    
    if([ud stringForKey:@"dob"].length > 0){
        
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:[ud stringForKey:@"dob"]];
        
    NSLog(@"dateFromString :%@",dateFromString);
        
    datePicker.date = dateFromString;
    }
    
}

-(NSString*)selectedItem{
    
    // NSDate *date = [datePickerView date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    NSString *dateString = [dateFormatter stringFromDate:datePicker.date ];
    NSLog(@"the date String is :%@",dateString);
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:dateString forKey:@"dob"];
    [ud synchronize];

    
   
    return dateString;
}

-(IBAction)onsetClicked:(id)sender{
    NSLog(@"#Set Click %@",[self selectedItem]);
    ;
    if(_onDatePickerSetClick)
    _onDatePickerSetClick([self selectedItem]);
}


-(IBAction)onCancelClicked:(id)sender{
    NSLog(@"#Set Click");
    if(_onDatePickerCancelClick)
    _onDatePickerCancelClick();
}


@end
