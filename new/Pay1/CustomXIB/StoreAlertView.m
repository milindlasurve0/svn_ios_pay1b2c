//
//  StoreAlertView.m
//  Pay1
//
//  Created by webninjaz on 03/09/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "StoreAlertView.h"

@implementation StoreAlertView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self awakeFromNib];
    }
    return self;
}
-(void)awakeFromNib {
    
    
    UIView* myView = [[[NSBundle mainBundle] loadNibNamed:@"StoreAlertView" owner:self options:nil] objectAtIndex:0];
    [self addSubview:myView];
    
}



-(IBAction)awesomeBtnClick:(id)sender{
    if(_onCheckClick){
        
        if(checkButton.selected){
            [checkButton setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
        }
        else{
            [checkButton setBackgroundImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
 
        }
        
        checkButton.selected = !checkButton.selected;
        
        _onCheckClick();

        
    }
}

-(IBAction)laterBtnClick:(id)sender{
    if(_onDoneClick)
        _onDoneClick();
}


@end
