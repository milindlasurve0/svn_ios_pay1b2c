//
//  TopOfferCell.m
//  Pay1
//
//  Created by Annapurna on 11/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "MyGiftsCell.h"
#import "GetMyDealList.h"
#import "Constant.h"
@implementation MyGiftsCell

- (void)awakeFromNib {
    // Initialization code
    if([UIScreen mainScreen].bounds.size.width == 320){
    giftImageView.frame = CGRectMake(8, 10, 320- 15, 160);
    blackView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    blackView.frame = CGRectMake(9, 125, 320-15, 45);
    }
    
    blackView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    
    giftThumbNailImageView.layer.cornerRadius = giftThumbNailImageView.frame.size.height/2;
    giftThumbNailImageView.clipsToBounds = YES;
    giftThumbNailImageView.layer.masksToBounds = YES;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    NSArray *tblView=[[NSBundle mainBundle]loadNibNamed:@"MyGiftsCell" owner:self options:nil];
    self=[tblView objectAtIndex:0];
        
    self.frame = CGRectMake(0, 0, self.frame.size.width, 250);

    }
    return self;
}
-(void)showMyGiftDeal:(GetMyDealList *)myDeal{
    
    expiredLabel.hidden = TRUE;
    
    NSLog(@"Coupon Status :%@",myDeal.couponStatus);
   
    if(![myDeal.couponStatus isEqualToString:@"Active"]){
        
    if([myDeal.couponStatus isEqualToString:@"Redeem"])
    expiredLabel.text = @"REDEEMED"; //[myDeal.couponStatus uppercaseString];
    else
    expiredLabel.text =  [myDeal.couponStatus uppercaseString];
 
    expiredLabel.hidden = FALSE;
    }
    
    
    [giftImageView setImageWithURL:[NSURL URLWithString:myDeal.myDealImgURL] placeholderImage:[UIImage imageNamed:@"deal_default.png"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        UIImage *newImage = [UIImage imageWithImage:image  scaledToSize:CGSizeMake(320,212)];
        
    giftImageView.image = newImage;
    }usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)UIActivityIndicatorViewStyleGray];
    
    [giftThumbNailImageView setImageWithURL:[NSURL URLWithString:myDeal.myDealLogoURL] placeholderImage:[UIImage imageNamed:@"deal_default.png"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        UIImage *newImage = [UIImage imageWithImage:image  scaledToSize:CGSizeMake(320,212)];
    giftThumbNailImageView.image = newImage;
    }usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)UIActivityIndicatorViewStyleGray];
    
    
    giftPointLabel.text = myDeal.minAmount;
    
    NSString *name = [myDeal.dealName stringByReplacingOccurrencesOfString:@"%27" withString:@"'"];

    giftNameLabel.text = name; //myDeal.dealName;
    
    giftDetailLabel.text = myDeal.shortDesc;
    giftDetailLabel.lineBreakMode = NSLineBreakByWordWrapping;
    giftDetailLabel.numberOfLines = 0;
    [giftDetailLabel sizeToFit];
   // NSLog(@"#MyGifts :%@---%@",myDeal.dealName,myDeal.shortDesc);
    
    offerPriceBGView.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 150, giftDetailLabel.frame.origin.y + giftDetailLabel.frame.size.height, 150, 40);
    
    if(![[myDeal offerPrice]isEqualToString:@"0"]){
     //   rupeeLabel.text = [NSString stringWithFormat:@"%@",[myDeal offerPrice]];
        NSString *ofPrice =  [NSString stringWithFormat:@"%@",[myDeal offerPrice]]; //[offerPriceArray objectAtIndex:i];
        
        rupeeLabel.text = [NSString stringWithFormat:@"%@",ofPrice];
        
        if(ofPrice.length > 3){
            rupeeImageView.frame = CGRectMake(12, 12, 18, 18);
            rupeeLabel.frame = CGRectMake(32, 9, 38, 21);
            plusImageView.frame = CGRectMake(75, 16, 10, 10);
        }
        
        
        if(ofPrice.length == 3){
            rupeeImageView.frame = CGRectMake(22, 12, 18, 18);
            rupeeLabel.frame = CGRectMake(43, 9, 38, 21);
            plusImageView.frame = CGRectMake(72, 16, 10, 10);
        }

        
        else{
            rupeeImageView.frame = CGRectMake(25, 12, 18, 18);

            rupeeLabel.frame = CGRectMake(45, 9, 30, 21);
            plusImageView.frame = CGRectMake(72, 16, 10, 10);
        }
        
        //[NSString stringWithFormat:@"%@",[offerPriceArray objectAtIndex:indexPath.row]];
        
        [rupeeLabel sizeToFit];
        [rupeeLabel sizeToFit];
        
          }
    else{
    rupeeLabel.hidden = TRUE;
    rupeeImageView.hidden = TRUE;
    plusImageView.hidden = TRUE;
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
