//
//  TopOfferCell.h
//  Pay1
//
//  Created by Annapurna on 11/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Utility.h"
@interface MyLikesCell : UITableViewCell{
    NSMutableArray *offerIDArrayList;
    NSArray *offerIDArray;
    NSMutableArray *imageURLArray;
    NSMutableArray *thumbURLArray;
    NSMutableArray *shopNameArray;
    NSMutableArray *offerArray;
    NSMutableArray *priceArray;
    NSMutableArray *offerPriceArray;


    IBOutlet UIView *blackView;
    
    NSMutableArray *dealArray;
    NSMutableArray *offerIDList;
    BOOL isHeartClicked;
    
    IBOutlet UIView *offerPriceBGView;
    
    

}

@property (nonatomic, strong) IBOutlet UIImageView *offerImageView;
@property (nonatomic, strong) IBOutlet UIImageView *offerThumbNailImageView;
@property (nonatomic, strong) IBOutlet UILabel *offerDetailLabel;
@property (nonatomic, strong) IBOutlet UILabel *offerPointLabel;
@property (nonatomic, strong) IBOutlet UILabel *offerNameLabel;
@property (nonatomic, strong) IBOutlet UIImageView *moneyImageView;
@property (nonatomic, strong) IBOutlet UIButton *heartButton;

@property (nonatomic, strong) IBOutlet UILabel *rupeeLabel;
@property (nonatomic, strong) IBOutlet UIImageView *rupeeImageView;

@property (nonatomic, strong) IBOutlet UIButton *heartBGButton;
@property (nonatomic, strong) IBOutlet UIImageView *plusImageView;






-(void)fetchMyLikesDataFromDb:(NSString *)likeStr;
@property(nonatomic, copy) void (^onTableRowClick)(NSString * dealID,NSString *offerID,NSString *shop);
@property(nonatomic, copy) void (^onHeartClick)();


@end
