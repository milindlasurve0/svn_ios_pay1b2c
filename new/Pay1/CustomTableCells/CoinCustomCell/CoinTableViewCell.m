//
//  CoinTableViewCell.m
//  Pay1
//
//  Created by webninjaz on 01/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "CoinTableViewCell.h"

@implementation CoinTableViewCell

- (void)awakeFromNib {
    if([UIScreen mainScreen].bounds.size.width > 320){
        _dateTimeLabel.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 150, 105, 140, 21);
    }}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *tblView=[[NSBundle mainBundle]loadNibNamed:@"CoinTableViewCell" owner:self options:nil];
        self=[tblView objectAtIndex:0];
        
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
