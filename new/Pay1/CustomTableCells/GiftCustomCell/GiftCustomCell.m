//
//  GiftCustomCell.m
//  Pay1
//
//  Created by Annapurna on 16/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "GiftCustomCell.h"
#import "GiftList.h"
#import "DBManager.h"
#import "Session.h"
@implementation GiftCustomCell

- (void)awakeFromNib {
    // Initialization code
    
    offerIDArrayList = [[NSMutableArray alloc]init];
    imageURLArray = [[NSMutableArray alloc]init];
    shopNameArray = [[NSMutableArray alloc]init];
    offerArray = [[NSMutableArray alloc]init];
    areaArray = [[NSMutableArray alloc]init];
    priceArray = [[NSMutableArray alloc]init];
    offerPriceArray = [[NSMutableArray alloc]init];
    offerVoucherArray = [[NSMutableArray alloc]init];
    
    dealArray = [[NSMutableArray alloc]init];
    offerIDList = [[NSMutableArray alloc]init];

    
    UIView* myView = [[[NSBundle mainBundle] loadNibNamed:@"OfferView" owner:self options:nil] objectAtIndex:0];
    [myView setFrame:CGRectMake(300, 300, self.frame.size.width, self.frame.size.height)];
    
    
    sepView.backgroundColor = [UIColor colorWithRed:244/255.0 green:244/255.0 blue:244/255.0 alpha:1];
    
    [self addSubview:myView];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *tblView=[[NSBundle mainBundle]loadNibNamed:@"GiftCustomCell" owner:self options:nil];
        
        self=[tblView objectAtIndex:0];
        self.frame = CGRectMake(0, 0, self.frame.size.width , 260);
       
    }
    return self;
}

-(void)loadGiftDataOnTableView:(GiftList *)giftList rowIndex:(NSInteger)rowIndex{
    seeAllButton.tag = rowIndex;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(seeAllClicked:)];
    tapGesture.delegate = self;
    tapGesture.view.tag = rowIndex;

    [seeAllButton addGestureRecognizer:tapGesture];

    if([UIScreen mainScreen].bounds.size.width == 320){
        seeAllButton.frame = CGRectMake(320 - 70, 7, 70, 30);
    }
    titleLabel.text = giftList.name;
    
   
    [offerIDArrayList addObject:giftList.details];
    

    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
    dispatch_async(dispatch_get_main_queue(), ^{
    [self fetchValueFromDBForGifts:offerIDArrayList];
    });
    });
    });
    
    
}



-(void)fetchValueFromDBForGifts:(NSMutableArray *)dataArray{
    for(int i=0;i<dataArray.count;i++){
    offerIDArray= [[dataArray objectAtIndex:i] componentsSeparatedByString:@","];
    [self fetchDataFromDB];
    }
    
}

-(void)fetchDataFromDB{
    
    float x = 10;

    
    NSString *query;
    NSMutableArray *offerArrayList = [[NSArray arrayWithArray:offerIDArray] mutableCopy];
    DBManager *dbManager = [[DBManager alloc]initWithDatabaseFilename:@"dbgiftdata.sql"];
    for(int i=0;i<offerArrayList.count;i++){
    offserValue = [offerArrayList objectAtIndex:i];
        
  //  NSLog(@"offserValue :%@",offserValue);
    query = [NSString stringWithFormat:@"select * from GIFTTABLE WHERE FREEBIEOFFERID=\"%@\"",offserValue];
   // NSLog(@"DB is Mobile:%@ %lu",[[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]],(unsigned long)[[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]].count);
        
    if([[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]].count == 0){
  //  NSLog(@"Array is empty");
        [imageURLArray addObject: @""];
        [shopNameArray addObject: @""];
        [areaArray addObject: @""];
        [offerArray addObject: @""];
        [priceArray addObject: @""];
        [dealArray addObject: @""];
        [offerIDList addObject: @""];
        [offerPriceArray addObject:@""];


    }
        
        
        
    else{
    NSArray *theArray = [[NSArray alloc] initWithArray:[dbManager loadDataFromDB:query]];
        
    NSInteger indexOfImageURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEURL"];
    for(int i=0;i<theArray.count;i++){
    [imageURLArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfImageURL]];
    }
    NSInteger indexOfShopNameURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEDEALNAME"];
    for(int i=0;i<theArray.count;i++){
    [shopNameArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfShopNameURL]];
    }
    NSInteger indexOfAreaURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEAREA"];
    for(int i=0;i<theArray.count;i++){
    [areaArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfAreaURL]];
    }
    NSInteger indexOfOfferURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIESHORTDESC"];
    for(int i=0;i<theArray.count;i++){
    [offerArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOfferURL]];
    }
    NSInteger indexOfPriceURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEMINAMOUNT"];
    for(int i=0;i<theArray.count;i++){
    [priceArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfPriceURL]];
    }
        
    NSInteger indexOfOfferPriceURL = [dbManager.arrColumnNames indexOfObject:@"FREEBIEOFFERPRICE"];
    for(int i=0;i<theArray.count;i++){
    [offerPriceArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOfferPriceURL]];
    }
        
    NSInteger indexOfDeal = [dbManager.arrColumnNames indexOfObject:@"FREEBIEDEALID"];
    for(int i=0;i<theArray.count;i++){
    [dealArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfDeal]];
    }
        
    NSInteger indexOfOfferID = [dbManager.arrColumnNames indexOfObject:@"FREEBIEOFFERID"];
    for(int i=0;i<theArray.count;i++){
   // NSLog(@"OfferID%@", [[theArray objectAtIndex:i] objectAtIndex:indexOfOfferID]);
    [offerIDList addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOfferID]];
    }
        
    NSInteger indexOfOfferVoucher = [dbManager.arrColumnNames indexOfObject:@"FREEBIEVOUCHER"];
    for(int i=0;i<theArray.count;i++){
    [offerVoucherArray addObject: [[theArray objectAtIndex:i] objectAtIndex:indexOfOfferVoucher]];
    }
        
       
        
    giftImgView = [[UIImageView alloc]initWithFrame:CGRectMake(x, 13, 140, 65)];
     

    [giftImgView setImageWithURL:[NSURL URLWithString:[imageURLArray objectAtIndex:i]] placeholderImage:[UIImage imageNamed:@"deal_default.png"]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
    UIImage *newImage = [UIImage imageWithImage:image  scaledToSize:CGSizeMake(320,212)];
    giftImgView.image = newImage;
    }usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)UIActivityIndicatorViewStyleGray];
    
        
    giftImgView.userInteractionEnabled = TRUE;
        
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageTapped:)];
    tapGesture.delegate = self;
    giftImgView.tag = i; //[[dealArray objectAtIndex:i]intValue]; //[objGiftDetailList.offerID intValue];
        
    [giftImgView addGestureRecognizer:tapGesture];
        
        
      //  NSLog(@"offer title  label :%f %f",offerTitleLabel.frame.origin.y,offerTitleLabel.frame.size.height);
        
        offerTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(x, giftImgView.frame.origin.y + giftImgView.frame.size.height + 2, giftImgView.frame.size.width, 40)];
        offerTitleLabel.text = [offerArray objectAtIndex:i]; //objGiftDetailList.offer;
        offerTitleLabel.font = [UIFont systemFontOfSize:13]; //[UIFont fontWithName:@"OpenSans-Regular" size:8.0];
        offerTitleLabel.textColor = [UIColor darkGrayColor];
        offerTitleLabel.numberOfLines = 2;
        //offerTitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [offerTitleLabel sizeToFit];
        
       // shopName = [[UILabel alloc]initWithFrame:CGRectMake(x, offerTitleLabel.frame.origin.y + offerTitleLabel.frame.size.height + 5  , giftImgView.frame.size.width - 10, 42)];
        
        shopName = [[UILabel alloc]initWithFrame:CGRectMake(x,116 , giftImgView.frame.size.width - 10, 42)];

        
        NSString *name = [[shopNameArray objectAtIndex:i] stringByReplacingOccurrencesOfString:@"%27" withString:@"'"];
        
        shopName.text = name; //[shopNameArray objectAtIndex:i]; //objGiftDetailList.deal;
        shopName.font = [UIFont systemFontOfSize:11];
        shopName.textColor = [UIColor lightGrayColor];
      //  shopName.lineBreakMode = NSLineBreakByWordWrapping;
        shopName.numberOfLines = 2;
        
        [shopName sizeToFit];
        
        
      //  NSLog(@"SOPFRAME :%f %f %@",shopName.frame.origin.y,shopName.frame.size.height,[offerPriceArray objectAtIndex:i]);
        
        UIImageView *rupeeImageView;
        UILabel *rupeePriceLabel;
        UIImageView *plusImageView;
        
      //  NSLog(@"offerVoucherArray :%@ %lu %@ %lu",offerVoucherArray,(unsigned long)offerVoucherArray.count,offerPriceArray,(unsigned long)offerPriceArray.count);
        
        
      // if(offerPriceArray.count > 1){
      //  if(![[offerVoucherArray objectAtIndex:i]isEqualToString:@"0"]){
     //   if([[offerVoucherArray objectAtIndex:i]isEqualToString:@"1"]){
            
     if(![[offerPriceArray objectAtIndex:i]isEqualToString:@"0"]){
        
      //  if(![[offerPriceArray objectAtIndex:0]isEqualToString:@"0"]){



        NSString * priceLength = [offerPriceArray objectAtIndex:i];
            
            
            rupeeImageView = [[UIImageView alloc]initWithFrame:CGRectMake(x, 147, 15, 15)];
            rupeeImageView.image = [UIImage imageNamed:@"rupee.png"];
            
            if(priceLength.length <3)
             rupeePriceLabel = [[UILabel alloc]initWithFrame:CGRectMake(rupeeImageView.frame.origin.x + rupeeImageView.frame.size.width , 145, priceLength.length+15, 20)];
             else
            rupeePriceLabel = [[UILabel alloc]initWithFrame:CGRectMake(rupeeImageView.frame.origin.x + rupeeImageView.frame.size.width , 145, priceLength.length+25, 20)];
            
            
            rupeePriceLabel.text = [NSString stringWithFormat:@"%@",[offerPriceArray objectAtIndex:i]];
            rupeePriceLabel.font = [UIFont systemFontOfSize:13];
            
            plusImageView = [[UIImageView alloc]initWithFrame:CGRectMake(rupeePriceLabel.frame.origin.x + rupeePriceLabel.frame.size.width , 150, 10, 10)];
            plusImageView.image = [UIImage imageNamed:@"plus.png"];
            
            pointBtn = [[UIButton alloc]initWithFrame:CGRectMake(plusImageView.frame.origin.x + plusImageView.frame.size.width +5 , 147, 15, 15)];
            [pointBtn setBackgroundImage:[UIImage imageNamed:@"gift.png"] forState:UIControlStateNormal];
            
            
            pointLabel = [[UILabel alloc]initWithFrame:CGRectMake(pointBtn.frame.origin.x + pointBtn.frame.size.width, 145 , 30, 21)];
            
            
        }
       // }
        
        else{
        pointBtn = [[UIButton alloc]initWithFrame:CGRectMake(x, 147, 15, 15)];
        [pointBtn setBackgroundImage:[UIImage imageNamed:@"gift.png"] forState:UIControlStateNormal];
        
        
        pointLabel = [[UILabel alloc]initWithFrame:CGRectMake(pointBtn.frame.origin.x + pointBtn.frame.size.width, 145 , 30, 21)];
        }
        
        pointLabel.text = [priceArray objectAtIndex:i];
        pointLabel.font = [UIFont systemFontOfSize:13];
        pointLabel.textColor = [UIColor darkGrayColor];
        
        NSString *areaStr = [areaArray objectAtIndex:i];
        
      //  NSLog(@"#Area :%@ %d",[areaArray objectAtIndex:i],areaStr.length);
        
        
        
        
        
     //   addressLabel = [[UILabel alloc]initWithFrame:CGRectMake(addressBtn.frame.origin.x + addressBtn.frame.size.width + 5, 147, giftImgView.frame.size.width, 15)];
        
        
        // addressLabel = [[UILabel alloc]initWithFrame:CGRectMake(x, 147, giftImgView.frame.size.width, 15)];
        
        
      
        
        addressLabel = [[UILabel alloc]initWithFrame:CGRectMake(x + 50, 147, giftImgView.frame.size.width - 50, 15)];
        
        addressLabel.text = [areaArray objectAtIndex:i];
        
        addressLabel.font = [UIFont systemFontOfSize:10];
        addressLabel.textColor = [UIColor lightGrayColor];
        addressLabel.textAlignment = NSTextAlignmentRight;
        addressLabel.adjustsFontSizeToFitWidth = YES;
        addressLabel.minimumScaleFactor = 0;
        
        
        CGSize yourLabelSize = [addressLabel.text sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:10]}];
        
        CGRect rect = addressLabel.frame;
        rect.origin.x = x +giftImgView.frame.size.width - yourLabelSize.width;
        rect.size.width = yourLabelSize.width;
        addressLabel.frame = rect;

        
        addressBtn = [[UIButton alloc]initWithFrame:CGRectMake(addressLabel.frame.origin.x - 20, 147, 15, 15)];
         [addressBtn setBackgroundImage:[UIImage imageNamed:@"gift_locator.png"] forState:UIControlStateNormal];
        
        
       /* NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
        attachment.image = [UIImage imageNamed:@"pay1_iconsmall.png"];
        
        NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
        
        NSMutableAttributedString *myString= [[NSMutableAttributedString alloc] initWithString:[areaArray objectAtIndex:i]];
        [myString appendAttributedString:attachmentString];
        
        addressLabel.attributedText = myString;*/
        
        
        
       // addressLabel.backgroundColor = [UIColor yellowColor];
        
        
        
        
      //  addressBtn = [[UIButton alloc]initWithFrame:CGRectMake(addressLabel.text.length, 1, 15, 15)];
       // [addressBtn setBackgroundImage:[UIImage imageNamed:@"pay1_icon.png"] forState:UIControlStateNormal];

        
        [giftCellScrollView addSubview:giftImgView];
        [giftCellScrollView addSubview:offerTitleLabel];
        [giftCellScrollView addSubview:shopName];
        
        [giftCellScrollView addSubview:rupeeImageView];
        [giftCellScrollView addSubview:plusImageView];
        [giftCellScrollView addSubview:rupeePriceLabel];
        
        [giftCellScrollView addSubview:pointBtn];
        [giftCellScrollView addSubview:pointLabel];

      
        
        if(areaStr.length > 0){
        [giftCellScrollView addSubview:addressBtn];
          //  [addressLabel addSubview:addressBtn];
        [giftCellScrollView addSubview:addressLabel];
        }
        
        x+= 150;
    }
    }
    
   // }
    
    //  giftCellScrollView.contentSize = CGSizeMake(giftCellScrollView.frame.size.width * detailsDic.count , 0);
    
  //  NSLog(@"#TheXValues :%f",x);
    
    giftCellScrollView.contentSize = CGSizeMake(x + 50 , 0);
    
    
}


-(void)imageTapped:(UIGestureRecognizer *)tapGesture{
    NSLog(@"#TheTapGestureValueis :%ld  %@ %@",(long)tapGesture.view.tag,shopName.text,addressLabel.text);
    
    NSString *deal = [dealArray objectAtIndex:tapGesture.view.tag];
    NSString *offer = [offerIDList objectAtIndex:tapGesture.view.tag];
    NSString *shopNameTxt = [shopNameArray objectAtIndex:tapGesture.view.tag];
   // NSLog(@"## :%@ %@",deal,offer);
    
    
    if(_onImageViewClick){
        _onImageViewClick(deal,offer,shopNameTxt);
    }
}

//-(void)seeAllClicked:(UIButton *)sender{
-(void)seeAllClicked:(UITapGestureRecognizer *)sender{

 //   NSLog(@"Values :%@ %@ %ld",titleLabel.text,offserValue,(long)sender.view.tag);
    
    NSString *offerVoucher = [offerVoucherArray objectAtIndex:sender.view.tag];
    NSString *offerIDS = [offerIDArrayList objectAtIndex:0]; //[offerIDArrayList objectAtIndex:sender.view.tag];
  //  NSLog(@"offerIDS :%@",offerIDS);
    

    if(_onSeeAllClick){
    _onSeeAllClick(titleLabel.text,offerIDS,offerVoucher,sender.view.tag);
        
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
