//
//  MapCustomCell.h
//  Pay1
//
//  Created by Annapurna on 18/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>
@class GiftLocationList;
@interface MapCustomCell : UITableViewCell{
    IBOutlet UILabel *addressLabel;
    IBOutlet UIButton *phoneNumButton;
    IBOutlet UILabel *distanceLabel;
    IBOutlet UIView *buttonBottomView;
    
    IBOutlet UIButton *mapPinImgView;
    
    NSString *giftLocLat;
    NSString *giftLocLong;

}
-(void)showGiftLocationOnMap:(GiftLocationList *)objList;
@property (nonatomic,copy) void (^onPhoneButtonCLick)(NSString *phoneNumber);
@property (nonatomic,copy) void (^onMapButtonCLick)(NSString *theGiftLocLat, NSString *theGiftLocLong,NSString *theGiftLocAddress);

@end
