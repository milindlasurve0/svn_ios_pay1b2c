//
//  PlanCustomCell.h
//  Pay1
//
//  Created by Annapurna on 01/07/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlanCustomCell : UITableViewCell{
    IBOutlet UILabel *daysLabel;
    IBOutlet UILabel *descLabel;
    IBOutlet UILabel *moneyLabel;
    IBOutlet UIView *underLineView;
}
-(void)showPlanData:(NSString *)planV planAmount:(NSString *)planAmount planDesc:(NSString *)planDesc;
@end
