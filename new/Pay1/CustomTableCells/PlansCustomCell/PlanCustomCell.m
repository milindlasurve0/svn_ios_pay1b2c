//
//  PlanCustomCell.m
//  Pay1
//
//  Created by Annapurna on 01/07/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "PlanCustomCell.h"

@implementation PlanCustomCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *tblView=[[NSBundle mainBundle]loadNibNamed:@"PlanCustomCell" owner:self options:nil];
        self=[tblView objectAtIndex:0];
    }
    return self;
}

- (void)awakeFromNib {
    
    if([UIScreen mainScreen].bounds.size.width > 320){
    moneyLabel.frame = CGRectMake(self.frame.origin.x + self.frame.size.width - 103, 37, 100, 21);
    descLabel.frame = CGRectMake(10, daysLabel.frame.origin.y + daysLabel.frame.size.height + 8, self.frame.size.width - 0, 21);
    }
    
}

-(void)showPlanData:(NSString *)planV planAmount:(NSString *)planAmount planDesc:(NSString *)planDesc{
    daysLabel.text = planV;
    moneyLabel.text = [NSString stringWithFormat:@"%@",planAmount];
    descLabel.text = planDesc;
    
    CGSize yourLabelSize = [moneyLabel.text sizeWithAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"OpenSans-SemiBold" size:16.0]}];
    
    CGRect rect = moneyLabel.frame;
    rect.size.width = yourLabelSize.width + 20;
    moneyLabel.frame = rect;

    
   // underLineView.frame.size.width = rect.size.width; //moneyLabel.frame.size.width;
    
    underLineView.frame = CGRectMake(220, 59,  moneyLabel.frame.size.width - 20, 1);
    
    
    
    
   /* CGSize size = [PlanCustomCell sizeForText:planDesc];
    CGRect rect = descLabel.frame;
    rect.size.width = size.width;
    
    rect.size.height =  size.height;//number+10;
    descLabel.frame = rect;*/
    
  //  NSLog(@"#PlanCellHeight :%f",rect.size.height);
    
   /* descLabel.lineBreakMode = NSLineBreakByWordWrapping;
    descLabel.numberOfLines = 0;
    [descLabel sizeToFit];*/

  }

+(CGSize) sizeForText:(NSString *) text {
   // CGSize size = [text sizeWithAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"Open Sans" size:14.0]}];

   // return size;
    
    
    CGRect textRect = [text boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width - 20, FLT_MAX) options:NSStringDrawingUsesLineFragmentOrigin  attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Open Sans" size:14.0]}context:nil];
    
    CGSize size = textRect.size;

    return size;
    
   // return [text sizeWithFont:[UIFont fontWithName:@"Open Sans" size:14.0] constrainedToSize:CGSizeMake(210.0f, FLT_MAX)];
}

+(float) CellHeight:(NSString *) text {
    CGSize size = [PlanCustomCell sizeForText:text];
   return  size.height + 80;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
