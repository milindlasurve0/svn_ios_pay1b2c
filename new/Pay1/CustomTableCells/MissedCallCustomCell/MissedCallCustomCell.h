//
//  MissedCallCustomCell.h
//  Pay1
//
//  Created by Annapurna on 14/07/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MissedCallCustomCell : UITableViewCell{
    
}

@property (nonatomic, strong) IBOutlet UIImageView *logoImageView;
@property (nonatomic, strong) IBOutlet UILabel *phoneNumLabel;
@property (nonatomic, strong) IBOutlet UILabel *missedNumLabel;
@property (nonatomic, strong) IBOutlet UILabel *priceLabel;
@property (nonatomic, strong) IBOutlet UISwitch *cellSwitch;
@property (nonatomic, strong) IBOutlet UIImageView *operatorLogoImageView;




@end
