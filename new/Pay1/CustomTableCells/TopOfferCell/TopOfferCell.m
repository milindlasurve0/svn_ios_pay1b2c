//
//  TopOfferCell.m
//  Pay1
//
//  Created by Annapurna on 11/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "TopOfferCell.h"

@implementation TopOfferCell

- (void)awakeFromNib {
    // Initialization code
    
    
    if([UIScreen mainScreen].bounds.size.width == 320){
  //  NSLog(@"WidthIs320 In TopOffer%f",self.frame.size.width);
    _offerImageView.frame = CGRectMake(8, 10, 320- 15, 160);
    
    _heartButton.frame = CGRectMake(320-40, 21, 25, 25);
    blackView.frame = CGRectMake(9, 125, 320-15, 45);

    }
    blackView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    _offerThumbNailImageView.layer.cornerRadius = _offerThumbNailImageView.frame.size.height/2;
    _offerThumbNailImageView.clipsToBounds = YES;
    _offerThumbNailImageView.layer.masksToBounds = YES;
    
    isHeartClicked = NO;

}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *tblView=[[NSBundle mainBundle]loadNibNamed:@"TopOfferCell" owner:self options:nil];
        self=[tblView objectAtIndex:0];
    }
    return self;
}

-(IBAction)heartClick:(id)sender{
    if(_onHeartClick){
    if(isHeartClicked == NO){
    [_heartButton setBackgroundImage:[UIImage imageNamed:@"red_heart.png"] forState:UIControlStateNormal];
    }
    else{
    [_heartButton setBackgroundImage:[UIImage imageNamed:@"ic_unlike.png"] forState:UIControlStateNormal];
    }
    isHeartClicked = !isHeartClicked;
    _onHeartClick();
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
