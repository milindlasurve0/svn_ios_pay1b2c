//
//  TopOfferCell.h
//  Pay1
//
//  Created by Annapurna on 11/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopOfferCell : UITableViewCell{
    IBOutlet UIView *blackView;
    BOOL isHeartClicked;
        
}

@property (nonatomic, strong) IBOutlet UIImageView *offerImageView;
@property (nonatomic, strong) IBOutlet UIImageView *offerThumbNailImageView;
@property (nonatomic, strong) IBOutlet UILabel *offerDetailLabel;
@property (nonatomic, strong) IBOutlet UILabel *offerPointLabel;
@property (nonatomic, strong) IBOutlet UILabel *offerNameLabel;
@property (nonatomic, strong) IBOutlet UIButton *heartButton;
@property (nonatomic, strong) IBOutlet UIImageView *moneyImageView;

@property (nonatomic, strong) IBOutlet UILabel *rupeeLabel;
@property (nonatomic, strong) IBOutlet UIImageView *rupeeImageView;

@property (nonatomic, strong) IBOutlet UIView *offerPriceBGView;
@property (nonatomic, strong) IBOutlet UIImageView *plusImageView;


@property(nonatomic, copy) void (^onHeartClick)();


@end
