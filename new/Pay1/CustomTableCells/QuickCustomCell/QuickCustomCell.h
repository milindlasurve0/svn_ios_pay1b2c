//
//  QuickCustomCell.h
//  Pay1
//
//  Created by Annapurna on 08/07/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuickCustomCell : UICollectionViewCell{
    
}
@property (nonatomic, strong) IBOutlet UIView *bgView;
@property (nonatomic, strong) IBOutlet UIImageView *logoImageView;
@property (nonatomic, strong) IBOutlet UIImageView *operatorLogoImageView;

@property (nonatomic, strong) IBOutlet UILabel *numLabel;
@property (nonatomic, strong) IBOutlet UILabel *amountLanel;

@end
