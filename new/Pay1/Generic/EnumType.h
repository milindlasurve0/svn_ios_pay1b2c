//
//  EnumType.h
//  Pay1
//
//  Created by Annapurna on 05/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

typedef enum{
    kServiceTypeQuickRecharge = 0,
    kServiceTypeMobile,
    kServiceTypePostPaid,
    kServiceTypeDTH,
    kServiceTypeDataCard
}ServiceType;

typedef enum{
    kSelectedTypeGift= 0,
    kSelectedTypeNearYou,
    kSelectedTypeCategories,
    kSelectedTypeMyGift,
    kSelectedTypeMyLikes
}GiftSelectedType;

typedef enum{
    kDataTypeThreeG= 0,
    kDataTypeTwoG,
    kDataTypeOther,
    kDataTypeTopup
}DataSelectedType;


