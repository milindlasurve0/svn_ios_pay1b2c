//
//  ServiceManager.m
//  ASIHTTPDemo
//
//  Created by Sunita on 02/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ServiceManager.h"

#import "ASIFormDataRequest.h"
#import "ASINetworkQueue.h"

#import "SBJSON.h"
#import "XMLReader.h"



@implementation ServiceManager
static ServiceManager *sharedInstance = nil;

+ (ServiceManager *)sharedInstance {
  if (sharedInstance == nil) {
    sharedInstance = [[super allocWithZone:NULL] init];
  }
  
  return sharedInstance;
}

- (id)init
{
  self = [super init];
  
  if (self) {
    // Work your initialising magic here as you normally would
  }
  
  return self;
}

-(void)dealloc
{
  // I'm never called!
  [super dealloc];
}

+ (id)allocWithZone:(NSZone*)zone {
  return [[self sharedInstance] retain];
}


-(void)showLoader:(NSTimer *)timer
{
	[[[[UIApplication sharedApplication] delegate] window] addSubview:[timer userInfo]];
}
//internal
-(void)hideLoader:(NSTimer *)timer
{
	[[timer userInfo] removeFromSuperview];
}



-(void)makeRequest:(RequestData *)requestData{
	
	if(requestData.showLoader)
	{
		[NSTimer scheduledTimerWithTimeInterval: 0.01 target: self  selector: @selector(showLoader:) userInfo:requestData.loaderView repeats: NO];
	}
	
  if(requestData.requestType == Request_Type_Get)
    [self makeRequestForGet:requestData];
  else if(requestData.requestType == Request_Type_Post)
    [self makeRequestForPost:requestData];
}



-(void)makeRequestForGet:(RequestData *)requestData {
  NSURL *requestURL=[self createRequestUrl:requestData];
  NSLog(@"GetURL%@",requestURL);
  ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:requestURL];
	[request setRequestData:requestData];
  [request setDelegate:self];
  [request setShouldAttemptPersistentConnection:NO];
  [request startAsynchronous];
}



-(void)makeRequestForPost:(RequestData *)requestData{
  NSURL *requestURL = [self createRequestUrl:requestData];
  NSLog(@"PostURL%@",requestURL);

	ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:requestURL];
	[request setRequestData:requestData];
	for (NSString *key in [requestData.postData allKeys])
		[request setPostValue:[requestData.postData valueForKey:key] forKey:key];
	for (NSString *key in [requestData.fileData allKeys])
		[request setFile:[requestData.fileData valueForKey:key] forKey:key];
	
	ASINetworkQueue *queue=[[ASINetworkQueue alloc] init];
	[queue setDelegate:self];
	[queue setRequestDidFinishSelector:@selector(requestFinished:)];
	[queue setRequestDidFailSelector:@selector(requestFailed:)];
	
	[queue addOperation:request];
	[queue go];
	[queue release];
}



-(NSURL *)createRequestUrl:(RequestData *)requestData{
  NSString *string = [self makeKeyUsingDictionary:requestData.getData];
  NSString *urlString = [NSString stringWithFormat:@"%@%@%@",requestData.baseURL,requestData.webServiceURL,string];
  return [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];;
}



-(NSString*) makeKeyUsingDictionary:(NSDictionary *)dic{
  NSArray *dicKeyArray = [dic allKeys];
  NSString *parametrString = @"?";
  for(int i=0;i<[dicKeyArray count];i++)
  {
    NSString *key = [dicKeyArray objectAtIndex:i];
    if(i == 0)
      parametrString = [NSString stringWithFormat:@"%@%@=%@",parametrString,key,[dic objectForKey:key]];
    else
      parametrString = [NSString stringWithFormat:@"%@&%@=%@",parametrString,key,[dic objectForKey:key]];
  }
  return parametrString;
}


- (void)requestFinished:(ASIHTTPRequest *)request {
  //NSLog(@"responseString%@",[request responseString]);
	
	[self respondeToCaller:request];
}


- (void)requestFailed:(ASIHTTPRequest *)request {
  NSLog(@"error%@",[request error]);
	RequestData *reqData=[request requestData];
	reqData.responseData = [request error];

	if(reqData.showLoader)
	{
		[NSTimer scheduledTimerWithTimeInterval: 0.01 target: self  selector: @selector(hideLoader:) userInfo:reqData.loaderView repeats: NO];
	}
	
	if([reqData.delegate respondsToSelector:reqData.failureSelector])
		[reqData.delegate performSelector:reqData.failureSelector withObject:reqData];
	
	if(reqData.requestFailureBlock)
		reqData.requestFailureBlock(reqData);
}


-(void)respondeToCaller:(ASIHTTPRequest *)request
{
	RequestData *reqData=[request requestData];
	
	if(reqData.showLoader)
	{
		[NSTimer scheduledTimerWithTimeInterval: 0.01 target: self  selector: @selector(hideLoader:) userInfo:reqData.loaderView repeats: NO];
	}
	
	
	if(reqData.responseType == Response_Type_Dic)
	{
		NSMutableString *responseString= [self fixResponseStringForDic:(NSMutableString *)[request responseString]];
		
		if(reqData.serviceResponseType ==  Service_Response_JSON)
		{
			SBJSON *json = [[SBJSON new] autorelease];
			NSError *error;
			reqData.responseData = [json objectWithString:responseString error:&error];
		}
		else if(reqData.serviceResponseType ==  Service_Response_XML)
		{
			NSError *error;
			reqData.responseData = [XMLReader dictionaryForXMLString:[request responseString] error:&error];
		}
		
		if([reqData.delegate respondsToSelector:reqData.successSelector])
			[reqData.delegate performSelector:reqData.successSelector withObject:reqData];
		
		if(reqData.requestSuccessBlock)
			reqData.requestSuccessBlock(reqData);

	}
	else if( reqData.responseType == Response_Type_Data)
	{
		reqData.responseData = [request responseData];
		
		if([reqData.delegate respondsToSelector:reqData.successSelector])
			[reqData.delegate performSelector:reqData.successSelector withObject:reqData];
		
		if(reqData.requestSuccessBlock)
			reqData.requestSuccessBlock(reqData);

		
	}
	else if(reqData.responseType == Response_Type_String)
	{
		reqData.responseData = [request responseString];
		if([reqData.delegate respondsToSelector:reqData.successSelector])
			[reqData.delegate performSelector:reqData.successSelector withObject:reqData];
		
		if(reqData.requestSuccessBlock)
			reqData.requestSuccessBlock(reqData);
	}
	//[reqData release];

	
}



-(NSMutableString *) fixResponseStringForDic:(NSMutableString *) responseString {
	
	NSInteger indexOfFinctionStart=[responseString rangeOfString:@"("].location;
	
	if(indexOfFinctionStart>=0
		 && [responseString rangeOfString:@"["].location>indexOfFinctionStart
		 && [responseString rangeOfString:@"{"].location>indexOfFinctionStart
		 && [responseString rangeOfString:@"\""].location>indexOfFinctionStart
		 )
	{
		NSMutableArray *responseComponents=(NSMutableArray *)[responseString componentsSeparatedByString:@"("];
		
		if( [responseComponents count] > 0 )
			[responseComponents removeObjectAtIndex:0];
		
		responseString=(NSMutableString *)[responseComponents componentsJoinedByString:@"("];
		
		responseComponents=(NSMutableArray *)[responseString componentsSeparatedByString:@")"];
		
		if( [responseComponents count] > 0 )
			[responseComponents removeObjectAtIndex:[responseComponents count]-1];
		
		responseString=(NSMutableString *)[responseComponents componentsJoinedByString:@")"];
		
		
	}
	
	return responseString;

}

@end
