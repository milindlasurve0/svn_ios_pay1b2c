//
//  ServiceLoaderView.h
//  ASIHTTPDemo
//
//  Created by Dharmender Yadav on 03/11/12.
//
//

#import <UIKit/UIKit.h>
@interface ServiceLoaderView : UIView{
}

+(ServiceLoaderView *)initializeLoader;
@property (nonatomic, assign) BOOL forBothMode;

@end
