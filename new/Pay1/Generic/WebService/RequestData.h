//
//  RequestData.h
//  ASIHTTPDemo
//
//  Created by Sunita on 02/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class RequestData;
typedef enum {
  Request_Type_Get=0,
  Request_Type_Post,
}RequestType;

typedef enum {
  Response_Type_Dic=0,
  Response_Type_String,
  Response_Type_Data,
}ResponseType;

typedef enum {
  Service_Response_XML=0,
  Service_Response_JSON,
}ServiceResponseType;

typedef void (^requestSuccessCallback)(RequestData *request);
typedef void (^requestFailureCallback)(RequestData *request);

@interface RequestData : NSObject
{
	NSString *baseURL;
  NSString *webServiceURL;
  
  NSMutableDictionary *getData;
  NSMutableDictionary *postData;
	NSMutableDictionary *fileData;

  id delegate;
  SEL successSelector;
  SEL failureSelector;
	
	UIView *loaderView;
	BOOL showLoader;
	
	
	
	id responseData;
	id callbackObject;
  
}
@property (nonatomic, strong) NSString *baseURL;
@property (nonatomic, strong) NSString *webServiceURL;

@property (nonatomic, strong) NSMutableDictionary *getData;
@property (nonatomic, strong) NSMutableDictionary *postData;
@property (nonatomic, strong) NSMutableDictionary *fileData;

@property (nonatomic, assign) id delegate;
@property (nonatomic, assign) SEL successSelector;
@property (nonatomic, assign) SEL failureSelector;
@property (nonatomic, copy) requestSuccessCallback requestSuccessBlock;
@property (nonatomic, copy) requestFailureCallback requestFailureBlock;


@property (nonatomic, assign) BOOL showLoader;
@property (nonatomic, strong) UIView *loaderView;

@property (nonatomic,assign) RequestType requestType;
@property (nonatomic,assign) ResponseType responseType;
@property (nonatomic,assign) ServiceResponseType serviceResponseType;

@property (nonatomic,strong) id responseData;
@property (nonatomic,strong) id callbackObject;

@end
