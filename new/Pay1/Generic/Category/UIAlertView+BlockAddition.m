




#import "UIAlertView+BlockAddition.h"
#import <objc/runtime.h>

static char DISMISS_IDENTIFER;
static char CANCEL_IDENTIFER;


@implementation UIAlertView (BlockAddition)
@dynamic cancelBlock;
@dynamic dismissBlock;

- (void)setDismissBlock:(DismissBlock)dismissBlock
{
  objc_setAssociatedObject(self, &DISMISS_IDENTIFER, dismissBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (DismissBlock)dismissBlock
{
  return objc_getAssociatedObject(self, &DISMISS_IDENTIFER);
}

- (void)setCancelBlock:(CancelBlock)cancelBlock
{
  objc_setAssociatedObject(self, &CANCEL_IDENTIFER, cancelBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (CancelBlock)cancelBlock
{
  return objc_getAssociatedObject(self, &CANCEL_IDENTIFER);
}


+ (UIAlertView*) alertViewWithTitle:(NSString*) title message:(NSString*) message cancelButtonTitle:(NSString*) cancelButtonTitle
                  otherButtonTitles:(NSArray*) otherButtons onDismiss:(DismissBlock) dismissed onCancel:(CancelBlock) cancelled {
  
  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                  message:message
                                                 delegate:[self class]
                                        cancelButtonTitle:cancelButtonTitle
                                        otherButtonTitles:nil];
  
  if(dismissed)
    [alert setDismissBlock:dismissed];
  
  if(cancelled)
    [alert setCancelBlock:cancelled];
  
  for(NSString *buttonTitle in otherButtons)
    [alert addButtonWithTitle:buttonTitle];
  
  [alert show];
  return alert;
}


+ (UIAlertView*) alertViewWithDefaultTitle:(NSString*) message cancelButtonTitle:(NSString*) cancelButtonTitle
                  otherButtonTitles:(NSArray*) otherButtons onDismiss:(DismissBlock) dismissed onCancel:(CancelBlock) cancelled {
  
  return [UIAlertView alertViewWithTitle:@"AlertTitle"
                                 message:message
                       cancelButtonTitle:cancelButtonTitle
                       otherButtonTitles:otherButtons
                               onDismiss:dismissed
                                onCancel:cancelled];
}





+ (UIAlertView*) alertViewWithDefaultTitle:(NSString*) message onCancel:(CancelBlock) cancelled {
  
  return [UIAlertView alertViewWithDefaultTitle:message cancelButtonTitle:@"OK" otherButtonTitles:nil onDismiss:NULL onCancel:cancelled];
}



+ (UIAlertView*) alertViewWithTitle:(NSString*) title message:(NSString*) message onCancel:(CancelBlock) cancelled {
  return [UIAlertView alertViewWithTitle:title
                                 message:message
                       cancelButtonTitle:@"OK"
                       otherButtonTitles:nil
                               onDismiss:NULL
                                onCancel:cancelled];
}



+ (UIAlertView*) alertViewWithDefaultTitle:(NSString*) message onDismiss:(DismissBlock)onDismiss  onCancel:(CancelBlock) cancelled {
  return [UIAlertView alertViewWithDefaultTitle:message
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:[NSArray arrayWithObjects:@"Cancel", nil]
                                      onDismiss:onDismiss onCancel:cancelled];
}



+ (UIAlertView*) alertViewWithTitle:(NSString*) title message:(NSString*) message onDismiss:(DismissBlock)onDismiss  onCancel:(CancelBlock) cancelled {
  return [UIAlertView alertViewWithTitle:title
                                 message:message
                       cancelButtonTitle:@"OK"
                       otherButtonTitles:[NSArray arrayWithObjects:@"Cancel", nil]
                               onDismiss:onDismiss
                                onCancel:cancelled];
}






+ (void)alertView:(UIAlertView*) alertView didDismissWithButtonIndex:(NSInteger) buttonIndex {
  
	if(buttonIndex == [alertView cancelButtonIndex])
	{
		if (alertView.cancelBlock) {
      alertView.cancelBlock();
    }
	}
  else
  {
    if (alertView.dismissBlock) {
      alertView.dismissBlock(buttonIndex - 1); // cancel button is button 0
    }
  }
}


@end

