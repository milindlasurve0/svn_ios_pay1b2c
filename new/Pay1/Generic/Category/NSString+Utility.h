//
//  NSString+Utility.h
//  PassengerCab
//
//  Created by Dharmender on 20/09/12.
//  Copyright (c) 2012 Hemantech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utility)

- (BOOL) isEmpty ;
- (NSString *) trim ;
+ (NSString *)intToBinary:(int)number ;
+ (NSString *)longlongToBinary:(long long)number ;
+ (NSString *) formatDateDifference:(NSDate *) fDate lDate:(NSDate *) lDate ;
+ (NSString *)handleNull:(id) object;
- (NSString *) search:(NSString *)regex replace:(NSString *)s ;
- (NSString *) convertToSingularOrPlural:(NSInteger)count ;
+ (NSString *)handleNullDecimal:(id) object;
+(NSArray *)handleUserInfoNull:(id)object;
@end
