




#import "NSDate-Extensions.h"

static NSCalendar *calendar;
@implementation NSDate (Extensions)

+ (void) initialize {
	dispatch_queue_t queue = dispatch_queue_create("Queue", NULL);
  dispatch_async(queue, ^{
    calendar = [[NSCalendar currentCalendar] retain];
    calendar.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
  });
}

+ (NSCalendar *) calendar {
  return calendar;
}

+ (NSDate *) dateForMinute:(uint)minute hour:(uint)hour day:(uint) day month:(uint)month year:(uint)year {
  NSDateComponents *c = [[[NSDateComponents alloc] init] autorelease] ;
  c.calendar = calendar;
  c.hour = hour;
  c.minute = minute;
  c.day = day;
  c.month = month;
  c.year = year;
  return [calendar dateFromComponents:c];
}

+ (NSDate *) dateForDay: (uint) day month: (uint) month year: (uint) year {
  NSDateComponents *c = [[[NSDateComponents alloc] init] autorelease] ;
  c.calendar = calendar;
  c.day = day;
  c.month = month;
  c.year = year;
  return [calendar dateFromComponents:c];
}

+ (NSDate *) currentDate {
  return [NSDate date];
}


+(NSDate *) convertStringInDate:(NSString *) dateStr  format: (NSString *) format {
	 NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
	formatter.dateFormat = format;
	formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
	//[formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
	//[formatter setLocale:[NSLocale currentLocale]];
	return [formatter dateFromString:dateStr];
}

- (NSString *) formattedDateString: (NSString *) format {
  NSDateFormatter *formatter = [self getFormatter];
  [formatter setDateFormat:format];
  return [formatter stringFromDate:self];
}

- (NSDate *) formattedDate: (NSString *) format {
  NSString *formattedDateString = [self formattedDateString:format];
  NSDateFormatter *dateFormatter = [self getFormatter];
  [dateFormatter setDateFormat:format];

  return [dateFormatter dateFromString:formattedDateString];
}


-(NSDateFormatter *) getFormatter {
  NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
  formatter.timeZone = calendar.timeZone;
  [formatter setLocale:[NSLocale currentLocale]];
  return formatter;
}


- (NSDate *) nextYear {
  NSDateComponents *components = self.dateComponents;
  [components setDay:1];
  [components setYear: components.year + 1];
  return [calendar dateFromComponents: components];
}

- (NSDate *) nextMonth {
  NSDateComponents *components = self.dateComponents;
  [components setDay:1];
  [components setMonth: components.month + 1];
  return [calendar dateFromComponents: components];
}

- (NSDate *) prevMonth {
  NSDateComponents *components = self.dateComponents;
  [components setDay:1];
  [components setMonth: components.month - 1];
  return [calendar dateFromComponents: components];
}

- (NSDate *) dayAfterDays:(NSInteger) days {
  NSDateComponents *offset = [[[NSDateComponents alloc] init] autorelease];
  [offset setDay: days];
  
  return [calendar dateByAddingComponents: offset toDate: self options: 0];
}

- (NSDate *) nextDay {
  return [self dayAfterDays:1];
}

- (NSDate *) prevDay {
  return [self dayAfterDays:-1];
}

- (NSInteger) timeOfDay {
  NSDateComponents *c = self.dateComponents;
  return c.hour*100 + c.minute;
}

- (NSInteger) daysUntil:(NSDate *) date {
  const int day = 24 * 60 * 60;
  NSTimeInterval end = [date timeIntervalSinceNow];
  NSTimeInterval start = [self timeIntervalSinceNow];
  return (end - start) / day;
}


- (NSDateComponents *) dateComponents {
  NSInteger units = NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSWeekdayCalendarUnit;
  return [calendar components:units fromDate: self];
}


- (NSDate *) getUTCFormateDate {
  NSDateFormatter *dateFormatter = [self getFormatter];
  [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
  [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
  return  [dateFormatter dateFromString:[dateFormatter stringFromDate:self]];
}


- (NSComparisonResult) compareDateOnly:(NSDate *) date {
  NSComparisonResult result = [[self formattedDate:@"yyyy-MM-dd"] compare:[date formattedDate:@"yyyy-MM-dd"]];
  return result;
}

- (BOOL) isSameDay:(NSDate*)date2 {
  unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
  NSDateComponents* comp1 = [calendar components:unitFlags fromDate:self];
  NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
  return [comp1 day]   == [comp2 day] && [comp1 month] == [comp2 month] && [comp1 year]  == [comp2 year];
}

@end
