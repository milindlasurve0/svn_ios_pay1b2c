

#import <UIKit/UIKit.h>

@interface UIImage (Utility)

- (id)initWithCoder:(NSCoder *)decoder;
- (void)encodeWithCoder:(NSCoder *)encoder;
- (UIImage *) strechedImage ;
- (UIImage *) strechedImage:(float)top left:(float)left bottom:(float)bottom right:(float)right;
- (CGRect) imagePositionForMaxFrame:(CGRect) rect;
- (NSData *) data;
- (UIImage *)fixOrientation;
+ (UIImage *) makeImageOfView:(UIView*)view imageSize:(CGSize)imageSize;
- (UIImage *) cropImageWithRect:(CGRect) rect;
+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
@end
