//
//  NSString+Utility.m
//  PassengerCab
//
//  Created by Dharmender on 20/09/12.
//  Copyright (c) 2012 Hemantech. All rights reserved.
//

#import "NSString+Utility.h"

@implementation NSString (Utility)

- (BOOL) isEmpty {
  return [[self trim] length] == 0;
}

- (NSString *) trim {
  return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

+ (NSString *)intToBinary:(int)number {
  int bits =  sizeof(number) * 8;
  NSMutableString *binaryStr = [NSMutableString string];
  for (; bits > 0; bits--, number >>= 1) {
    [binaryStr insertString:((number & 1) ? @"1" : @"0") atIndex:0];
  }
  return (NSString *)binaryStr;
}

+ (NSString *)longlongToBinary:(long long)number {
  int bits =  sizeof(number) * 8;
  NSMutableString *binaryStr = [NSMutableString string];
  for (; bits > 0; bits--, number >>= 1) {
    [binaryStr insertString:((number & 1) ? @"1" : @"0") atIndex:0];
  }
  return (NSString *)binaryStr;
}

+ (NSString *) formatDateDifference:(NSDate *) fDate lDate:(NSDate *) lDate {
  NSInteger layover = [fDate timeIntervalSinceDate: lDate] / 60;
  NSInteger minutes = layover % 60;
  NSInteger hours = layover / 60;
  NSString *hoursStr = [NSString stringWithFormat:@"%dh", hours];
  NSString *minutesStr = [NSString stringWithFormat:@"%dm", minutes];
  return [NSString stringWithFormat:@"%@%@%@", hours?hoursStr:@"", hours>0&minutes>0?@" ":@"", minutes?minutesStr:@""];
}


- (NSString *) search:(NSString *)regex replace:(NSString *)s {
  NSRegularExpression *r = [NSRegularExpression regularExpressionWithPattern:regex options:0 error:nil];
  NSString *output = [r stringByReplacingMatchesInString:self options:0 range:NSMakeRange(0, [self length]) withTemplate:@""];
  return output;
}

- (NSString *) convertToSingularOrPlural:(NSInteger)count {
  if(count <= 1)
    return self;
  return [self stringByAppendingString:@"s"];
}

+ (NSString *)handleNull:(id) object{
	if([object isKindOfClass:[NSNull class]] || object == NULL || object==nil || !object)
		return @"";
	return [object trim];
}
+ (NSString *)handleNullDecimal:(id) object{
	if([object isKindOfClass:[NSNull class]] || object == NULL || object==nil || !object)
		return @"";
	return object ;
}

+(NSArray *)handleUserInfoNull:(id)object{
  if([object isKindOfClass:[NSNull class]] || object == NULL || object==nil || !object)
  return nil;
	return object ;

}


@end
