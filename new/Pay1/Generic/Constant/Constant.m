//
//  Constant.m
//  Pay1
//
//  Created by Annapurna on 11/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "Constant.h"
#import "Reachability.h"
#import <CoreLocation/CoreLocation.h>
@implementation Constant

+(BOOL)checkNetworkConnection{
    Reachability *networkReachability = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable){
        return FALSE;
    }else{
        return TRUE;
    }
    
}

+(void)saveUserPhoneNumber:(NSString *)userNumber{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:userNumber forKey:userPhoneNum];
    [userDefault synchronize];
}
+(NSString *)getUserPhoneNumber{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault stringForKey:userPhoneNum];
}
+(void)saveUserPassword:(NSString *)userPass{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:userPass forKey:userLoginPassword];
    [userDefault synchronize];
}
+(NSString *)getUserPass{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault stringForKey:userLoginPassword];
 
}

+(void)saveUserCoinBalance:(NSString *)coinBal{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:coinBal forKey:coinBalance];
    [userDefault synchronize];
}
+(NSString *)getUserCoinBalance{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault stringForKey:coinBalance];
}


+(void)saveUserWalletBalance:(NSString *)wallBal{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:wallBal forKey:walletBalance];
    [userDefault synchronize];
}
+(NSString *)getUserWalletBalance{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault stringForKey:walletBalance];
}



+(NSString *)calculateDistanceBetweenTwoPoints:(NSString *)firstLat firstLong:(NSString *)firstLong secondLat:(NSString *)secondLat secondLong:(NSString *)secondLong{
    
 //   NSLog(@"First and second Lat :%@ %@ %@ %@",firstLat,firstLong,secondLat,secondLong);
    CLLocation *location1 = [[CLLocation alloc] initWithLatitude:[firstLat floatValue] longitude:[firstLong floatValue]];
    CLLocation *location2 = [[CLLocation alloc] initWithLatitude:[secondLat floatValue] longitude:[secondLong floatValue]];
  //  NSLog(@"Distance i meters: %f", [location1 distanceFromLocation:location2]);
    
    float distanceVal = [location1 distanceFromLocation:location2]/1000;
    
  //  NSLog(@"#DistanceVal :%f",distanceVal);
    
    float rounded_up = ceilf(distanceVal * 100) / 100;
    
      
    NSString *distance = [NSString stringWithFormat:@"%@Km",[NSString stringWithFormat:@"%.02f", rounded_up]];
    
    return distance;
}
+(void)saveMyGiftsList:(NSMutableArray *)saveProductArrayList{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
   // NSLog(@"saveProductArrayList :%@",saveProductArrayList);
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:saveProductArrayList];
    [userDefaults setObject:data forKey:myGiftsList];
    [userDefaults synchronize];    
}
+(NSArray*)getMyGiftsList{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [userDefaults objectForKey:myGiftsList];
    NSArray *myArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
   // NSLog(@"myArrayList:%@",myArray);
    // return [userDefaults arrayForKey:saveProduct];
    return myArray;
}

+(void)saveMyGiftsCount:(NSString *)gCount{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:gCount forKey:myGiftsCount];
    [userDefault synchronize];
}
+(NSString*)getMyGiftsCount{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault stringForKey:myGiftsCount];
}
+(void)saveDealFroPurchasedGift:(NSDictionary *)dealDic{
    [[NSUserDefaults standardUserDefaults] setObject:dealDic forKey:purchaseDealList];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+(NSDictionary *)getPurchaseDealGift{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault dictionaryForKey:purchaseDealList];
}



+(NSString *)convertServerDate:(NSString *)serverDate{
    //  NSLog(@"time :%@",serverDate);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];// here give the format which you get in TimeStart
    
    NSDate *date = [dateFormatter dateFromString: serverDate];
    
    dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setAMSymbol:@"AM"];
    [dateFormatter setPMSymbol:@"PM"];

    [dateFormatter setDateFormat:@"dd-MMM-yyyy, hh:mm a"];
    
    NSString *convertedString = [dateFormatter stringFromDate:date];
      //NSLog(@"Converted String : %@",convertedString);
       return convertedString;
}

+(NSString *)convertBirthDate:(NSString *)birthDate{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];// here give the format which you get in TimeStart
    
    NSDate *date = [dateFormatter dateFromString: birthDate];
    
    dateFormatter = [[NSDateFormatter alloc] init] ;
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString *convertedString = [dateFormatter stringFromDate:date];
    //NSLog(@"Converted String : %@",convertedString);
    return convertedString;
}

+(NSString *)convertServerBirthDate:(NSString *)birthDate{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];// here give the format which you get in TimeStart
    
    NSDate *date = [dateFormatter dateFromString: birthDate];
    
    dateFormatter = [[NSDateFormatter alloc] init] ;
    
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    
    NSString *convertedString = [dateFormatter stringFromDate:date];
    //NSLog(@"Converted String : %@",convertedString);
    return convertedString;
}

+(NSString *)convertRechargeDateTime:(NSString *)rechargeDate{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *date = [dateFormatter dateFromString: rechargeDate];
    
    dateFormatter = [[NSDateFormatter alloc] init] ;
    
    [dateFormatter setDateFormat:@"dd-MMM"];
    
    NSString *convertedString = [dateFormatter stringFromDate:date];
    //NSLog(@"Converted String : %@",convertedString);
    return convertedString;

}

+(void)saveUserImage:(UIImage *)userImg userKey:(NSString *)userKey{
    NSLog(@"TheUserImageIS :%@",userImg);
    NSData* imageData = UIImagePNGRepresentation(userImg);
    NSData* myEncodedImageData = [NSKeyedArchiver archivedDataWithRootObject:imageData];
    [[NSUserDefaults standardUserDefaults] setObject:myEncodedImageData forKey:userKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+(UIImage *)getUserImage:(NSString *)userKey{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSData* myEncodedImageData = [userDefault objectForKey:userKey];
    NSData *imageData  = [NSKeyedUnarchiver unarchiveObjectWithData:myEncodedImageData];
    UIImage* image = [UIImage imageWithData:imageData];
    return image;
}

+(NSString *)currentSystemDateTime{
    NSDate * startDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *systemDate = [dateFormatter stringFromDate:startDate];
  //  NSLog(@"#SystemDateTime :%@",systemDate);
    return systemDate;
}

+(void)saveLastServicePlanTime:(NSString *)servicePlan{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setValue:servicePlan forKey:serPlan];
    [userDefault synchronize];
}

+(NSString *)getLastTimeServicePlan{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
  //  NSLog(@"the date nsuser default is :%@",[userDefault stringForKey:serPlan]);
    return [userDefault stringForKey:serPlan];
}


+(void)saveLastMobilePlanTime:(NSString *)mobilePlan{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setValue:mobilePlan forKey:mobPlan];
    [userDefault synchronize];
}

+(NSString *)getLastTimeMobilePlan{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
   // NSLog(@"the date nsuser default is :%@",[userDefault stringForKey:mobPlan]);
    return [userDefault stringForKey:mobPlan];
}

+(void)saveLastGiftTime:(NSString *)gftTime{
  //  NSLog(@"#GiftTime :%@",gftTime);
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setValue:gftTime forKey:giftTime];
    [userDefault synchronize];
}
+(NSString *)getLastTimeGiftPlan{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
   // NSLog(@"the date nsuser default is :%@",[userDefault stringForKey:giftTime]);
    return [userDefault stringForKey:giftTime];
}

/*
+(void)saveFirstTimeAppLaunched:(NSString *)frstTime{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setValue:frstTime forKey:firstTime];
    [userDefault synchronize];
}
+(NSString *)getFirstTimeAppLaunched{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSLog(@"the date nsuser default is :%@",[userDefault stringForKey:firstTime]);
    return [userDefault stringForKey:firstTime];
}*/

+(NSDate *)convertTheDate:(NSString *)theDate{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:theDate];
    return dateFromString;
}




+(NSString *)convertTheDateFormat:(NSString *)theDate{
    // NSLog(@"time :%@",time);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];// here give the format which you get in TimeStart
    
    NSDate *date = [dateFormatter dateFromString: theDate];
    
    dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm a"];
    
    NSString *convertedString = [dateFormatter stringFromDate:date];
    //  NSLog(@"Converted String : %@",convertedString);
    return convertedString;
}




+(NSString *)getCurrentSystemDate{
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormat stringFromDate:today];
    
    return dateString;
}

+(BOOL)returnDataOfCompareDates:(NSString *)myDate{
    NSComparisonResult result;
    BOOL isResult = '\0';
    
    NSLog(@"#TheDate :%@ %@",myDate,[Constant getCurrentSystemDate]);
    
    result = [[Constant getCurrentSystemDate] compare:myDate];
    
    if(result == NSOrderedAscending || result == NSOrderedSame){
    isResult = TRUE;
    }
    if(result == NSOrderedDescending){
    isResult = FALSE;
    }
    
    return isResult;
}
+(void)saveFaceBookCalledRecord:(BOOL)isCalled{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setBool:isCalled forKey:fbCalled];
    [userDefault synchronize];
}
+(BOOL)getFacebookCalledRecord{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault boolForKey:fbCalled];
}

+(void)saveCheckButtonClick:(BOOL)isCheck{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setBool:isCheck forKey:checkButtonClick];
    [userDefault synchronize];
}
+(BOOL)getCheckButtonClick{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault boolForKey:checkButtonClick];
}

+(void)saveQuickPayRecord:(BOOL)isQuickPay{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setBool:isQuickPay forKey:QuickPay];
    [userDefault synchronize];
}
+(BOOL)getQuickPayRecord{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault boolForKey:QuickPay];
    
}

+(void)makePhoneCall:(NSString *)phoneNumber{
  //  NSLog(@"#PhoneNumber :%@",phoneNumber);
    NSString *callNumber = [@"tel://" stringByAppendingString:phoneNumber];
  //  NSLog(@"CallNumber :%@",callNumber);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:callNumber]];
}

+(NSString *)decodeNumber:(NSString *)decodeNumber{
        
    NSString *encodedString = [decodeNumber stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"encodedString :%@",encodedString);

    
    return encodedString;
}

+ (NSString *)generateMD5:(NSString *)string{
    const char *cStr = [string UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
}

+(void)saveUserLoginFailedStatus:(BOOL)isCheck{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setBool:isCheck forKey:isShowLoginFalied];
    [userDefault synchronize];
}
+(BOOL)getUserLoginFailedStatus{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault boolForKey:isShowLoginFalied];
}

+(void)saveHomeRefreshStatus:(BOOL)refresh{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setBool:refresh forKey:isRefreshed];
    [userDefault synchronize];
}
+(BOOL)getHomeRefreshStatus{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    return [userDefault boolForKey:isRefreshed];
}


@end
