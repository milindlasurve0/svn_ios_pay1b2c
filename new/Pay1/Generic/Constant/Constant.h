//
//  Constant.h
//  Pay1
//
//  Created by Annapurna on 11/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CommonCrypto/CommonDigest.h>


#define userPhoneNum @"phoneNum"
#define userLoginPassword @"password"
#define myGiftsList @"myGifts"
#define myGiftsCount @"giftsCount"
#define purchaseDealList @"theDealList"
//#define purchaseDealID @"theDealID"
#define loginUserImage @"img"
#define serPlan @"sPlan"
#define mobPlan @"mPlan"
#define giftTime @"gTime"

//#define tableNameFirst @"giftTable"
#define fbCalled @"facebookCalled"

#define QuickPay @"quick"
#define coinBalance @"cBalance"
#define walletBalance @"wBalance"
#define checkButtonClick @"checkClick"
#define isShowLoginFalied @"isSHown"
#define isRefreshed @"isCalledRefreshed"

@interface Constant : NSObject


+(BOOL)checkNetworkConnection;


+(void)saveUserPhoneNumber:(NSString *)userNumber;
+(NSString *)getUserPhoneNumber;

+(void)saveUserCoinBalance:(NSString *)coinBal;
+(NSString *)getUserCoinBalance;


+(void)saveUserWalletBalance:(NSString *)wallBal;
+(NSString *)getUserWalletBalance;


+(NSString *)calculateDistanceBetweenTwoPoints:(NSString *)firstLat firstLong:(NSString *)firstLong secondLat:(NSString *)secondLat secondLong:(NSString *)secondLong;
+(void)saveMyGiftsList:(NSMutableArray *)saveProductArrayList;
+(NSArray*)getMyGiftsList;
+(void)saveMyGiftsCount:(NSString *)gCount;
+(NSString*)getMyGiftsCount;

/*
+(void)saveDealFroPurchasedGift:(NSDictionary *)dealDic;
+(NSDictionary *)getPurchaseDealGift;
+(void)saveDealID:(NSMutableArray *)dealID;
+(NSArray *)getDealID;
 */
 
 
+(NSString *)convertServerDate:(NSString *)serverDate;
+(void)saveUserPassword:(NSString *)userPass;
+(NSString *)getUserPass;

+(void)saveUserImage:(UIImage *)userImg userKey:(NSString *)userKey;
+(UIImage *)getUserImage:(NSString *)userKey;

+(NSString *)convertBirthDate:(NSString *)birthDate;
+(NSString *)convertServerBirthDate:(NSString *)birthDate;


+(NSString *)currentSystemDateTime;


+(void)saveCheckButtonClick:(BOOL)isCheck;
+(BOOL)getCheckButtonClick;


+(void)saveLastServicePlanTime:(NSString *)servicePlan;
+(NSString *)getLastTimeServicePlan;

+(void)saveLastMobilePlanTime:(NSString *)mobilePlan;
+(NSString *)getLastTimeMobilePlan;

+(void)saveLastGiftTime:(NSString *)gftTime;
+(NSString *)getLastTimeGiftPlan;

+(NSDate *)convertTheDate:(NSString *)theDate;
+(NSString *)getCurrentSystemDate;

+(BOOL)returnDataOfCompareDates:(NSString *)myDate;

+(void)saveFaceBookCalledRecord:(BOOL)isCalled;
+(BOOL)getFacebookCalledRecord;

+(void)saveQuickPayRecord:(BOOL)isQuickPay;
+(BOOL)getQuickPayRecord;

+(void)makePhoneCall:(NSString *)phoneNumber;

+(NSString *)decodeNumber:(NSString *)decodeNumber;
+(NSString *)convertTheDateFormat:(NSString *)theDate;

+ (NSString *)generateMD5:(NSString *)string;

+(NSString *)convertRechargeDateTime:(NSString *)rechargeDate;


+(void)saveUserLoginFailedStatus:(BOOL)isCheck;
+(BOOL)getUserLoginFailedStatus;

+(void)saveHomeRefreshStatus:(BOOL)refresh;
+(BOOL)getHomeRefreshStatus;


@end
