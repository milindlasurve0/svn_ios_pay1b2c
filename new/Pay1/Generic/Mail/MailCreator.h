
#import <Foundation/Foundation.h>
#import <Foundation/Foundation.h>
#import <MessageUI/MFMailComposeViewController.h>



@interface MailCreator : NSObject <MFMailComposeViewControllerDelegate>{
	id delegate;	
	NSMutableString *typeOfCall;
	NSMutableString *idOfCall;
	NSMutableString *emailSubject;
	NSMutableString *emailBody;
	NSString *emailBodyToHyperLink;
	NSString *recipient;
	
	MFMailComposeViewController *picker;
	NSMutableArray *arrayForAttachements;
}
@property (nonatomic, strong ) NSString *emailBodyToHyperLink;
@property(nonatomic,strong)id delegate;
@property(nonatomic,strong) NSMutableString *typeOfCall;
@property(nonatomic,strong) NSMutableString *idOfCall;
@property(nonatomic,strong) NSString *recipient;
@property(nonatomic,strong) NSMutableString *emailSubject, *emailBody;
@property(nonatomic,strong)NSMutableArray *arrayForAttachements;

-(void)mail:(id)del;
-(void)displayComposerSheet;
@end
