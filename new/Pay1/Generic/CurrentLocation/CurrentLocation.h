//
//  CurrentLocation.h
//  CurrentLocation
//
//  Created by Dharmender Yadav on 21/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

typedef void (^geocoderCallback)(NSError *error, CLPlacemark *placemark);
typedef void (^citygeocoderCallback)(NSError *error, CLPlacemark *cityPlacemark);

@interface CurrentLocation : NSObject<CLLocationManagerDelegate>
{
  CLGeocoder *geocoder;  
}

@property (nonatomic, retain) CLLocationManager *locationMgr;
@property (nonatomic, retain) CLLocation *currentLocation;
@property (nonatomic, retain) CLPlacemark *placemark;

@property (nonatomic, assign) BOOL isGeoCodingForCurrentLocation;
//@property (nonatomic, assign) id delegate;
//@property (nonatomic, assign) SEL selectorLocationUpdate;
//@property (nonatomic, assign) SEL selectorLocationError;


+(CurrentLocation*)sharedLocation;

-(void)stopUpdatingLocation;
-(void)startUpdatingLocation;


#pragma mark Distance Calcutaion

-(double)getDistanceFromCurrentLocation :(double )desLat longitudeAddress:(double)desLong;
-(double) distance:(double)lati1  lon1:(double)long1 lat2:(double)lat2 lon2:(double)lon2;
-(float)toDegree:(float)rad;
-(float)toRadian:(float)deg;

#pragma mark ReverseGeoCoder
-(void) addressFromLocation:(CLLocation *) location callback:(geocoderCallback)callback;
-(NSString *) getCurrentLocationFormattedAddress;
-(NSString *) formatAddressWithPlacemark:(CLPlacemark *) placemark;
-(void)addressFromCity:(NSString *)cityName callback:(citygeocoderCallback)callback;
@end
