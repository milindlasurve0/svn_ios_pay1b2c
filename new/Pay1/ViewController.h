//
//  ViewController.h
//  Pay1
//
//  Created by Annapurna on 02/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
@class DBManager;
@class TPKeyboardAvoidingScrollView;
@class NoNetworkView;
@class CreateUserResponse;
@interface ViewController : UIViewController <UIScrollViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate>{
    
    IBOutlet UIButton *startButton;
    IBOutlet UIButton *termsButton;
    IBOutlet UIButton *privacyButton;
    
    IBOutlet UITextField *phNumTxtField;
    UIView *bgImgView;
    
    UIImageView *image;
    
    NSTimer *pageTimer;
    
    
 
    
    NoNetworkView *objNoNetworkView;
    
    MBProgressHUD *HUD;
    IBOutlet UIButton *nextButton;
    
    IBOutlet UIImageView *logoImageView;
    IBOutlet UIImageView *thinImageView;
    IBOutlet UILabel *indiaCodeLabel;
    
    
    IBOutlet UITextField *incorrectTextField;
    IBOutlet UIImageView *otpErrorImgView;
    
    IBOutlet UIView *termsView;
    
    CreateUserResponse *createUserResponseObj;
    DBManager *dbManager;
    
}
@property (nonatomic, strong) IBOutlet TPKeyboardAvoidingScrollView *scrollView;


@end

