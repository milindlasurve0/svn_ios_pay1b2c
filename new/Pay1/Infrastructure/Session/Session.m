//
//  Session.m
//  ClearPathGPS
//
//  Created by Hemantech on 06/03/14.
//  Copyright (c) 2014 Hemantech. All rights reserved.
//

#import "Session.h"

@implementation Session


static Session *_sharedSession=nil;

+(Session*) shared {
	@synchronized([Session class]) {
		if(!_sharedSession)
		_sharedSession	= [[self alloc] init];
		return _sharedSession;
	}
}


+(id)alloc {
	@synchronized([Session class]) {
		NSAssert(_sharedSession==nil,@"Attempt to allocate second object of singleton class");
		_sharedSession = [super alloc];
		return _sharedSession;
	}
	return nil;
}



-(id)init {
	self = [super init];
	if(self!=nil) {
	}
	return self;
}

@end
