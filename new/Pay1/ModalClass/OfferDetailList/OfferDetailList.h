//
//  OfferDetailList.h
//  Pay1
//
//  Created by Annapurna on 09/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OfferDetailList : NSObject
@property (nonatomic, strong) NSString *featured;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *idVal;
@property (nonatomic, strong) NSString *details;
@end
