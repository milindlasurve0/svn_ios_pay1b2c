//
//  GiftCategoryList.h
//  Pay1
//
//  Created by Annapurna on 16/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GiftCategoryList : NSObject

@property (nonatomic, strong) NSString *catID;
@property (nonatomic, strong) NSString *catName;
@property (nonatomic, strong) NSString *details;


@end
