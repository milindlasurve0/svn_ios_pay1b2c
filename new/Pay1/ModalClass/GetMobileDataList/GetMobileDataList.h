//
//  GetMobileDataList.h
//  Pay1
//
//  Created by Annapurna on 03/07/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetMobileDataList : NSObject

@property (nonatomic, strong) NSString *areaName;
@property (nonatomic, strong) NSString *area;
@property (nonatomic, strong) NSString *operatorName;
@property (nonatomic, strong) NSString *operatorID;
@property (nonatomic, strong) NSString *productID;
@property (nonatomic, strong) NSString *startNumber;


@end
