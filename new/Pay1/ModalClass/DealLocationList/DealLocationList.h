//
//  DealLocationList.h
//  Pay1
//
//  Created by Annapurna on 09/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DealLocationList : NSObject

@property (nonatomic, assign) NSString *dealLat;
@property (nonatomic, assign) NSString *dealLong;
@property (nonatomic, strong) NSString *dealAddress;
@property (nonatomic, strong) NSString *dealCity;
@property (nonatomic, strong) NSString *dealState;
@property (nonatomic, strong) NSString *dealArea;
@property (nonatomic, strong) NSString *dealFullAddress;
@property (nonatomic, assign) NSString *dealOfferID;
@property (nonatomic, strong) NSString *dealerContactNum;


@end
