//
//  GetMyDealList.h
//  Pay1
//
//  Created by Annapurna on 12/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetMyDealList : NSObject

@property (nonatomic, strong) NSString *actualPrice;
@property (nonatomic, strong) NSString *amount;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *couponStatus;
@property (nonatomic, strong) NSString *dealID;
@property (nonatomic, strong) NSString *dealName;
@property (nonatomic, strong) NSString *dealerContact;
@property (nonatomic, strong) NSString *discount;
@property (nonatomic, strong) NSString *expiry;
@property (nonatomic, strong) NSString *myDealID;
@property (nonatomic, strong) NSString *myDealImgURL;
@property (nonatomic, strong) NSString *myDealLogoURL;
@property (nonatomic, strong) NSString *longDesc;
@property (nonatomic, strong) NSString *minAmount;
@property (nonatomic, strong) NSString *myLikes;
@property (nonatomic, strong) NSString *offerDec;
@property (nonatomic, strong) NSString *offerID;
@property (nonatomic, strong) NSString *offerName;
@property (nonatomic, strong) NSString *offerPrice;
@property (nonatomic, strong) NSString *quantity;
@property (nonatomic, strong) NSString *shortDesc;
@property (nonatomic, strong) NSString *staus;
@property (nonatomic, strong) NSString *stockSold;
@property (nonatomic, strong) NSString *totalStock;
@property (nonatomic, strong) NSString *transDateTime;
@property (nonatomic, strong) NSString *transactionID;
@property (nonatomic, strong) NSString *transactionMode;
@property (nonatomic, strong) NSString *usersID;
@property (nonatomic, strong) NSString *pinCode;


-(void)encodeWithCoder:(NSCoder *)aCoder;
-(id)initWithCoder:(NSCoder *)aDecoder;
@end
