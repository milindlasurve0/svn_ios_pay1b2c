//
//  GetMyDealList.m
//  Pay1
//
//  Created by Annapurna on 12/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "GetMyDealList.h"

@implementation GetMyDealList

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:_actualPrice forKey:@"actual_price"];
    [aCoder encodeObject:_amount forKey:@"amount"];
    
    [aCoder encodeObject:_code forKey:@"code"];
    [aCoder encodeObject:_couponStatus forKey:@"coupon_status"];
    [aCoder encodeObject:_dealID forKey:@"deal_id"];
    [aCoder encodeObject:_dealName forKey:@"deal_name"];
    [aCoder encodeObject:_dealerContact forKey:@"dealer_contact"];
    [aCoder encodeObject:_discount forKey:@"discount"];
    [aCoder encodeObject:_expiry forKey:@"expiry"];
    [aCoder encodeObject:_myDealID forKey:@"id"];
    [aCoder encodeObject:_myDealImgURL forKey:@"img_url"];
    [aCoder encodeObject:_myDealLogoURL forKey:@"logo_url"];
    [aCoder encodeObject:_longDesc forKey:@"long_desc"];
    [aCoder encodeObject:_minAmount forKey:@"min_amount"];
    [aCoder encodeObject:_myLikes forKey:@"mylikes"];
    [aCoder encodeObject:_offerDec forKey:@"offer_desc"];
    [aCoder encodeObject:_offerID forKey:@"offer_id"];
    [aCoder encodeObject:_offerName forKey:@"offer_name"];
    [aCoder encodeObject:_offerPrice forKey:@"offer_price"];
    [aCoder encodeObject:_quantity forKey:@"quantity"];
    [aCoder encodeObject:_shortDesc forKey:@"short_desc"];
    [aCoder encodeObject:_staus forKey:@"status"];
    // [aCoder encodeObject:_featureListArray forKey:@"featureList"];
    [aCoder encodeObject:_stockSold forKey:@"stock_sold"];
    [aCoder encodeObject:_totalStock forKey:@"total_stock"];
    [aCoder encodeObject:_transDateTime forKey:@"trans_datetime"];
    [aCoder encodeObject:_transactionID forKey:@"transaction_id"];
    [aCoder encodeObject:_transactionMode forKey:@"transaction_mode"];
    [aCoder encodeObject:_usersID forKey:@"users_id"];

    
    
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [self init];
    self.actualPrice    = [aDecoder decodeObjectForKey:@"actual_price"];
    self.amount = [aDecoder decodeObjectForKey:@"amount"];
    self.code    = [aDecoder decodeObjectForKey:@"code"];
    self.couponStatus = [aDecoder decodeObjectForKey:@"coupon_status"];
    self.dealID    = [aDecoder decodeObjectForKey:@"deal_id"];
    self.dealName = [aDecoder decodeObjectForKey:@"deal_name"];
    self.dealerContact    = [aDecoder decodeObjectForKey:@"dealer_contact"];
    self.discount = [aDecoder decodeObjectForKey:@"discount"];
    self.expiry    = [aDecoder decodeObjectForKey:@"expiry"];
    self.myDealID = [aDecoder decodeObjectForKey:@"id"];
    self.myDealImgURL    = [aDecoder decodeObjectForKey:@"img_url"];
    self.myDealLogoURL = [aDecoder decodeObjectForKey:@"logo_url"];
    self.longDesc    = [aDecoder decodeObjectForKey:@"long_desc"];
    self.minAmount = [aDecoder decodeObjectForKey:@"min_amount"];
    self.myLikes    = [aDecoder decodeObjectForKey:@"mylikes"];
    self.offerDec = [aDecoder decodeObjectForKey:@"offer_desc"];
    self.offerID    = [aDecoder decodeObjectForKey:@"offer_id"];
    self.offerName = [aDecoder decodeObjectForKey:@"offer_name"];
    self.offerPrice    = [aDecoder decodeObjectForKey:@"offer_price"];
    self.quantity = [aDecoder decodeObjectForKey:@"quantity"];
    self.shortDesc    = [aDecoder decodeObjectForKey:@"short_desc"];
    self.staus = [aDecoder decodeObjectForKey:@"status"];
    self.stockSold    = [aDecoder decodeObjectForKey:@"stock_sold"];
    self.transDateTime = [aDecoder decodeObjectForKey:@"trans_datetime"];
    self.transactionID    = [aDecoder decodeObjectForKey:@"transaction_id"];
    self.transactionMode = [aDecoder decodeObjectForKey:@"transaction_mode"];
    self.usersID = [aDecoder decodeObjectForKey:@"users_id"];
    return self;
}

@end
