//
//  PlanList.h
//  Pay1
//
//  Created by Annapurna on 02/07/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlanList : NSObject

@property (nonatomic, strong) NSString *planAmount;
@property (nonatomic, strong) NSString *planValidity;
@property (nonatomic, strong) NSString *planDescription;
@property (nonatomic, strong) NSString *planFlag;

@property (nonatomic, strong) NSString *planCircleID;
@property (nonatomic, strong) NSString *planCircleName;
@property (nonatomic, strong) NSString *planType;



@end
