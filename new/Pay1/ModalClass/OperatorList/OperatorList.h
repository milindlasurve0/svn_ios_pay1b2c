//
//  OperatorList.h
//  Pay1
//
//  Created by Annapurna on 13/05/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OperatorList : NSObject

@property (nonatomic, strong) NSString *operatorID;
@property (nonatomic, strong) NSString *operatorCode;
@property (nonatomic, strong) NSString *productID;
@property (nonatomic, strong) NSString *operatorName;
@property (nonatomic, strong) NSString *serviceID;
@property (nonatomic, strong) NSString *flag;
@property (nonatomic, strong) NSString *operatorDelegate;
@property (nonatomic, strong) NSString *operatorSTV;
@property (nonatomic, strong) NSString *operatorMin;
@property (nonatomic, strong) NSString *operatorMax;
@property (nonatomic, strong) NSString *operatorChargeSlab;
@property (nonatomic, strong) NSString *serviceChargeAmount;
@property (nonatomic, strong) NSString *serviceTaxPercent;
@property (nonatomic, strong) NSString *serviceChargePercent;
@property (nonatomic, strong) NSString *operatorLength;
@property (nonatomic, strong) NSString *operatorPrefix;




@end
