//
//  TransactionList.m
//  Pay1
//
//  Created by webninjaz on 09/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import "TransactionList.h"

@implementation TransactionList

-(id)init {
    self = [super init];
    if(self) {
        self.transactionDataArray = [[NSMutableArray alloc] init] ;
    }
    return self;
}

@end
