//
//  TransactionList.h
//  Pay1
//
//  Created by webninjaz on 09/06/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransactionList : NSObject

@property (nonatomic, strong) NSString *transactionID;
@property (nonatomic, strong) NSString *transactionTransType;
@property (nonatomic, strong) NSString *transactionDateTime;
@property (nonatomic, strong) NSString *transactionAmount;
@property (nonatomic, strong) NSString *transactionTransCategory;
@property (nonatomic, strong) NSString *transactionStatus;
@property (nonatomic, strong) NSString *transactionPhoneNumber;
@property (nonatomic, strong) NSString *transactionRechargeFlag;
@property (nonatomic, strong) NSString *transactionClosingBalance;
@property (nonatomic, strong) NSString *transactionOperatorName;
@property (nonatomic, strong) NSString *transactionDealName;
@property (nonatomic, strong) NSString *transactionOfferName;
@property (nonatomic, strong) NSString *transactionPaymentMode;
@property (nonatomic, strong) NSString *transactionBaseAmount;
@property (nonatomic, strong) NSString *transactionIPAddress;
@property (nonatomic, strong) NSString *transactionMihPayID;
@property (nonatomic, strong) NSString *transactionResponseID;
@property (nonatomic, strong) NSString *transactionProductID;
@property (nonatomic, strong) NSString *transactionServiceID;
@property (nonatomic, strong) NSString *transactionMode;


@property (nonatomic, strong) NSString *paymentMode;



@property (nonatomic, strong) NSMutableArray *transactionDataArray;

@end
