//
//  StoreList.h
//  Pay1
//
//  Created by Annapurna on 10/08/15.
//  Copyright (c) 2015 Annapurna. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoreList : NSObject

@property (nonatomic, strong) NSString *storeMobileNum;
@property (nonatomic, strong) NSString *storeShopName;
@property (nonatomic, strong) NSString *storeSale;
@property (nonatomic, strong) NSString *storeLat;
@property (nonatomic, strong) NSString *storeLong;
@property (nonatomic, strong) NSString *storeUserID;
@property (nonatomic, strong) NSString *storeAddress;
@property (nonatomic, strong) NSString *storePinCode;
@property (nonatomic, strong) NSString *storeAreaName;
@property (nonatomic, strong) NSString *storeCityName;
@property (nonatomic, strong) NSString *storeDistance;
@property (nonatomic, strong) NSString *storeStateName;

@property (nonatomic, strong) NSString *storeImageURL;


@end
